package com.tct.utils;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class Log {
    /** Priority constant for the log method; use Log.d */
    public static final int DEBUG = 3;
    /** Priority constant for the log method; use Log.i */
    public static final int INFO = 4;
    /** Priority constant for the log method; use Log.w */
    public static final int WARN = 5;
    /** Priority constant for the log method; use Log.e */
    public static final int ERROR = 6;
    /** Priority constant for the log method; use Log.r */
    public static final int REPORT = 7;
    public static final int NOLOG = 9;
    public static final int FILE = 0x01;
    public static final int CONSOLE = 0x02;

    private static final HashMap<String, Log> logs = new HashMap<String, Log>();

    private boolean enableFile = false;
    private boolean enableConsole = false;
    private int level = DEBUG;
    private String tag = null;
    private FileWriter fw = null;
    
    private final static String LOGDIR = ".com/tct/utils/Log/";
    static {
    	File dirFile = new File(LOGDIR);
    	if (!dirFile.exists()) {
    		dirFile.mkdirs();
    	}
    }
    
    private Log(String t) {
        tag = t;
        try {
            fw = new FileWriter(LOGDIR + t+".log", true);
        } catch (IOException e) {
        }
    }

    public synchronized static Log instance(String tag) {
        if (tag == null || tag.length() == 0) {
            tag = "default";
        }
        if (!logs.containsKey(tag)) {
            logs.put(tag, new Log(tag));
        }
        return logs.get(tag);
    }

    private void log(int lvl, String msg) {
        if (lvl >= level) {
            if (enableFile) {
                try {
                    fw.write(toLevel(lvl) + tag + ":" + msg + "\n");
                    fw.flush();
                } catch (IOException e) {
                }
            }
            if (enableConsole) {
                System.out.println(toLevel(lvl) + tag + ":" + msg);
            }
        }
    }

    private String toLevel(int lvl) {
        String str = "";
        switch (lvl) {
        case DEBUG:
            str = "[DEBUG]";
            break;
        case INFO:
            str = "[INFO]";
            break;
        case WARN:
            str = "[WARN]";
            break;
        case ERROR:
            str = "[ERROR]";
            break;
        case REPORT:
            str = "[REPORT]";
            break;
        default:
            break;
        }
        return str;
    }

    public Log setLogLevel(int lvl) {
        level = lvl;
        return this;
    }

    public Log setOutputs(int outputs) {
        enableFile = (outputs & FILE) > 0;
        enableConsole = (outputs & CONSOLE) > 0;
        return this;
    }

    public void d(String msg) {
        log(DEBUG, msg);
    }

    public void i(String msg) {
        log(INFO, msg);
    }

    public void w(String msg) {
        log(WARN, msg);
    }

    public void e(String msg) {
        log(ERROR, msg);
    }
    
    public void r(String msg) {
        log(REPORT, msg);
    }
}
