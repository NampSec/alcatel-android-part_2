package com.tct.android.tools.feature.app;

import static com.tct.android.tools.feature.app.Main.LOG;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.tct.android.tools.feature.sdm2.SDM2;
import com.tct.android.tools.feature.sdm2.SdmTable;
import com.tct.utils.AndroidPath;

/**
 * CommandLine: tct_feature sdm2h make命令参数 项目名 ${OUT_DIR}
 * 例如 tct_feature sdm2j "" riol_rt out/target/product/riol_tf/tct_intermediates/feature/libtctfeature
 * 最后的文件为：
 * out/target/product/riol_tf/tct_intermediates/feature/libtctfeature/tct_feature_sdm_rid.h 和
 * out/target/product/riol_tf/tct_intermediates/feature/libtctfeature/tct_feature_sdm_value.h
 */
//make       | (plf+xplf)+splf
//make perso | plf+(xplf+splf)
public class SDM2H implements Main.Command {
	private static final String RID_FILE = "tct_feature_sdm_rid.h";
	private static final String VALUE_FILE = "tct_feature_sdm_value.h";
	
	private String mOutDir;
	private List<String> mStrSdmidList;
	private List<String> mNumSdmidList;	
	
	@Override
	public int action(String[] args) {
		if (args.length == 4) {
			LOG.i(args[1] + "  " + args[2] + "  " + args[3]);
			boolean isMakePerso = args[1].contains("perso");
			String proj = args[2].trim();
			mOutDir = AndroidPath.plainPath(args[3]);
			SDM2 sdm2h = new SDM2(isMakePerso, proj);
			mStrSdmidList = sdm2h.getStrSdmidList();
			mNumSdmidList = sdm2h.getNumSdmidList();
			genRidH();
			genValueH();
		} else {
			LOG.e("COMMAND ERROR: args.LEN=" + args.length);
		}
		return 0;
	}
	
	//############################################################//
	private void genRidH() {
        File file  = new File(mOutDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        String oPLFJavaFile = file.getPath() + "/" + RID_FILE;
        FileWriter fw = null;
        try {
            fw = new FileWriter(oPLFJavaFile);
            fw.write("//----This file is auto-generated.  DO NOT MODIFY.----//\n\n");
            fw.write("#ifndef __TCT_FEATURE_SDM_RID_H\n");
            fw.write("#define __TCT_FEATURE_SDM_RID_H\n\n");
            fw.write("#define SDM_TO_RID(sdmid) TF_##sdmid\n\n");
            if (!mStrSdmidList.isEmpty()) {
            	int index = 0;
            	for(String str : mStrSdmidList) {
            		fw.write("#define TF_" + str.trim() + "  "+ index + "\n");
            		index++;
            	}
            }
            fw.write("\n");
            if (!mNumSdmidList.isEmpty()) {
            	int index = 0;
            	for(String num : mNumSdmidList) {
            		fw.write("#define TF_" + num.trim() + "  "+ index + "\n");
            		index++;
            	}
            }
            fw.write("\n#endif\n");
        } catch (Exception e) {
            Main.LOG.e("tct_feature_sdm_rid.h Error:" + e.toString());
        } finally {
            try {
                fw.close();
            } catch (IOException e) {
            }
        }
	}
	
	private void genValueH() {
		File file  = new File(mOutDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        String oPLFJavaFile = file.getPath() + "/" + VALUE_FILE;
        FileWriter fw = null;
        try {
            fw = new FileWriter(oPLFJavaFile);
            fw.write("//----This file is auto-generated.  DO NOT MODIFY.----//\n\n");
            fw.write("#ifndef __TCT_FEATURE_SDM_VALUE_H\n");
            fw.write("#define __TCT_FEATURE_SDM_VALUE_H\n\n");
            fw.write("const unsigned TF_SDM_STR_LEN = " + mStrSdmidList.size() + ";\n");
            fw.write("const unsigned TF_SDM_NUM_LEN = " + mNumSdmidList.size() + ";\n\n");
            
            fw.write("const char TF_SDM_STR[][TCT_FEATURE_SDM_VALUE_MAX]  = {");
            if (!mStrSdmidList.isEmpty()) {
            	int index = 1;
            	for(String str : mStrSdmidList) {
            		SdmTable.VarInfo vInfo = SdmTable.getVar(str.trim());
            		fw.write("\n        \"" + vInfo.value() + "\""); //这里可能要逃逸一些特殊符号，暂时不实现了
            		if (index < mStrSdmidList.size()) {
            			fw.write(",");
            		}
            		fw.write("    //" + (index-1) + "  " + str);
            		index++;
            	}
            }
            fw.write("\n};\n\n");
            
            fw.write("const int TF_SDM_NUM[] = {");
            if (!mNumSdmidList.isEmpty()) {
            	int index = 1;
            	for(String num : mNumSdmidList) {
            		SdmTable.VarInfo vInfo = SdmTable.getVar(num.trim());
            		String v = vInfo.value();
            		if (vInfo.type().equals(SdmTable.VarInfo.BOOLEAN)) {
            			v = v.equals("true") ? "1" : "0";
            		}
            		fw.write("\n        " + v);
            		if (index < mNumSdmidList.size()) {
            			fw.write(",");
            		}
            		fw.write("    //" + (index-1) + "  " + num);
            		index++;
            	}
            }
            fw.write("\n};\n\n");
            
            fw.write("#endif\n");
        } catch (Exception e) {
            Main.LOG.e("tct_feature_sdm_rid.h Error:" + e.toString());
        } finally {
            try {
                fw.close();
            } catch (IOException e) {
            }
        }
	}

}
