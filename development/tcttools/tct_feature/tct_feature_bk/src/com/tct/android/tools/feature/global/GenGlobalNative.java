package com.tct.android.tools.feature.global;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.tct.android.tools.feature.global.GlobalConf.Section;

public class GenGlobalNative {
	private final static String NATIVE_MK_FILE = "global_native.mk";
	private FileWriter fw;
	
	public GenGlobalNative(String outDir) {
		File file  = new File(outDir);
        if (!file.exists()) {
            file.mkdirs();
        }
        String outputJavaFile = file.getPath() + "/" + NATIVE_MK_FILE;
        try {
            fw = new FileWriter(outputJavaFile);
        }catch (IOException e) {
        }
	}
	
	
	public void write(Section section) {
		try {
			if ("integer".equalsIgnoreCase(section.type())) {
				fw.write("LOCAL_CFLAGS += -D" + section.finalName() + "=" + section.value() + "\n");
			} else if ("boolean".equalsIgnoreCase(section.type())) {
				if ("true".equalsIgnoreCase(section.value())) {
					fw.write("LOCAL_CFLAGS += -D" + section.finalName() + "\n");
				}
			} else if ("string".equalsIgnoreCase(section.type())) {
				fw.write("LOCAL_CFLAGS += -D" + section.finalName() + "=\\\"" + section.value() + "\\\"\n");
			}
		} catch (IOException e) {
		}
	}
	
	public void close(){
		try {
			fw.close();
		} catch (IOException e) {
		}
	}
}
