package com.tct.android.tools.feature.sdm2;


public interface OnSdmListener {
	public void onNewSdm(Boolean definedInPlf, String sdmid, String type, String value);
	public void onSdmUpdate(String sdmid, String value);
}
