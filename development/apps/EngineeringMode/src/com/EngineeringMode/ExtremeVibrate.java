/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                       ExtremeVibrate.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.app.Activity;
import android.app.Service;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ToggleButton;

public class ExtremeVibrate extends Activity {
    private Vibrator mVibrator;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vibrator);
        final ToggleButton mToggleButton1 = (ToggleButton) findViewById(R.id.myTogglebutton1);
        final ToggleButton mToggleButton2 = (ToggleButton) findViewById(R.id.myTogglebutton2);
        final ToggleButton mToggleButton3 = (ToggleButton) findViewById(R.id.myTogglebutton3);
        mVibrator = (Vibrator) getApplication().getSystemService(
                Service.VIBRATOR_SERVICE);

        mToggleButton1.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (mToggleButton1.isChecked()) {
                    mToggleButton2.setChecked(false);
                    mToggleButton3.setChecked(false);
                    mVibrator.vibrate(new long[] { 100, 10, 100, 1000 }, -1);
                    // mTV.setText(getString(R.string.str_text1) + " "
                    // + getString(R.string.str_ok));
                } else {
                    mVibrator.cancel();
                    // mTV.setText(getString(R.string.str_text1) + " "
                    // + getString(R.string.str_end));
                }
            }
        });

        mToggleButton2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (mToggleButton2.isChecked()) {
                    mToggleButton1.setChecked(false);
                    mToggleButton3.setChecked(false);
                    mVibrator.vibrate(new long[] { 0, 10 }, 0);
                    // mTV.setText(getString(R.string.str_text2) + " "
                    // + getString(R.string.str_ok));

                } else {

                    mVibrator.cancel();
                    // mTV.setText(getString(R.string.str_text2) + " "
                    // + getString(R.string.str_end));

                }
            }
        });

        mToggleButton3.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (mToggleButton3.isChecked()) {
                    mToggleButton1.setChecked(false);
                    mToggleButton2.setChecked(false);
                    mVibrator.vibrate(new long[] { 1000, 50, 1000, 50, 1000 },
                            0);
                    // mTV.setText(getString(R.string.str_text3) + " "
                    // + getString(R.string.str_ok));

                } else {

                    mVibrator.cancel();
                    // mTV.setText(getString(R.string.str_text3) + " "
                    // + getString(R.string.str_end));

                }
            }
        });

    }

    protected void onResume() {
        super.onResume();
        mVibrator.cancel();
    }

    protected void onPause() {
        super.onPause();
        mVibrator.cancel();
    }

    protected void onDestroy() {
        super.onDestroy();
        mVibrator.cancel();
    }
}
