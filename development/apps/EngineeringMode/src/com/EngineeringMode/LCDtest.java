/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                            ColorView.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;

import android.util.TctLog;

import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

/*
 * ColorView
 */
class ColorView extends View {

    private Paint mPainter = new Paint();
    private Rect mTempRect = new Rect();
    private CallBack mPreDrawCb;

    private int[] Colors = { Color.BLACK };

    private Bitmap LcdBitmap;

    ColorView(Context c) {
        super(c);
    }

    ColorView(Context c, int[] colors) {
        super(c);
        Colors = colors;
    }

    ColorView(Context c, int color) {
        super(c);
        Colors[0] = color;
    }

    ColorView(Context c, Bitmap b, CallBack cb) {
        super(c);
        LcdBitmap = b;
        mPreDrawCb = cb;
    }

    @Override
    protected void onDraw(Canvas canvas) {

        mPainter.setStyle(Paint.Style.FILL);

        if (LcdBitmap == null) {

            int rectLength = Lcd.height() / Colors.length;
            int FirstRectLength = Lcd.height() - (Colors.length - 1)
                    * rectLength;

            mPainter.setColor(Colors[0]);
            // mPainter.setAlpha(0x20);
            // draw background rect
            mTempRect.set(0, 0, Lcd.width(), FirstRectLength);
            canvas.drawRect(mTempRect, mPainter);

            for (int i = 1; i < Colors.length; i++) {
                mPainter.setColor(Colors[i]);
                // draw rect : the function just fills INSIDE the rectangle,
                // thus the rectangle
                // has to be one more pixel wide/long
                mTempRect.set(0, FirstRectLength + rectLength * (i - 1),
                        Lcd.width(), FirstRectLength + rectLength * i);
                canvas.drawRect(mTempRect, mPainter);
            }
        } else {
            // canvas.drawBitmap(LcdBitmap, 0, 0, null);
            mPainter.setColor(Color.WHITE);
            mTempRect.set(0, 0, Lcd.width(), Lcd.height());
            canvas.drawRect(mTempRect, mPainter);

            for (int x = 0; x < Lcd.width(); x++) {
                for (int y = (x & 1); y < Lcd.height(); y += 2) {
                    mPainter.setColor(Color.BLACK);
                    canvas.drawPoint(x, y, mPainter);
                }
            }

            // canvas.drawText(text, start, end, x, y, paint)
        }
        if (mPreDrawCb != null)
            mPreDrawCb.c();
    }
}

class LcdTest extends Test {
    String TAG = "LCD_Color";

    private FrameLayout fl;
    private ColorView cv;

    // private LinearLayout ll;
    // rivate LinearLayout llsk;
    private TestLayout1 tl;

    private boolean mPop = false; // pop buttons or remove buttons.

    LcdTest(ID pid, String s, int min) {
        super(pid, s, min, 0);
        setTouchHandler();
    }

    LcdTest(ID pid, String s) {
        super(pid, s);
        setTouchHandler();
    }

    private void setTouchHandler() {
        hTouch = new TouchHandler() {
            public boolean handleTouch(MotionEvent event) {
                TctLog.d(TAG, "on touch event.");
                if (event.getAction() == event.ACTION_UP) {
                    // toggleButtons();
                    return true;
                }
                return false;
            }
        };
    }

    private void toggleButtons() {

        if (mPop == false) {
            fl.addView(tl.ll);
            mPop = true;
        } else {
            mPop = false;
            fl.removeView(tl.ll);
        }
    }

    private void setButtonLayout(boolean pop) {
        this.setButtonLayout("PASS", "FAIL", pop);

    }

    private void setButtonLayout(String lsk, String rsk, boolean pop) {

        /* fix venus_cr [148173],jiangdong,start */
        tl = new TestLayout1(mContext, "    ", "    ", lsk, rsk, 200);
        /* fix venus_cr [148173],jiangdong,end */
        // tl = new TestLayout1(mContext, null, null, lsk, rsk, 600);

        if (pop == true) {
            fl.addView(tl.ll);
            mPop = true;
        } else {
            mPop = false;
        }
    }

    private void TestRGB() {
        int[] Colors = { Color.RED, Color.GREEN, Color.BLUE };

        fl = new FrameLayout(mContext);

        cv = new ColorView(mContext, Colors);
        fl.addView(cv);

        // only the first test scene.
        TextView tvbody = new TextView(mContext);
        tvbody.setGravity(Gravity.CENTER);
        // tvbody.setTypeface(Typeface.MONOSPACE, 1);
        tvbody.setTextAppearance(mContext, android.R.style.TextAppearance_Large);
        tvbody.setText("LCD");
        tvbody.setTextSize(40);
        tvbody.setTextColor(Color.BLACK);

        // fl.addView(tvbody);

        setButtonLayout(true);
        mContext.setContentView(fl);
    }

    private void TestGreyChart() {
        int MaxRows = 16;
        int Colors[] = new int[MaxRows];

        for (int i = 0; i < Colors.length; i++) {
            Colors[i] = Color.BLACK + 0x111111 * i;
        }

        fl = new FrameLayout(mContext);

        cv = new ColorView(mContext, Colors);
        fl.addView(cv);

        setButtonLayout(true);
        mContext.setContentView(fl);
    }

    private void TestColor(int c) {
        fl = new FrameLayout(mContext);

        cv = new ColorView(mContext, c);
        fl.addView(cv);

        setButtonLayout(true);
        mContext.setContentView(fl);
    }

    private void TestChecker() {

        Bitmap b;

        int[] tc = new int[Lcd.height() * Lcd.width()];
        for (int i = 0; i < tc.length; i++) {
            tc[i] = Color.BLUE;
            // tc[i] = ((i & 1) == 0 ? Color.BLACK : Color.WHITE);
        }

        b = Bitmap.createBitmap(tc, Lcd.width(), Lcd.height(), Config.ALPHA_8);

        fl = new FrameLayout(mContext);

        cv = new ColorView(mContext, b, new CallBack() {
            public void c() {
                mTimeIn.start();
            }
        });
        fl.addView(cv);

        setButtonLayout(false);
        mContext.setContentView(fl);
    }

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT:
            TestRGB();
            break;

        case INIT + 1:
            TestColor(Color.BLACK);
            break;

        case INIT + 2:
            TestGreyChart();
            break;

        case INIT + 3:
            TestColor(Color.WHITE);
            mState = MULTI_PAGE_LAST_PAGE;
            break;

        case INIT + 4:
            // TestChecker();
            break;

        case END:
        default:
            break;
        }
    }
}

/*
 * Image display test
 */
class ImageTest extends Test {

    private int mImageId;

    ImageTest(ID pid, String s, int r) {
        super(pid, s);
        mImageId = r;

    }

    ImageTest(ID pid, String s, int r, int timein) {
        super(pid, s, timein, 0);
        mImageId = r;

        hKey = new KeyHandler() {
            public boolean handleKey(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_MENU
                        && mTimeIn.isFinished()) {
                    ExecuteTest.currentTest.Exit();
                    return true;
                }
                return false;
            }
        };

    }

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT: // init the test, shows the first screen

            mTimeIn.start();

            mContext.setContentView(new UnscaledView(mContext));
            Result = PASSED;

            break;
        case INIT + 1:// step n of the test, update the screen, set key
            // handlers

            break;
        case END://

            break;
        }

    }

    private class UnscaledView extends View {
        private Paint mPaint = new Paint();
        private Path mPath = new Path();
        private boolean mAnimate;
        private long mNextTime;
        Bitmap mBitmap;

        android.graphics.BitmapFactory.Options bfo;

        public UnscaledView(Context context) {
            super(context);
            // if we don't turn of the the scaling property, image will be
            // resized according
            // to screen density . as we set density = 120 and the reference in
            // android is 160
            // image will be down-sized by 0.75 ratio */
            bfo = new android.graphics.BitmapFactory.Options();
            bfo.inScaled = false;

            mBitmap = android.graphics.BitmapFactory.decodeResource(
                    getResources(), mImageId, bfo);
        }

        @Override
        protected void onDraw(Canvas canvas) {

            mBitmap.setDensity(Bitmap.DENSITY_NONE);
            canvas.setDensity(Bitmap.DENSITY_NONE);
            canvas.drawBitmap(mBitmap, 0, 0, null);

        }

        @Override
        protected void onAttachedToWindow() {
            mAnimate = true;
            super.onAttachedToWindow();
        }

        @Override
        protected void onDetachedFromWindow() {
            mAnimate = false;
            super.onDetachedFromWindow();
        }
    }

}
