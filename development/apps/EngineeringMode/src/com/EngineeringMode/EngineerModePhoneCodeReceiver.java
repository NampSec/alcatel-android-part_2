package com.EngineeringMode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.android.internal.telephony.TelephonyIntents;
import android.util.TctLog;

public class EngineerModePhoneCodeReceiver extends BroadcastReceiver{
     private static final String TAG = "EngineerModePhoneCodeReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {

           TctLog.d(TAG,"onReceive "+intent);
           String action = intent.getAction();
           String host = intent.getData() != null ? intent.getData().getHost() : null;

           //EngineeringMode,*#*#2637643#*#*
           if(TelephonyIntents.SECRET_CODE_ACTION.equals(action) && "2637643".equals(host)){
             Intent i = new Intent(Intent.ACTION_MAIN);
             i.setClassName("com.EngineeringMode", "com.EngineeringMode.EngineerMode");
             i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             context.startActivity(i);
        }
     }
}
