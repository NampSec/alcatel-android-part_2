/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/src/com/EngineeringMode/       */
/*                                                         FlashLEDTest.java  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;

import android.hardware.Camera;

import android.content.Context;
import android.util.TctLog;

/*
 * Empty Test use as a default when no test was defined
 */
class FlashLEDTest extends Test {

    TestLayout1 tl;
    String mDisplayString;
    private String TAG = "FlashLED";

    private int mCount = 0;
    private int mLastValue = 0;

    private android.hardware.Camera mCamera;
    private Camera.Parameters mParameters;

    FlashLEDTest(ID pid, String s) {
        super(pid, s);
        TAG = Test.TAG + TAG;

    }

    FlashLEDTest(ID pid, String s, int timein) {
        super(pid, s, timein, 0);
        TAG = Test.TAG + TAG;
    }

    @Override
    protected void Run() {
        // this function executes the test
        switch (mState) {
        case INIT:

            try {
                mCamera = Camera.open();
            } catch (Exception e) {
                TctLog.e("MMI Test", "can't open camera ");
                tl = new TestLayout1(mContext, mName, "Flash LED open failed");
                // tl.setAutoTestButtons(true,true);
                mContext.setContentView(tl.ll);
                return;
            }

            mParameters = mCamera.getParameters();

            String fl = "torch";
            mParameters.set("flash-mode", fl);
            mCamera.setParameters(mParameters);

            tl = new TestLayout1(mContext, mName, "turning on", 1000, true);
            mContext.setContentView(tl.ll);
            break;

        case INIT + 1:
            tl = new TestLayout1(mContext, mName, "Please check Camera LED");
            mContext.setContentView(tl.ll);
            break;

        case END://
            if (mCamera != null) {
                mCamera.release();
            }
            break;

        default:
            break;
        }
    }

    @Override
    protected void onTimeInFinished() {
        // if(tl!=null)
        // tl.setAutoTestButtons(true);
    }

}
