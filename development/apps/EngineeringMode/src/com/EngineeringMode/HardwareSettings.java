/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2015 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Dongdong Wang                                                     */
/* E-Mail:  Dongdong.wang@tcl.com                                             */
/* Role  :  MMITest                                                           */
/* Reference documents :  Hardware test_1.0.pdf                               */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EnginneringMode/src/com/EngineeringMode/       */
/*                                                    SettingsForIdol4.java   */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 11/12/15| Dongdong Wang  |                    | Add for hardware           */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.EngineeringMode;


import android.content.Context;
import android.content.Intent;
import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.os.SystemProperties;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;


import android.provider.Settings;
import android.widget.TextView;
import android.widget.Toast;


public class HardwareSettings extends Activity implements View.OnClickListener {
    private static final String ROOT_ON = "diag,serial_smd,rmnet_bam,adb";
    private static final String ROOT_OFF = "mtp,diag,adb";
    private static final int ENABLE = 1;//add by dongdong.wang 2015-12-15  t0 fix task 1133932
    private static final int DISENABLE = 0;//add by dongdong.wang 2015-12-15  t0 fix task 1133932
    private SwitchHolder usbSwitch = null;
    private SwitchHolder rootSwitch = null;
    private SwitchHolder lowPowerSwitch = null;
    private SwitchHolder lowTemperateureSwitch = null;
    private Context mContext;
    private final String TAG = "EnginneringMode.HardwareSettings";
    private String lowTemperaturePoint =
            "/sys/class/power_supply/battery/tcl_fixtemp";
    private String chargePoint = "/sys/class/power_supply/battery/charging_enabled";
    private String rootPoint = "persist.sys.usb.config";
    private String powerKeyPoint = "";
    private String lowPowerPoint = "low_power_detection_enable";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_idol4);
        mContext = HardwareSettings.this;
        usbSwitch = initSwitchButton(R.id.usb_switch, R.string.swith_use, R.drawable.settings_button_on_off);
        lowPowerSwitch = initSwitchButton(R.id.low_power_switch, R.string.switch_low_power, R.drawable.settings_button_on_off);
        rootSwitch = initSwitchButton(R.id.root_switch, R.string.switch_root, R.drawable.settings_button_on_off);
        lowTemperateureSwitch = initSwitchButton(R.id.low_temperature_switch, R.string.switch_low_temperature, R.drawable.settings_button_on_off);
        updateSwitchState();
    }

    /**
     * update switch state
     */
    private void updateSwitchState() {
        //modify by dongdong.wang 2015-12-15  t0 fix task 1133932 begin
        usbSwitch.setChecked(readValueForDevices(chargePoint) == ENABLE);//modify by dongdong.wang to fix defect 1280634 at 2016-01-04
        rootSwitch.setChecked(ROOT_ON.equals(SystemProperties.get(rootPoint)));
        lowPowerSwitch.setChecked(Settings.Global.getInt(mContext.getContentResolver(), lowPowerPoint, 0) != ENABLE);//default is false.
        lowTemperateureSwitch.setChecked(readValueForDevices(lowTemperaturePoint) == ENABLE);//modidy by dongdong.wang to fix defect 1280634 at 2016-01-04
        //modify by dongdong.wang 2015-12-15  t0 fix task 1133932 end
    }


    /**
     * set SystemProperity though by cmd.
     *
     * @param cmd
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public String setProperity(String cmd) throws IOException, InterruptedException {
        String result = "";
        Process process = null;
        Log.i(TAG, "cmd = " + cmd);
        process = Runtime.getRuntime().exec(cmd);
        process.waitFor();
        InputStreamReader isReader = new InputStreamReader(process.getInputStream());
        BufferedReader buffer = new BufferedReader(isReader);
        while ((result = buffer.readLine()) != null) {
            Log.i(TAG, "result = " + result);
            return result;
        }

        return null;
    }

    @Override
    public void onClick(View buttonView) {
        boolean isChecked = false;
        //modify by dongdong.wang 2015-12-15  t0 fix task 1133932 begin
        if (buttonView.getId() == R.id.usb_switch) {
            isChecked = usbSwitch.isChecked();
            boolean result = writeValueToDevices(chargePoint, isChecked);
            usbSwitch.setChecked(!isChecked);
        } else if (buttonView.getId() == R.id.low_power_switch) {
            isChecked = lowPowerSwitch.isChecked();
            if (isChecked) {
                //according from frameworks developer. default value = 0.
                // value = 0 means Close  prompts and automatic shutdown,
                // value = 1 means Open   prompts and automatic shutdown,
                Settings.Global.putInt(mContext.getContentResolver(), lowPowerPoint, DISENABLE);
            } else {
                Settings.Global.putInt(mContext.getContentResolver(), lowPowerPoint, ENABLE);
            }
            lowPowerSwitch.setChecked(!isChecked);
        } else if (buttonView.getId() == R.id.root_switch) {
            isChecked = rootSwitch.isChecked();
            if (isChecked) {
                SystemProperties.set(rootPoint, ROOT_OFF);
            } else {
                SystemProperties.set(rootPoint, ROOT_ON);
            }
            rootSwitch.setChecked(!isChecked);
        } else if (buttonView.getId() == R.id.low_temperature_switch) {
            isChecked = lowTemperateureSwitch.isChecked();
            boolean result = writeValueToDevices(lowTemperaturePoint, isChecked);
            lowTemperateureSwitch.setChecked(!isChecked);
        } else {
            Log.i(TAG, "unknow buttonview id");
        }
        //modify by dongdong.wang 2015-12-15  t0 fix task 1133932 end
    }

    /**
     * show Fail Reason
     * this si a
     */
    private void showFailReason() {
        Toast.makeText(mContext, R.string.settings_fail_tip, Toast.LENGTH_LONG).show();
    }

    /**
     * read device point value from devices
     * @param devicePoint
     * @return
     */
    public int readValueForDevices(String devicePoint) {
        int value = -1;
        File file = null;
        FileInputStream fis = null;
       
        try {
            file = new File(devicePoint);
            fis = new FileInputStream(file);
            value = fis.read();
        } catch (FileNotFoundException e) {
            Log.i(TAG, "File Not Found.");
            e.printStackTrace();
        } catch (IOException e) {
            Log.i(TAG, "File read null");
            e.printStackTrace();
        }finally {
            if (fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        value = value - '0';//add by dongdong.wang to fix defect 1280634 at 2016-01-04
        return  value;
    }


    /**
     * set The underlying hardware devices Point
     *
     * @param devicePoint
     * @param isChecked
     * @return
     */
    public boolean writeValueToDevices(String devicePoint, boolean isChecked) {
        int value = -1;
        File file = null;
        boolean result = false;
        FileOutputStream fos = null;

        if (isChecked) {
            value = 0;
        } else {
            value = 1;
        }
        try {
            file = new File(devicePoint);
            fos = new FileOutputStream(file);
            fos.write(Integer.toString(value).getBytes());
            result = true;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "openOutputStream error: " + devicePoint);
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    /**
     * init Switch Button
     *
     * @param resId
     * @param titleResId
     * @param bgResId
     * @return
     */
    private SwitchHolder initSwitchButton(int resId, int titleResId, int bgResId) {
        SwitchHolder holder = new SwitchHolder(findViewById(resId), titleResId, this, bgResId);
        return holder;
    }

    /**
     * instance a class SwitchHold View .
     */
    private class SwitchHolder {
        private View view = null;
        private TextView title = null;
        /**
         * <p>
         * Default visibility is gone, we call {@link #setVisibility(boolean)} to show or hide it.
         * </p>
         */
        private Switch mSwitch = null;

        public SwitchHolder(View parent, int titleResId, View.OnClickListener listener, int bgResId) {
            view = parent;
            title = (TextView) parent.findViewById(R.id.msi_title);
            mSwitch = (Switch) parent.findViewById(R.id.msi_switch);
            title.setText(titleResId);
            view.setOnClickListener(listener);
            view.setBackgroundResource(bgResId);
        }

        /**
         * Set text to {@link #title}
         *
         * @param resId
         */
        public void setTitle(int resId) {
            title.setText(resId);
        }

        /**
         * change the text color of {@link #title}
         * <p>
         * This funcation just be called by {@link #mFuncOn}.
         * </p>
         *
         * @param color
         */
        public void setTitleCoclor(int color) {
            title.setTextColor(color);
        }

        /**
         * change the visibility of {@link #mSwitch}
         *
         * @param visibility value is {@link View#VISIBLE} or {@link View#GONE}.
         */
        public void setSwitchVisibility(int visibility) {
            mSwitch.setVisibility(visibility);
        }

        /**
         * @return a boolean form {@link Switch#isChecked()}
         */
        public boolean isChecked() {
            return mSwitch.isChecked();
        }

        /**
         * change {@link #mSwitch}'s check and enable state.
         *
         * @param checked
         */
        public void setChecked(boolean checked) {
            mSwitch.setChecked(checked);
            mSwitch.setEnabled(checked);
        }

        /**
         * change visibility of {@link #view}.
         *
         * @param visibility value is {@link View#VISIBLE} or {@link View#GONE}.
         */
        public void setVisibility(int visibility) {
            view.setVisibility(visibility);
        }
    }
}