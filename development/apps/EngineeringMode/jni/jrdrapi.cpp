/******************************************************************************/
/*                                                               Date:09/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Qianbo Pan                                                        */
/* E-Mail:  Qianbo.Pan@tcl-mobile.com                                         */
/* Role  :  MMITest                                                           */
/* Reference documents :  Scribe 5_MMI_AUTO_Test_specfication_V3.0.Doc        */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/EngineeringMode/jni/jrdrapi.cpp                */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/12| Qianbo.Pan     |                    | Porting for EngineeringMode*/
/*         |                |                    |                            */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
#define LOG_TAG "jrdrapijni"
#define LOG_NDEBUG 0
#include <cutils/Log.h>

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include "comdef.h"

#include "snd.h"
#include "oem_rapi.h"

#include "jni.h"

#define INT_TO_LBYTE(a) ( (uint8_t) ( a & 0xFF) )
#define INT_TO_HBYTE(a) ( (uint8_t) ((a >> 8) & 0xFF) )

#define MAX_CMD_LEN 8

static int pack_n_int(uint8_t *buf, int n, ...) {
    int i = 0;
    va_list args;
    va_start(args, n);
    for (i = 0; i < n; i++) {
        int current = va_arg(args, int);
        buf[i * 2] = INT_TO_LBYTE(current);
        buf[i * 2 + 1] = INT_TO_HBYTE(current);
    }
    va_end(args);
    return n;
}

int writefifo(const char *name, uint8_t *buffer, int len) {

    int count = 0;
    int fifo = 0;
    int result = 0;

    if ((fifo = open(name, O_WRONLY)) == -1) {
        TCTALOGD("can't open fifo");
        result = -1;
    } else {
        count = 0;
        do {
            result = write(fifo, &buffer[count], len - count);
            count += result;
        } while (count < len && result > 0);

        (void) close(fifo);
    }
    return result;
}

int readfifo(const char *name, uint8_t *buffer, int max) {

    int result = 0;
    int nread = 0;
    int fifo = 0;

    if ((fifo = open(name, O_RDONLY)) == -1) {
        TCTALOGD("can't open fifo");
        result = -1;
    } else {
        nread = 0;
        do {
            result = read(fifo, &buffer[nread], max - nread);
            nread += result;
        } while (result > 0);

        TCTALOGD("received from modem : %d bytes : %s\n", nread, buffer);

        (void) close(fifo);
    }

    return result;
}

static jint sendcmd(JNIEnv *env, jobject thiz, jstring name, jshort cmd,
        jshort para1, jshort para2, jshort para3) {

    const char *fifo_name;
    char fifo_name_read[100];
    char fifo_name_write[100];

    int fifo;
    uint8_t bytes[8] = { 0 };
    uint8_t readbuf[128];
    jint result = 0;

    fifo_name = env->GetStringUTFChars(name, NULL);
    if (name == NULL) {
        return -1; /* OutOfMemoryError already thrown */
    }

    strncpy(fifo_name_read, fifo_name, 100);
    strncpy(fifo_name_write, fifo_name, 100);
    strcat(fifo_name_read, "_read");
    strcat(fifo_name_write, "_write");

    TCTALOGD("fifo > proxy  is %s", fifo_name_read);
    TCTALOGD("fifo < proxy  is %s", fifo_name_write);

    pack_n_int(bytes, 4, cmd, para1, para2, para3);
    memset(readbuf, 0, sizeof(readbuf));

    if (writefifo(fifo_name_read, bytes, MAX_CMD_LEN) < MAX_CMD_LEN) {
        result = -2;
    } else if ((result = readfifo(fifo_name_write, readbuf, sizeof(readbuf)))
            < 0) {
        result = -3;
    } else if (strstr((char *) readbuf, "ERROR") != NULL) {
        result = -4;
    }

    env->ReleaseStringUTFChars(name, fifo_name);

    return result;
}

static jstring sendcmdForStrResult(JNIEnv *env, jobject thiz, jstring name,
        jshort cmd, jshort para1, jshort para2, jshort para3) {
    const char *fifo_name;

    int fifo;
    uint8_t bytes[8] = { 0 };
    uint8_t readbuf[128];
    jint result = 0;
    jstring str;

    fifo_name = env->GetStringUTFChars(name, NULL);
    if (name == NULL) {
        return NULL;
    }

    pack_n_int(bytes, 4, cmd, para1, para2, para3);
    memset(readbuf, 0, sizeof(readbuf));

    if (writefifo(fifo_name, bytes, MAX_CMD_LEN) < MAX_CMD_LEN) {
        result = -2;
    } else if ((result = readfifo(fifo_name, readbuf, sizeof(readbuf))) < 0) {
        result = -3;
    } else if (strstr((char *) readbuf, "ERROR") != NULL) {
        result = -4;
    }

    if (strlen((char *) readbuf) > 0) {
        str = env->NewStringUTF((char *) readbuf);
    } else {
        str = NULL;
    }

    env->ReleaseStringUTFChars(name, fifo_name);

    return str;
}

static const char *classPathName = "com/EngineeringMode/JRDRapi";

static JNINativeMethod methods[] = { { "sendcmd", "(Ljava/lang/String;SSSS)I",
        (void*) sendcmd } };

/*
 * Register several native methods for one class.
 */
static int registerNativeMethods(JNIEnv* env, const char* className,
        JNINativeMethod* gMethods, int numMethods) {
    jclass clazz;

    clazz = env->FindClass(className);
    if (clazz == NULL) {
        TCTALOGE("Native registration unable to find class '%s'", className);
        return JNI_FALSE;
    }
    if (env->RegisterNatives(clazz, gMethods, numMethods) < 0) {
        TCTALOGE("RegisterNatives failed for '%s'", className);
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

/*
 * Register native methods for all classes we know about.
 *
 * returns JNI_TRUE on success.
 */
static int registerNatives(JNIEnv* env) {
    if (!registerNativeMethods(env, classPathName, methods,
            sizeof(methods) / sizeof(methods[0]))) {
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

// ----------------------------------------------------------------------------

/*
 * This is called by the VM when the shared library is first loaded.
 */

typedef union {
    JNIEnv* env;
    void* venv;
} UnionJNIEnvToVoid;

jint JNI_OnLoad(JavaVM* vm, void* reserved) {
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;

    ALOGI("JNI_OnLoad");

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_4) != JNI_OK) {
        TCTALOGE("ERROR: GetEnv failed");
        goto bail;
    }
    env = uenv.env;

    if (registerNatives(env) != JNI_TRUE) {
        TCTALOGE("ERROR: registerNatives failed");
        goto bail;
    }

    result = JNI_VERSION_1_4;

    bail: return result;
}
