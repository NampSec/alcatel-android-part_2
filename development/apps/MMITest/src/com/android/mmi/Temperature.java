/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Temperature.java   */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.NumberFormat;

import com.android.mmi.util.TestItemManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

public class Temperature extends TestBase {
    private final static int VALUE_TEMPERATURE_DEFALUT = 0;
    private final static int VALUE_TEMPERATURE_DEFAULT_UNIT = 10;
    private final static int VALUE_VOLTAGE_DEFALUT = 0;
    private final static String Battery_Present_FILE = "/sys/class/power_supply/battery/present";
    private String stateString;
    private float voltage=0;
    private int present = 0;
    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;
    private boolean mForceFail = false;
    private String mBatteryIdString=null;

    private NumberFormat numformat;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        present = getPresent();
        mForceFail = (present == 0);

        broadcastReceiver = new MemberReceiver();
        intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);

        stateString = mContext.getString(R.string.battery_message);

        if(TestItemManager.getProduct().equals("idol4s_cn")) {
            mBatteryIdString = getBatteryId();
            setDefTextMessage(String.format(stateString,
                    VALUE_TEMPERATURE_DEFALUT,voltage,"",0)
                    +"\n"+mBatteryIdString);
        }
        else {
            setDefTextMessage(String.format(stateString,
                    VALUE_TEMPERATURE_DEFALUT,voltage,"",0));
        }
        numformat=NumberFormat.getPercentInstance();
        numformat.setMaximumFractionDigits(2);
    }

    @Override
    public void resume() {
        mContext.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void pause() {
        mContext.unregisterReceiver(broadcastReceiver);
    }

    private String getStateString(Intent intent,float level,String percent,int present){
        return String.format(stateString, getTemperature(intent),level,percent,present);
    }

    private int getTemperature(Intent intent) {
        int temperature = (intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,
                VALUE_TEMPERATURE_DEFALUT))/10;
        if( temperature<20 || temperature >50 ) {
            mForceFail = true;
        }
        return temperature;//temperature /= VALUE_TEMPERATURE_DEFAULT_UNIT;
    }
    private int getPresent(){
        InputStreamReader in = null;
        int mpresent = 0;
        try {
            File file = new File(Battery_Present_FILE);
            in = new InputStreamReader(new FileInputStream(file));
            mpresent = in.read()-48;
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mpresent;
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Inner Classes
    // //////////////////////////////////////////////////////////////////////////////
    private class MemberReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent intent) {
            voltage=(float)intent.getIntExtra(
             BatteryManager.EXTRA_VOLTAGE,VALUE_VOLTAGE_DEFALUT);
            int rawlevel=intent.getIntExtra("level",-1);
            int scale=intent.getIntExtra("scale",-1);
            String percent="";
            if(rawlevel>=0 && scale>0){
                percent=numformat.format((rawlevel*1.0)/scale);
            }

            if(TestItemManager.getProduct().equals("idol4s_cn")) {
                setDefTextMessage(getStateString(intent,voltage,percent,present)
                        +"\n"+mBatteryIdString);
            }
            else {
                setDefTextMessage(getStateString(intent,voltage,percent,present));
            }
            if(mForceFail) {
                setPassButtonEnable(false);
            }
            else {
                setPassButtonEnable(true);
            }
        }
    }

    private String getBatteryId() {
        int resistance = 0;
        String result;
        try {
            BufferedReader bufReader = new BufferedReader(new FileReader("/sys/class/power_supply/bms/resistance_id"));
            String info = bufReader.readLine();
            resistance = Integer.parseInt(info);
            bufReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (NumberFormatException e) {
            //
        }

        // Idol4s_cn BYD: 33K +-5% is 31350~34650 ohm
        if( 31350<resistance && resistance<34650 ) {
            result = String.format("Battery ID: %d ohm(OK)", resistance);
        }
        else {
            result = String.format("Battery ID: %d ohm(NOK)", resistance);
            mForceFail = true;
        }
        return result;
    }
}
