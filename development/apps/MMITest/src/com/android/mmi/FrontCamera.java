/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/FrontCamera.java   */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Handler;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;

import com.android.mmi.camera.CameraTextureView;

public class FrontCamera extends TestBase implements CameraTextureView.TextureViewListener{
    private TextView mCameraText;
    protected int mTargetCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
    private int mCurrentCameraId = -1;

    private Handler objHandler = new Handler();
    protected CameraTextureView mCameraTextureView;
    protected boolean match_result = true;

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);

        setContentView(R.layout.test_camera, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);

        hideNavigationBar();
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        setButtonGone();
        mCameraText = (TextView) findViewById(R.id.camera_text);

        TextureView textureView = (TextureView) findViewById(R.id.preview_content);
        CameraInfo info = initCameraInfo();
        if (mCurrentCameraId == -1) {
            showText(R.string.cam_no_camera);
            return;
        }
        mCameraTextureView = new CameraTextureView(mContext, textureView, mCurrentCameraId, info);
        mCameraTextureView.setTextureViewListener(this);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        if(mCameraTextureView != null){
            mCameraTextureView.setPause();
        }
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        if(mCameraTextureView != null){
            mCameraTextureView.setDestroy();
        }
        resetConfirmed();
    }

    private void showText(int resId) {
        mCameraText.setText(resId);
        setButtonAnimateVisible();
        mCameraText.setVisibility(View.VISIBLE);
        setPassButtonEnable(false);
    }

    @Override
    public void setButton(int timeout){
        objHandler.postDelayed(mRestartTask, timeout);
    }

    @Override
    public void setText(int resId){
        showText(resId);
    }

    private CameraInfo initCameraInfo() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == mTargetCameraId) {
                mCurrentCameraId = camIdx;
                return cameraInfo;
            }
        }
        return null;
    }

    private Runnable mRestartTask = new Runnable() {

        @Override
        public void run() {
             // TODO Auto-generated method stub
            setButtonVisible();
            setButtonTransparent();
            setPassButtonEnable(match_result);
        }
    };
}
