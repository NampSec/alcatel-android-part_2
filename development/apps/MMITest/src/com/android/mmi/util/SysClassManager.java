/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/util/              */
/*                                                       SysClassManager.java */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;


public class SysClassManager {
    public final String LOG = "MMITest.SysClassManager";
    public static final String FLASH_LED_FILE = "/sys/class/leds/led-flash/brightness";
    public static final String LCD_FILE = "/sys/class/leds/lcd-backlight/brightness";
    public static final String KEY_FILE = "/sys/class/leds/button-backlight/brightness";
//    public static final String LED_GREEN_FILE = "/sys/class/leds/green/brightness";
    public static final String FLASH_LED_FILE_1 = "/sys/class/leds/led_torch/brightness";
    public static final String FLASH_LED_FILE_2 = "/sys/class/leds/led:torch_0/brightness";
    public static final String FLASH_LED_FILE_3 = "/sys/class/leds/led:torch_1/brightness";
    public static final String FLASH_LED_SWITCH = "/sys/class/leds/led:switch/brightness";

    public static final String LED_CHARGE_LED = "/sys/class/leds/red/brightness";

    public static final String LED_ORANGE_FILE = "/sys/class/leds/led_G/brightness";

    public static final String SENSOR_ENABLE = "/sys/class/sensors/tsl2772-ps/enable";
    public static final String SENSOR_PRX_RAW = "/sys/class/sensors/tsl2772-ps/device/prx_raw";

    public static final String GYROSCOPE_SELF_TEST_FILE = "/sys/class/sensors/bmg160/device/selftest";

    public static final String BOARD_ID_STATUS_FILE = "/sys/class/board_id_status/status";

    public static final String NPI_DOWNLOAD = "/sys/class/npi_down_status/status";

//    public static final String LED_ORANGE_FILE = "/sys/class/leds/green/brightness";
//    public static final String LED_RED_FILE = "/sys/class/leds/red/brightness";

    public static final String LED_duty_pcts = "/sys/class/leds/green/duty_pcts";
    public static final String LED_ramp_step_ms = "/sys/class/leds/green/ramp_step_ms";
    public static final String LED_blink = "/sys/class/leds/green/blink";
    public static final String LED_duty_pcts_red = "/sys/class/leds/red/duty_pcts";
    public static final String LED_ramp_step_ms_red = "/sys/class/leds/red/ramp_step_ms";
    public static final String LED_blink_red = "/sys/class/leds/red/blink";

    public static final String VIBRATOR = "/sys/class/timed_output/vibrator/enable";
    public static final String BATTERY_USB = "/sys/class/power_supply/usb/online";
    public static final String BATTERY_AC = "/sys/class/power_supply/pm8921-dc/online";
    public static final String CHARGE_NOW = "/sys/class/power_supply/battery/current_now";
    public static final String CHARGE_STATUS = "/sys/class/power_supply/battery/status";

    public static final String UPDATE_CHARGE_NOW = "/sys/class/power_supply/bms/update_now";

    public static final String AREA_ID = "/sys/class/rio4g_devinfo/area_id/area_id_info";

    public static final String TP_FIRMWARE_FILE_IDOL4S = "/sys/class/tp_device/tp_fw/fw_version";

    public static final String SUPPLIER_TP = "/proc/tct_devices/TP";
    public static final String SUPPLIER_LCD = "/sys/class/lcd_id/value";
    public static final String SUPPLIER_PRI_CAMERA = "/proc/tct_devices/Primary_Camera";
    public static final String SUPPLIER_SEC_CAMERA = "/proc/tct_devices/Secondary_Camera";

    public static final String FILE_USBTYPE_C = "/sys/devices/soc/7575000.i2c/i2c-1/1-0050/rdinfo";

    public static final int MIN = 0;
    public static final int NORMAL = 180;
    public static final int MAX = 255;
    public static final int FLASH = 127;

    public static final int SWITCH_OFF = 0;
    public static final int SWITCH_ON = 1;

    private static SysClassManager mManager;
    private FileOutputStream fileOutputStream = null;
    private FileInputStream fileInputStream = null;
    private int bright = -1;

    public static SysClassManager getInstance() {
        if (mManager == null) {
            mManager = new SysClassManager();
        }
        return mManager;
    }

    public SysClassManager() {
    }

    public FileOutputStream fileOutputOpen(String path) {
        FileOutputStream fos = null;

        try {
            File file = new File(path);
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            MMILog.e(LOG, "openOutputStream error: "+path);
            e.printStackTrace();
            return fos;
        }
        return fos;
    }

    public FileInputStream fileInputOpen(String path) {
        FileInputStream fis = null;

        try {
            File file = new File(path);
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            MMILog.e(LOG, "openInputStream error: "+path);
            e.printStackTrace();
            return fis;
        }
        return fis;
    }

    public boolean BrightSetting(int value) {
        if (fileOutputStream != null) {
            try {
                fileOutputStream.write(Integer.toString(value).getBytes());
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        } else {
            MMILog.e(LOG, "fileOutputStream is null");
            return false;
        }
    }

    public boolean WriteFileOutputChar(String  value) {
        if (fileOutputStream != null) {
            try {
                fileOutputStream.write(value.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        } else {
            MMILog.e(LOG, "fileOutputStream is null");
            return false;
        }
    }

    public int readFileInputStream() {
        int range = -1;
        if (fileInputStream == null) {
            return -1;
        }
        try {
            range = fileInputStream.read();
        } catch (IOException e) {
            MMILog.e(LOG, "InputStream read error");
            e.printStackTrace();
            return -1;
        }

        return range;
    }

    public byte[] readFileInputByte() {
        if(fileInputStream != null){
            byte[] range = new byte[128];
            int ret = -1;
            try {
                ret = fileInputStream.read(range);
                if (ret <= 0) {
                    return null;
                }
            } catch (IOException e) {
                MMILog.e(LOG, "InputStream read error");
                e.printStackTrace();
                return null;
            }

            byte[] readBytes = new byte[ret];
            System.arraycopy(range, 0, readBytes, 0, ret);
            range = null;
            return readBytes;
        }

        return null;
    }

    public String readFileInputString() {
        byte[] range = new byte[128];
        int ret = -1;
        try {
            ret = fileInputStream.read(range);
            if (ret == -1) {
                return "";
            }
            else {
                return new String(range, 0, ret, StandardCharsets.US_ASCII);
            }
        } catch (IOException e) {
            MMILog.e(LOG, "InputStream read error");
            e.printStackTrace();
            return "";
        }
    }

    public void fileOutputClose() {
        if (fileOutputStream != null) {
            try {
                fileOutputStream.flush();
                fileOutputStream.close();
                fileOutputStream = null;
            } catch (IOException e) {
                MMILog.e(LOG, "outputStreamClose error");
                e.printStackTrace();
            }
        }
    }

    public void fileInputClose() {
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
                fileInputStream = null;
            } catch (IOException e) {
                MMILog.e(LOG, "inputStreamClose error");
                e.printStackTrace();
            }
        }
    }

    /**
     * @return the fileOutputStream
     */
    public FileOutputStream getFileOutputStream() {
        return fileOutputStream;
    }

    /**
     * @return the fileInputStream
     */
    public FileInputStream getFileInputStream() {
        return fileInputStream;
    }

    /**
     * @param fileOutputStream
     *            the fileOutputStream to set
     */
    public void setFileOutputStream(FileOutputStream fileOutputStream) {
        this.fileOutputStream = fileOutputStream;
    }

    /**
     * @param fileInputStream
     *            the fileInputStream to set
     */
    public void setFileInputStream(FileInputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    /**
     * @return the lcdLight
     */
    public int getBright() {
        return bright;
    }

    /**
     * @return the keyLight
     */
    public void setBright(int bright) {
        this.bright = bright;
    }

    public void initBacklight(String path) {
        if (bright == -1) {
            setBright(path);
        }
        setFileOutputStream(fileOutputOpen(path));
    }

    public void backLightOn(String path, int value) {
        if (bright == -1) {
            setBright(path);
        }
        setFileOutputStream(fileOutputOpen(path));
        BrightSetting(value);
        fileOutputClose();
    }

    public void setBright(String path) {
        setFileInputStream(fileInputOpen(path));
        setBright(readFileInputStream());
        fileInputClose();
    }

}
