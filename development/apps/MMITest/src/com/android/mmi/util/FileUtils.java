/******************************************************************************/
/*                                                               Date:09/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Xiaoqi Wu                                                         */
/* E-Mail:  xiaoqi.wu@tcl.com                                                 */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/util/FileUtils.java*/
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 10/09/14| Xiaoqi.Wu      |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import android.content.Context;

public class FileUtils {

    private String assertIni = "";
    private String filesIni = "";
    Context mContext;

    public FileUtils(Context mContext, String initName, String nowName) {
        // TODO Auto-generated constructor stub
        this.mContext = mContext;
        this.assertIni = initName;
        this.filesIni = nowName;

        File dirs = new File("/storage/sdcard0/Rawdata/");
        if (!dirs.exists()) {
            dirs.mkdirs();
        }

        if (!fileIsExitWithPath(filesIni)) {
            copyIniFromAssetsToFiles(assertIni, filesIni);
        }

    }

    public boolean fileIsExitWithPath(String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                return true;
            }
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
        return false;
    }

    public boolean deleteFileWithPath(String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }
        return true;
    }

    public void copyIniFromAssetsToFiles(String from, String to) {
        try {
            int bytesum = 0;
            int byteread = 0;
            InputStream is = mContext.getAssets().open(from);
            FileOutputStream fs = new FileOutputStream(to);
            byte[] buffer = new byte[1444];
            while ((byteread = is.read(buffer)) != -1) {
                bytesum += byteread;
                System.out.println(bytesum);
                fs.write(buffer, 0, byteread);
            }
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
