/*
 * Copyright 2008-2016 Synaptics, Inc.
 */

package com.android.mmi.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
//import android.widget.Toast;

//import java.util.Locale;

import com.android.mmi.R;

import com.synaptics.fingerprint.CapturedImageData;
import com.synaptics.fingerprint.DeviceInfo;
import com.synaptics.fingerprint.Fingerprint;
import com.synaptics.fingerprint.FingerprintEvent;
import com.synaptics.fingerprint.IdentifyResult;
import com.synaptics.fingerprint.VcsObjectArray;
import com.synaptics.fingerprint.SensorTest;

/**
 * Sample code to demonstrate the usage of Synaptics Java APIs
 */
public class FpApiHelper implements Fingerprint.EventListener {
    public static final int MSG_SCIPT_TEST_PASS = 0;
    public static final int MSG_SCIPT_TEST_FAIL = 1;
    public static final int MSG_LOG_INFO = 2;
    public static final int MSG_SCIPT_TEST_SENSOR_INFO = 3;
    public static final int MSG_IDENTFY_SHOW_IMG = 4;

    private static final String TAG = "MMITest.FpApiHelper";

    private static final int OPERATION_NONE = 0;
    private static final int OPERATION_IDENTIFY = 1;
    private static final int OPERATION_IDENTIFY_LOOP = 2;

    private Context mContext;

    private Fingerprint mFingerprint;

    private int mCurrentOperation = OPERATION_NONE;

    /**
     * Holds the completion status of current operation. It is used to cancel
     * the operation when a new operation is requested or closing the app.
     *
     * Identify is an asynchronous API and is completed with
     * EVT_EIV_STATUS_IDENTIFY_COMPLETED event in callback.
     */
    private boolean mOperationComplete = true;

    private Handler mHandler = new Handler();

    private SensorTestScripts mSnsrScriptIds = new SensorTestScripts();
    private int mCurrentScriptId = 0;
    private int mScriptToExecute = 0;
    private int mTestResult = Fingerprint.VCS_SNSR_TEST_CODE_FAIL_BIN;
    private boolean mIsScriptTestInProgress = false;
    private Handler mUiHandler = null;

    /**
     * Constructor
     */
    public FpApiHelper(Context context, Handler handler) {
        mContext = context;
        mUiHandler = handler;
    }

    /**
     * Create Fingerprint API class instance
     */
    public void init() {
        if (mFingerprint == null) {
            mFingerprint = new Fingerprint(mContext, FpApiHelper.this);
        }
    }

    /**
     * Get Version
     */
    public boolean doGetVersion() {

        // Create Fingerprint object if not already created; And cancel if any
        // pending operation.
        doPreprocess();

        String version = mFingerprint.getVersion();
        if (version != null) {
            setUiString("Version:" + version);
        } else {
            setUiString(mContext.getString(R.string.getversion_failed));
        }
        mOperationComplete = true;
        return true;
    }

    /**
     * Request sensor device information
     */
    public boolean doRequestSensorInfo() {

        // Create Fingerprint object if not already created; And cancel if any
        // pending operation.
        doPreprocess();

        int result = mFingerprint.request(Fingerprint.VCS_REQUEST_GET_SENSOR_INFO, null);
        if (result == Fingerprint.VCS_RESULT_OK) {
            setUiString(mContext.getString(R.string.operation_success));
        } else {
            setUiString(mContext.getString(R.string.api_failed) + getResultCodeDescription(result));
        }
        return (result == Fingerprint.VCS_RESULT_OK);
    }

    /**
     * Identify
     */
    public boolean doIdentify() {

        if (isDuplicateRequest(OPERATION_IDENTIFY)) {
            setUiString(mContext.getString(R.string.duplicate_identify_request));
            return false;
        }

        // Create Fingerprint object if not already created; And cancel if any
        // pending operation.
        doPreprocess();

        mCurrentOperation = OPERATION_IDENTIFY;

        setUiString("Start identify...");

        int result = mFingerprint.identify(""); // Async call
        if (result == Fingerprint.VCS_RESULT_OK) {
            setUiString(mContext.getString(R.string.identify_api_success));
            mOperationComplete = false;
        } else {
            setUiString(mContext.getString(R.string.identify_api_failed) + "Result code:" + result);
        }

        return (result == Fingerprint.VCS_RESULT_OK);
    }

    /**
     * Identify-Loop
     */
    public void doIdentifyLoop(int delayMillis) {

        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (doIdentify()) {
                    mCurrentOperation = OPERATION_IDENTIFY_LOOP;
                }
            }

        }, delayMillis);

    }

    /**
     * Get list of enrolled users
     */
    public void doGetUserList() {

        if (mCurrentOperation == OPERATION_IDENTIFY) {
            init();
        } else {
            doPreprocess();
        }

        VcsObjectArray userlist = new VcsObjectArray();

        int result = mFingerprint.getUserList(userlist);

        if (result != Fingerprint.VCS_RESULT_OK) {
            setUiString(mContext.getString(R.string.api_failed) + getResultCodeDescription(result));
            return;
        }

        if (userlist == null || userlist.list == null || userlist.list.size() == 0) {
            setUiString(mContext.getString(R.string.users_not_exist));
            return;
        }

        setUiString("No. of Users: " + userlist.list.size());

        // Users
        int i = 0;
        for (byte[] user : userlist.list) {
            setUiString("User [" + (i++) + "] = " + bytesToHexString(user));
        }

        return;
    }


    /**
     * Enable/disable logging
     */
    public boolean doRequestLogging(final boolean enable) {

        // Create Fingerprint object if not already created; And cancel if any
        // pending operation.
        doPreprocess();

        // request id
        int requestId = enable ? Fingerprint.VCS_REQUEST_ENABLE_LOGGING
                : Fingerprint.VCS_REQUEST_DISABLE_LOGGING;

        // call request API
        int result = mFingerprint.request(requestId, null);

        // format ui message
        String message = "";
        if (result == Fingerprint.VCS_RESULT_OK) {
            message = mContext.getString(enable ? R.string.enable_logging_success
                    : R.string.disable_logging_success);
        } else {
            message = mContext.getString(enable ? R.string.enable_logging_failed
                    : R.string.disable_logging_failed) + getResultCodeDescription(result);
        }

        // show result in UI
        setUiString(message);

        return (result == Fingerprint.VCS_RESULT_OK);
    }

    /**
     * Cancel pending operation
     */
    public boolean doCancel() {

        mHandler.removeCallbacksAndMessages(null);

        if (mFingerprint == null) {
            MMILog.w(TAG, "Fingerprint API instance is null");
            mOperationComplete = true;
            return false;
        }

        if (mOperationComplete) {
            MMILog.w(TAG, "No operation pending");
            return false;
        }

        int result = mFingerprint.cancel();
        if (result == Fingerprint.VCS_RESULT_OK) {
            setUiString(mContext.getString(R.string.cancel_success));
        } else {
            setUiString(mContext.getString(R.string.cancel_failed)
                    + getResultCodeDescription(result));
        }

        return (result == Fingerprint.VCS_RESULT_OK);
    }

    /**
     * Cleanup Fingerprint object
     */
    public void doCleanup() {

        if (mFingerprint != null) {
            mFingerprint.cleanUp();
            mFingerprint = null;
        }

        mOperationComplete = true;
    }

    /**
     * If identify operation is being cancelled, wait until the operation is
     * complete. The identify, verify operations completed with callback events
     * Fingerprint.EVT_EIV_STATUS_IDENTIFY_COMPLETED and
     * Fingerprint.EVT_EIV_STATUS_VERIFY_COMPLETED respectively.
     *
     * This method initiates wait() and notified from callback method when
     * operation is complete. The maximum wait time is 100 mill-seconds.
     */
    private void waitForCancelComplete() {

        if (mCurrentOperation != OPERATION_IDENTIFY) {
            return;
        }

        if (!mOperationComplete) {
            try {
                wait(100);
            } catch (InterruptedException e) {
                MMILog.e(TAG, "Interrupted " + e);
            }
        }
    }

    /**
     * Method for initialization. And to cancel pending operation.
     */
    private void doPreprocess() {

        init();

        // Cancel if any fingerprint operation is pending
        cancelPendingOperation();

    }

    /**
     * Cancel if any pending fingerprint operation. Get user list and Get finger
     * list operations are allowed during Identify.
     */
    private synchronized void cancelPendingOperation() {

        // If no operation pending, do nothing.
        if (mOperationComplete) {
            return;
        }

        setUiString(mContext.getString(R.string.cancel_prev_operation));

        // Cancel operation
        doCancel();

        // If identify operation is being cancelled, wait until they are
        // completely finished.
        waitForCancelComplete();

        // reset operation flag
        mOperationComplete = true;

    }

    /**
     * Callback method
     */
    public synchronized void onEvent(FingerprintEvent event) {

        if (event == null) {
            MMILog.e(TAG, "Invalid event");
            return;
        }

        MMILog.i(TAG, "onEvent(): Event Id = " + event.eventId);

        String statusMessage = "";

        switch (event.eventId) { // Event Id

        case Fingerprint.EVT_DEVICE_STATE_READY:
            statusMessage = mContext.getString(R.string.sensor_ready);
            break;

        case Fingerprint.EVT_DEVICE_STATE_UNAVAILABLE:
            statusMessage = mContext.getString(R.string.sensor_unavailable);
            break;

        case Fingerprint.EVT_DEVICE_INFO:
            onDeviceInfo((DeviceInfo) event.eventData);
            break;

        case Fingerprint.EVT_CAPTURE_STATUS_ARMED:
            statusMessage = mContext.getString(R.string.please_place);
            break;

        case Fingerprint.EVT_CAPTURE_STATUS_FINGER_ON_SENSOR:
            statusMessage = mContext.getString(R.string.finger_on_sensor);
            break;

        case Fingerprint.EVT_CAPTURE_STATUS_FINGER_SETTLED:
            statusMessage = mContext.getString(R.string.lift_finger);
            break;

        case Fingerprint.EVT_CAPTURE_STATUS_COMPLETED:
            onCaptureComplete((CapturedImageData) event.eventData);
            break;

        case Fingerprint.EVT_EIV_STATUS_BAD_CAPTURE:
            statusMessage = mContext.getString(R.string.fingerprint_captured_bad);
            if (event.eventData != null && (event.eventData instanceof Integer)) {
                statusMessage += "[Quality flags: "
                        + Integer.toHexString((Integer) event.eventData) + "]";
            }
            break;

        case Fingerprint.EVT_EIV_STATUS_IDENTIFY_COMPLETED:
            onIdentifyComplete((IdentifyResult) event.eventData);
            notify();
            break;

        case Fingerprint.EVT_SNSR_TEST_SCRIPT_START:
            MMILog.i(TAG, mContext.getString(R.string.script_start));
            break;

        case Fingerprint.EVT_SNSR_TEST_SCRIPT_END:
            MMILog.i(TAG, mContext.getString(R.string.script_end));
            onScriptEndHandler(event);
            break;

        default:
            MMILog.i(TAG, "Event Unhandled: " + event.eventId);
            break;

        } // switch (event.eventId)

        if (!statusMessage.equals("")) {
            setUiString(statusMessage);
        }

    } // -------------- End of Callback method onEvent()--------------------

    /**
     * Request sesor script test
     */
    public boolean doRequestSensorScriptTest(int iScriptId, boolean isScriptToExcute) {

        //Create Fingerprint object if not already created; And cancel if any pending operation.
        doPreprocess();

        SensorTest snsrTest = new SensorTest();
        mCurrentScriptId = snsrTest.scriptId = iScriptId;
        if(isScriptToExcute) {
            mScriptToExecute = mCurrentScriptId;
        }
        //Shut down script
        if (iScriptId == Fingerprint.VCS_SNSR_TEST_SHUTDOWN_SCRIPT_ID) {
            snsrTest.options = 0;
            snsrTest.dataLogOpt = 0;
            snsrTest.unitId = 0;
        }
        else {
            snsrTest.options = Fingerprint.VCS_SNSR_TEST_STOP_ON_FAIL_FLAG;
            snsrTest.dataLogOpt = Fingerprint.VCS_SNSR_TEST_DATALOG_TESTS_FLAG
                    | Fingerprint.VCS_SNSR_TEST_DATALOG_FILE_CREATE_FLAG
                    | Fingerprint.VCS_SNSR_TEST_DATALOG_FILE_KEEP_OLD_FILE_FLAG;
            snsrTest.unitId = 0;
        }
        MMILog.i(TAG, String.format("SensorTest: Script[%d], Options[%d] , DataLogOptions[%d], UnitId[%d]",
                snsrTest.scriptId, snsrTest.options, snsrTest.dataLogOpt, snsrTest.unitId));
        int ret = mFingerprint.request(Fingerprint.VCS_REQUEST_COMMAND_SENSOR_TEST, snsrTest);
        if (ret == Fingerprint.VCS_RESULT_OK) {
            mIsScriptTestInProgress = true;
            MMILog.i(TAG, "script(id="+iScriptId+") requested successfully");
        } else {
            MMILog.i(TAG, "script(id="+iScriptId+") request failed. Result :" + ret);
        }
        if (iScriptId == Fingerprint.VCS_SNSR_TEST_SHUTDOWN_SCRIPT_ID) {
            mIsScriptTestInProgress = false;
        }

        return ret == Fingerprint.VCS_RESULT_OK;
    }

    /**
     * Handler for the script completion event (EVT_SNSR_TEST_SCRIPT_END).
     * This method evaluates what to do next for the chosen script in the UI.
     */
    private void onScriptEndHandler(FingerprintEvent event) {

        //If No-stimulus script chosen and is completed, execute shutdown script to finish the test.
        if (mSnsrScriptIds.isNoStimulusScriptChosenAndComplete(mCurrentScriptId, mScriptToExecute)) {

            //Save the test result to a member
            if (event != null && event.eventData != null && event.eventData instanceof Integer) {
                //mTestResult = getResultString((Integer) event.eventData);
                mTestResult = ((Integer) event.eventData).intValue();
            } else {
                //mTestResult = mContext.getString(R.string.test_bin_failed);
                mTestResult = Fingerprint.VCS_SNSR_TEST_CODE_FAIL_BIN;
            }

            doRequestSensorScriptTest(Fingerprint.VCS_SNSR_TEST_SHUTDOWN_SCRIPT_ID, false);
        }

        //If Stimulus script chosen and No-stimulus script is currently completed,
        //Prompt a dialog to put stimulus on sensor to proceed.
        else if (mSnsrScriptIds.isStimulusScriptChosenAndNoStimulusScriptComplete(mCurrentScriptId, mScriptToExecute)) {

            //mSnsrTest.scriptId = mScriptToExecute;
            //showPromptDialog(STIMULUS_ACTION_PUT);

        }

        //If Stimulus script chosen and is currently completed, prompt a dialog to remove stimulus from sensor
        //and then execute shutdown script to finish the test.
        else if (mSnsrScriptIds.isStimulusScriptChosenAndComplete(mCurrentScriptId, mScriptToExecute)) {

            //Save the test result to a member
            //if (event != null && event.eventData != null && event.eventData instanceof Integer) {
            //    mTestResult = getResultString((Integer) event.eventData);
            //} else {
            //    mTestResult = getString(R.string.test_bin_failed);
            //}

            //Prompt to remove stimulus from sensor
            //showPromptDialog(STIMULUS_ACTION_REMOVE);

            //Execute Shutdown script finally
            //mSnsrTest.scriptId = Fingerprint.VCS_SNSR_TEST_SHUTDOWN_SCRIPT_ID;
            //doSensorTest();

        }


        //If Shutdown script is currently completed, show test result
        else if (mCurrentScriptId == Fingerprint.VCS_SNSR_TEST_SHUTDOWN_SCRIPT_ID) {
            String result = getResultString(mTestResult);
            mIsScriptTestInProgress = false;
            MMILog.i(TAG, "Result:" + result);
            setUiString(result, mTestResult==Fingerprint.VCS_SNSR_TEST_PASS_BIN?MSG_SCIPT_TEST_PASS:MSG_SCIPT_TEST_FAIL);
        }

    }

    /**
     * Return description for the supplied result code.
     */
    private String getResultString(int binResult) {
        MMILog.i(TAG, "binResult=" + binResult);
        String result = "";

        if (binResult == Fingerprint.VCS_SNSR_TEST_PASS_BIN) {
            result = mContext.getString(R.string.test_passed);
            return  " [" + result + "]";
        }

        switch (binResult) {
        case Fingerprint.VCS_SNSR_TEST_CODE_FAIL_BIN:
            result = mContext.getString(R.string.internal_failure);
            break;
        case Fingerprint.VCS_SNSR_TEST_USER_STOP_FAIL_BIN:
            result = mContext.getString(R.string.stopped_testing);
            break;
        case Fingerprint.VCS_SNSR_TEST_NO_TEST_SCRIPT_FAIL_BIN:
            result = mContext.getString(R.string.no_test_script);
            break;
        case Fingerprint.VCS_SNSR_TEST_NO_CALLBACK_FAIL_BIN:
            result = mContext.getString(R.string.no_cb);
            break;
        case Fingerprint.VCS_SNSR_TEST_INFO_CHCK_FAIL_BIN:
            result = mContext.getString(R.string.check_fail);
            break;
        case Fingerprint.VCS_SNSR_TEST_WRONG_SECT_TYPE_VER_BIN:
            result = mContext.getString(R.string.wrong_sec_type_ver);
            break;

        case Fingerprint.VCS_SNSR_TEST_DATA_CLCT_FAIL_BIN:
            result = mContext.getString(R.string.data_clct_fail);
            break;

        case Fingerprint.VCS_SNSR_TEST_DATA_MISSING_FAIL_BIN:
            result = mContext.getString(R.string.data_missing_fail);
            break;

        case Fingerprint.VCS_SNSR_TEST_VAL_SNSR_FAIL_BIN:
            result = mContext.getString(R.string.snsr_fail);
            break;

        case Fingerprint.VCS_SNSR_TEST_SER_NUM_FAIL_BIN:
            result = mContext.getString(R.string.ser_num_fail);
            break;

        default:
            //result = getString(R.string.test_bin_failed);
            MMILog.w(TAG, "Unknown result code" + result);
            break;
        }
        if (!result.equals("")) {
            result = mContext.getString(R.string.test_bin_failed) + " [" + result + "]";
        } else {
            result = mContext.getString(R.string.test_bin_failed);
        }
        return result;
    }

    /**
     * Device info callback event (EVT_DEVICE_INFO) handler
     */
    private void onDeviceInfo(DeviceInfo sensorInfo) {

        if (sensorInfo != null) {

            String statusMessage = mContext.getString(R.string.sensor_info_received);

            //Product Id
            statusMessage += "\nProduct Id: " + Integer.toHexString(sensorInfo.productId);

            // Proeject Id or Sensor Device Id
            //statusMessage += "\nProject Id: " + Integer.toHexString(sensorInfo.projectId );

            //Flex Id
            statusMessage += "\nFlex Id: " + sensorInfo.flexId;

            //Serial Number
            statusMessage += String.format("\nSerial Number: %02x%02x%02x%02x%02x%02x",
                            sensorInfo.serialNumber[0], sensorInfo.serialNumber[1],
                            sensorInfo.serialNumber[2], sensorInfo.serialNumber[3],
                            sensorInfo.serialNumber[4], sensorInfo.serialNumber[5]);

            //Firmware version
            statusMessage += "\nFirmware Version:" + sensorInfo.fwVersion;

            //Firmware extension version
            if (sensorInfo.fwExtVersion != null) {
                statusMessage += "\nFirmware Extension Version:" + sensorInfo.fwExtVersion;
            }

            //Silicon version
            if (sensorInfo.siliconVersion != 0) {
                statusMessage += "\nSilicon Version:" + sensorInfo.siliconVersion;
            }

            MMILog.i(TAG, statusMessage);
            setUiString(sensorInfo);
        } else {
            MMILog.e(TAG, "Invalid sensor info");
            setUiString("Invalid sensor info", MSG_SCIPT_TEST_FAIL);
        }

        mOperationComplete = true;
    }

    /**
     * Capture complete callback event (EVT_CAPTURE_STATUS_COMPLETED) handler
     */
    private void onCaptureComplete(CapturedImageData imageData) {
        if (imageData == null) {
            MMILog.e(TAG, "Invalid fingerprint object");
            return;
        }

        String qltyMsg = mContext.getString(R.string.fingerprint_captured) + " "
                + getImgQualityDesc(imageData.quality);
        setUiString(qltyMsg);

        // display image in UI
        setUiImage(imageData);
    }

    /**
     * Identify complete callback event (EVT_EIV_STATUS_IDENTIFY_COMPLETED)
     * handler
     */
    private void onIdentifyComplete(IdentifyResult identifyResult) {

        if (identifyResult == null) {
            mOperationComplete = true;
            mCurrentOperation = OPERATION_NONE;
            MMILog.e(TAG, "Invalid event data");
            return;
        }

        String statusMessage = "";

        if (identifyResult.result == Fingerprint.VCS_RESULT_OK) {

            statusMessage = mContext.getString(R.string.identify_success);

        } else {
            statusMessage = mContext.getString(R.string.identify_failed)
                    + getResultCodeDescription(identifyResult.result);
        }

        if (!statusMessage.equals("")) {
            setUiString(statusMessage);
        }

        mOperationComplete = true;

        if (mCurrentOperation == OPERATION_IDENTIFY_LOOP
                && identifyResult.result != Fingerprint.VCS_RESULT_GEN_OPERATION_CANCELED) {
            doIdentifyLoop(50);
        } else {
            mCurrentOperation = OPERATION_NONE;
        }

    }

    /**
     * Return the result code description for the supplied result code.
     */
    private String getResultCodeDescription(int resultCode) {

        String message = " Result code: " + resultCode + " [";

        switch (resultCode) {

        // success (no errors)
        case Fingerprint.VCS_RESULT_OK:
            message += mContext.getString(R.string.result_ok);
            break;

        // General errors
        case Fingerprint.VCS_RESULT_GEN_CORE_NOT_INITIALIZED:
            message += mContext.getString(R.string.result_gen_core_not_initialized);
            break;

        case Fingerprint.VCS_RESULT_GEN_BUSY:
            message += mContext.getString(R.string.result_gen_busy);
            break;

        case Fingerprint.VCS_RESULT_GEN_OPERATION_CANCELED:
            message += mContext.getString(R.string.result_gen_op_cancelled);
            break;

        case Fingerprint.VCS_RESULT_GEN_OPERATION_DENIED:
            message += mContext.getString(R.string.result_gen_op_denied);
            break;

        case Fingerprint.VCS_RESULT_GEN_NO_MATCH:
            message += mContext.getString(R.string.result_gen_no_match);
            break;

        case Fingerprint.VCS_RESULT_GEN_FINGER_NOT_ENROLLED:
            message += mContext.getString(R.string.result_gen_finger_not_enrolled);
            break;

        case Fingerprint.VCS_RESULT_GEN_SIGNATURE_VERIFICATION_FAILED:
            message += mContext.getString(R.string.result_gen_sign_verify_failed);
            break;

        case Fingerprint.VCS_RESULT_GEN_BAD_PARAM:
            message += mContext.getString(R.string.result_gen_bad_param);
            break;

        case Fingerprint.VCS_RESULT_GEN_NULL_POINTER:
            message += mContext.getString(R.string.result_gen_null_pointer);
            break;

        case Fingerprint.VCS_RESULT_GEN_NOT_IMPLEMENTED:
            message += mContext.getString(R.string.result_gen_not_implemented);
            break;

        case Fingerprint.VCS_RESULT_GEN_UNEXPECTED_FORMAT:
            message += mContext.getString(R.string.result_gen_unexpected_format);
            break;

        case Fingerprint.VCS_RESULT_GEN_NO_MORE_ENTRIES:
            message += mContext.getString(R.string.result_gen_no_more_entries);
            break;

        case Fingerprint.VCS_RESULT_GEN_BUFFER_TOO_SMALL:
            message += mContext.getString(R.string.result_gen_buffer_too_small);
            break;

        case Fingerprint.VCS_RESULT_GEN_TIMEOUT:
            message += mContext.getString(R.string.result_gen_timeout);
            break;

        case Fingerprint.VCS_RESULT_GEN_OBJECT_DOESNT_EXIST:
            message += mContext.getString(R.string.result_gen_object_doesnt_exist);
            break;

        // Sensor specific errors
        case Fingerprint.VCS_RESULT_SENSOR_RESET:
            message += mContext.getString(R.string.result_snsr_reset);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_MALFUNCTIONED:
            message += mContext.getString(R.string.result_snsr_malfunctioned);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_UNAVAILABLE:
            message += mContext.getString(R.string.result_snsr_unavailable);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_NEED_TO_RESET_OWNER:
            message += mContext.getString(R.string.result_snsr_need_to_reset_owner);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_NOT_READY_FOR_USE:
            message += mContext.getString(R.string.result_snsr_not_ready);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_OUT_OF_OTP_OWNERSHIP:
            message += mContext.getString(R.string.result_snsr_out_of_otp_ownership);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_FW_EXT_PROGRAM_FAILED:
            message += mContext.getString(R.string.result_snsr_fw_ext_program_failed);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_BAD_CMD:
            message += mContext.getString(R.string.result_snsr_bad_cmd);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_DRV_OPEN_FAILED:
            message += mContext.getString(R.string.result_snsr_drv_open_failed);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_TLS_INTERNAL_FAILURE:
            message += mContext.getString(R.string.result_snsr_tls_internal_failure);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_PARTITION_NOT_OPENED:
            message += mContext.getString(R.string.result_snsr_partition_not_opened);
            break;

        case Fingerprint.VCS_RESULT_SENSOR_PARTITION_IS_FULL:
            message += mContext.getString(R.string.result_snsr_partition_full);
            break;

        // Matcher specific errors.
        case Fingerprint.VCS_RESULT_MATCHER_OPEN_FAILED:
            message += mContext.getString(R.string.result_matcher_open_failed);
            break;

        case Fingerprint.VCS_RESULT_MATCHER_MATCH_FAILED:
            message += mContext.getString(R.string.result_matcher_match_failed);
            break;

        case Fingerprint.VCS_RESULT_MATCHER_EXTRACT_FAILED:
            message += mContext.getString(R.string.result_matcher_extract_failed);
            break;

        case Fingerprint.VCS_RESULT_MATCHER_ADD_IMAGE_FAILED:
            message += mContext.getString(R.string.result_matcher_add_image_failed);
            break;

        // database specific error codes.
        case Fingerprint.VCS_RESULT_DB_FULL:
            message += mContext.getString(R.string.result_db_full);
            break;

        case Fingerprint.VCS_RESULT_DB_EMPTY:
            message += mContext.getString(R.string.result_db_empty);
            break;

        // System specific error codes.
        case Fingerprint.VCS_RESULT_SYS_OUT_OF_MEMORY:
            message += mContext.getString(R.string.result_sys_out_of_memory);
            break;

        case Fingerprint.VCS_RESULT_SYS_FILE_OPEN_FAILED:
            message += mContext.getString(R.string.result_sys_file_open_failed);
            break;

        case Fingerprint.VCS_RESULT_SYS_FILE_WRITE_FAILED:
            message += mContext.getString(R.string.result_sys_file_write_failed);
            break;

        case Fingerprint.VCS_RESULT_SYS_FILE_READ_FAILED:
            message += mContext.getString(R.string.result_sys_file_read_failed);
            break;

        default:
            message += "Undefined error";
            break;
        }
        message += "]";

        return message;

    }

    /**
     * Returns the message to show in UI for the supplied Image Quality Flags.
     */
    public String getImgQualityDesc(int qualityFlag) {

        if (Fingerprint.VCS_IMAGE_QUALITY_GOOD == qualityFlag) {
            return mContext.getString(R.string.image_qlty_good);
        }

        String prompt = "\n[Quality flags:" + Integer.toHexString(qualityFlag) + "]";

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_FINGER_OFFSET) ==
                Fingerprint.VCS_IMAGE_QUALITY_FINGER_OFFSET) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_finger_offset);
        }

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_PRESSURE_TOO_LIGHT) ==
                Fingerprint.VCS_IMAGE_QUALITY_PRESSURE_TOO_LIGHT) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_pressure_too_light);
        }

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_FINGER_OFFSET_TOO_FAR_LEFT) ==
                Fingerprint.VCS_IMAGE_QUALITY_FINGER_OFFSET_TOO_FAR_LEFT) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_offset_too_left);
        }

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_FINGER_OFFSET_TOO_FAR_RIGHT) ==
                Fingerprint.VCS_IMAGE_QUALITY_FINGER_OFFSET_TOO_FAR_RIGHT) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_offset_too_right);
        }

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_PRESSURE_TOO_HARD) ==
                Fingerprint.VCS_IMAGE_QUALITY_PRESSURE_TOO_HARD) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_pressure_too_hard);
        }

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_ASP_DATA_INVALID) ==
                Fingerprint.VCS_IMAGE_QUALITY_ASP_DATA_INVALID) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_asp_data_invalid);
        }

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_NOT_A_FINGER) ==
                Fingerprint.VCS_IMAGE_QUALITY_NOT_A_FINGER) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_not_a_finger);
        }

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_BASELINE_DATA_INVALID) ==
                Fingerprint.VCS_IMAGE_QUALITY_BASELINE_DATA_INVALID) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_baseline_data_invalid);
        }

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_FINGER_TOO_THIN) ==
                Fingerprint.VCS_IMAGE_QUALITY_FINGER_TOO_THIN) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_finger_too_thin);
        }

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_PARTIAL_TOUCH) ==
                Fingerprint.VCS_IMAGE_QUALITY_PARTIAL_TOUCH) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_partial_touch);
        }

        if ((qualityFlag & Fingerprint.VCS_IMAGE_QUALITY_EMPTY_TOUCH) ==
                Fingerprint.VCS_IMAGE_QUALITY_EMPTY_TOUCH) {
            prompt += "\n" + mContext.getString(R.string.image_qlty_empty_touch);
        }

        return prompt;
    }

    /**
     * Check if the requested operation is duplicate?
     */
    private boolean isDuplicateRequest(int operation) {
        boolean isDuplicate = false;
        if (mCurrentOperation == operation && mOperationComplete == false) {
            isDuplicate = true;
        }
        return isDuplicate;
    }

    /**
     * Add message to the list view
     */
    public void setUiString(final String msg) {
        //((FpTestOdm) mContext).setUiString(msg);
        setUiString(msg, MSG_LOG_INFO);
        MMILog.i(TAG, msg);
    }

    public void setUiString(DeviceInfo info) {
        Message msgToSend = new Message();
        msgToSend.what = MSG_SCIPT_TEST_SENSOR_INFO;
        msgToSend.obj = info;
        mUiHandler.sendMessage(msgToSend);
    }

    private void setUiString(String msg, int msgId) {
        Message msgToSend = new Message();
        msgToSend.what = msgId;
        msgToSend.obj = msg;
        mUiHandler.sendMessage(msgToSend);
    }

    private void setUiImage(CapturedImageData imageData) {
        Message msgToSend = new Message();
        msgToSend.what = MSG_IDENTFY_SHOW_IMG;
        msgToSend.obj = imageData;
        mUiHandler.sendMessage(msgToSend);
    }

    /**
     * Print bytes.
     */
    private String bytesToHexString(byte[] bytes) {

        if (bytes == null || bytes.length == 0) {
            MMILog.e(TAG, "Empty buffer");
            return "";
        }

        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b & 0xff));
        }

        return sb.toString();
    }

}
