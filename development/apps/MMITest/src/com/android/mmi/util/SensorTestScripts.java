package com.android.mmi.util;

import java.util.ArrayList;

import com.synaptics.fingerprint.Fingerprint;

/**
 * Script IDs of Sensor Test
 */
public class SensorTestScripts  {

    /**
     * Populate Script IDs
     */
//    public ArrayList<ListItem> getScriptIDList() {
//
//        ArrayList<ListItem> list = new ArrayList<ListItem>();
//
//        list.add( new ListItem( Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_NO_STIMULUS_SCRIPT_ID, "Metallica 73A No stimulus [Project-490009/49000A]"));
//
//        list.add( new ListItem( Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_STIMULUS_SCRIPT_ID, "Metallica 73A Stimulus [Project-490009/49000A]"));
//
//        list.add( new ListItem( Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_NO_STIMULUS_SCRIPT_ID, "Metallica 73A No stimulus [Project-490003]"));
//
//        list.add( new ListItem( Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_STIMULUS_SCRIPT_ID, "Metallica 73A Stimulus [Project-490003]"));
//
//        return list;
//
//    }


    /**
     * Return No-Stimulus Script Id for the supplied Stimulus Script ID.
     * If matching "No-stimulus" script id is not found, same script id is returned.
     */
//    public int getNoStimulusScriptId(int scriptId) {
//
//        if (scriptId == Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_STIMULUS_SCRIPT_ID) {   //Metallica 73A sensor
//
//            return Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_NO_STIMULUS_SCRIPT_ID;
//
//        } else if (scriptId == Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_STIMULUS_SCRIPT_ID) {
//
//            return Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_NO_STIMULUS_SCRIPT_ID;
//
//        }
//
//        return scriptId;
//
//    }

    /**
     * Return true if Stimulus script Id was chosen and is completed.
     * Otherwise returns false.
     */
    public boolean isStimulusScriptChosenAndComplete(int scriptIdExecuted, int scriptIdToExecute) {

        if (scriptIdExecuted == Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_STIMULUS_SCRIPT_ID
            && scriptIdToExecute == Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_STIMULUS_SCRIPT_ID) {
            return true;
        } else if (scriptIdExecuted == Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_STIMULUS_SCRIPT_ID
            && scriptIdToExecute == Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_STIMULUS_SCRIPT_ID) {
            return true;
        }

        return false;
    }

    /**
     * Return true if No-Stimulus script Id was chosen and is completed.
     * Otherwise returns false.
     */
    public boolean isNoStimulusScriptChosenAndComplete(int scriptIdExecuted, int scriptIdToExecute) {

        if (scriptIdExecuted == Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_NO_STIMULUS_SCRIPT_ID
            && scriptIdToExecute == Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_NO_STIMULUS_SCRIPT_ID) {
                return true;
        } else if (scriptIdExecuted == Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_NO_STIMULUS_SCRIPT_ID
            && scriptIdToExecute == Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_NO_STIMULUS_SCRIPT_ID) {
                return true;
        }

        return false;

    }

    /**
     * Returns true if No-Stimulus script is executed and Stimulus script need to be executed.
     * Otherwise returns false.
     */
    public boolean isStimulusScriptChosenAndNoStimulusScriptComplete(int scriptIdExecuted, int scriptIdToExecute) {

        if (scriptIdExecuted == Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_NO_STIMULUS_SCRIPT_ID
            && scriptIdToExecute == Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_STIMULUS_SCRIPT_ID) {
            return true;
        } else if (scriptIdExecuted == Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_NO_STIMULUS_SCRIPT_ID
            && scriptIdToExecute == Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_STIMULUS_SCRIPT_ID) {
            return true;
        }

        return false;

    }

    /**
     * Returns true if supplied script id is a Stimulus Script. Otherwise returns false.
     */
    public boolean isStimulusScript(int scriptId) {
        return (scriptId == Fingerprint.VCS_SNSR_TEST_ID179_73A_MET_STIMULUS_SCRIPT_ID
            || scriptId == Fingerprint.VCS_SNSR_TEST_ID5179_73A_MET_STIMULUS_SCRIPT_ID);
    }
}
