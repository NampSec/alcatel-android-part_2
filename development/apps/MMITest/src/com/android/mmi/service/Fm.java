/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Fm.java            */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi.service;

import com.android.mmi.util.MMILog;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioSystem;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import qcom.fmradio.FmReceiver;
import qcom.fmradio.FmRxEvCallbacksAdaptor;
import qcom.fmradio.FmConfig;
import qcom.fmradio.FmTransmitter;
import qcom.fmradio.FmTransmitterCallbacksAdaptor;
import qcom.fmradio.FmRxRdsData;

public class Fm {

    private static final String TAG = "MMITest.Service_Fm";

    private int mFrequency = 103600;
    private Handler objHandler = new Handler();
    private AudioManager am;
    private FmReceiver mFmReceiver;
    private boolean mFMOn = false;

    private static final int AUDIO_SAMPLE_RATE = 44100;
    private static final int AUDIO_CHANNEL_CONFIG =
                                    AudioFormat.CHANNEL_CONFIGURATION_STEREO;
    private static final int AUDIO_ENCODING_FORMAT =
                                            AudioFormat.ENCODING_PCM_16BIT;
    private static final int FM_RECORD_BUF_SIZE =
                       AudioRecord.getMinBufferSize(AUDIO_SAMPLE_RATE,
                                    AUDIO_CHANNEL_CONFIG, AUDIO_ENCODING_FORMAT);

    private Thread mRecordSinkThread = null;
    private AudioRecord mAudioRecord = null;
    private AudioTrack mAudioTrack = null;
    private boolean mIsRecordSink = false;
    private static final int AUDIO_FRAMES_COUNT_TO_IGNORE = 3;
    private Object mRecordSinkLock = new Object();

    private static final String FMRADIO_DEVICE_FD_STRING = "/dev/radio0";

    private Messenger mClientMessenger;
    private Context mContext;

    static {
        System.loadLibrary("qcomfm_jni");
    }

    public Fm(Context context){
        mContext = context;
    }

    public void setMessenger(Messenger toClientMsg) {
        mClientMessenger = toClientMsg;
    }

    private void sendStatus(String setStation) {
        if (mClientMessenger != null) {
            Message toClientMsg = new Message();
            Bundle bundle = new Bundle();

            bundle.putString("setStation", setStation);
            bundle.putInt("getRssi", mFmReceiver.getRssi());

            toClientMsg.setData(bundle);
            try {
                MMILog.d(TAG, "service begin send msg to client");
                mClientMessenger.send(toClientMsg);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void run() {
        // TODO Auto-generated method stub
        am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
    }

    // Receiver callbacks back from the FM Stack
    FmRxEvCallbacksAdaptor mFmCallbacks = new FmRxEvCallbacksAdaptor() {
        public void FmRxEvEnableReceiver() {
            MMILog.d(TAG, "FmRxEvEnableReceiver");
        }

        public void FmRxEvDisableReceiver() {
            MMILog.d(TAG, "FmRxEvEnableReceiver");
        }

        public void FmRxEvConfigReceiver() {
            MMILog.d(TAG, "FmRxEvConfigReceiver");
        }

        public void FmRxEvMuteModeSet() {
            MMILog.d(TAG, "FmRxEvMuteModeSet");
        }

        public void FmRxEvStereoModeSet() {
            MMILog.d(TAG, "FmRxEvStereoModeSet");
        }

        public void FmRxEvRadioStationSet() {
            MMILog.d(TAG, "FmRxEvRadioStationSet");
        }

        public void FmRxEvPowerModeSet() {
            MMILog.d(TAG, "FmRxEvPowerModeSet");
        }

        public void FmRxEvSetSignalThreshold() {
            MMILog.d(TAG, "FmRxEvSetSignalThreshold");
        }

        public void FmRxEvRadioTuneStatus(int frequency) {
            MMILog.d(TAG, "FmRxEvRadioTuneStatus: Tuned Frequency: " + frequency);

        }

        public void FmRxEvStationParameters() {
            MMILog.d(TAG, "FmRxEvStationParameters");
        }

        public void FmRxEvRdsLockStatus(boolean bRDSSupported) {
            MMILog.d(TAG, "FmRxEvRdsLockStatus: " + bRDSSupported);

        }

        public void FmRxEvStereoStatus(boolean stereo) {
            MMILog.d(TAG, "FmRxEvStereoStatus: " + stereo);

        }

        public void FmRxEvServiceAvailable() {
            MMILog.d(TAG, "FmRxEvServiceAvailable");
        }

        public void FmRxEvGetSignalThreshold() {
            MMILog.d(TAG, "FmRxEvGetSignalThreshold");
        }

        public void FmRxEvSearchInProgress() {
            MMILog.d(TAG, "FmRxEvSearchInProgress");
        }

        public void FmRxEvSearchRdsInProgress() {
            MMILog.d(TAG, "FmRxEvSearchRdsInProgress");
        }

        public void FmRxEvSearchListInProgress() {
            MMILog.d(TAG, "FmRxEvSearchListInProgress");
        }

        public void FmRxEvSearchComplete(int frequency) {
            MMILog.d(TAG, "FmRxEvSearchComplete: Tuned Frequency: " + frequency);

        }

        public void FmRxEvSearchRdsComplete() {
            MMILog.d(TAG, "FmRxEvSearchRdsComplete");
        }

        public void FmRxEvSearchListComplete() {
            MMILog.d(TAG, "FmRxEvSearchListComplete");

        }

        public void FmRxEvSearchCancelled() {
            MMILog.d(TAG, "FmRxEvSearchCancelled");
        }

        public void FmRxEvRdsGroupData() {
            MMILog.d(TAG, "FmRxEvRdsGroupData");
        }

        public void FmRxEvRdsPsInfo() {
            MMILog.d(TAG, "FmRxEvRdsPsInfo: ");

        }

        public void FmRxEvRdsRtInfo() {
            MMILog.d(TAG, "FmRxEvRdsRtInfo");
        }

        public void FmRxEvRdsAfInfo() {
            MMILog.d(TAG, "FmRxEvRdsAfInfo");
        }

        public void FmRxEvRdsPiMatchAvailable() {
            MMILog.d(TAG, "FmRxEvRdsPiMatchAvailable");
        }

        public void FmRxEvRdsGroupOptionsSet() {
            MMILog.d(TAG, "FmRxEvRdsGroupOptionsSet");
        }

        public void FmRxEvRdsProcRegDone() {
            MMILog.d(TAG, "FmRxEvRdsProcRegDone");
        }

        public void FmRxEvRdsPiMatchRegDone() {
            MMILog.d(TAG, "FmRxEvRdsPiMatchRegDone");
        }
    };

    public void setFrequency(int frequency){
        mFrequency = frequency;

        if (mFmReceiver != null){
            boolean set = mFmReceiver.setStation(mFrequency);
            sendStatus("");
        }

    }

    public void initFm(int frequency) {

        boolean value = false;
        if (mFmReceiver == null) {
            try {
                MMILog.d(TAG, "create FmReceiver instance");

                mFmReceiver = new FmReceiver(FMRADIO_DEVICE_FD_STRING,
                        mFmCallbacks);

            } catch (InstantiationException e) {
                MMILog.e(TAG, "exception when creating FmReceiver instance");
                e.printStackTrace();
            }
        }

        if (mFmReceiver != null) {
            if (isFmOn()) {
                value = true;
            } else {
                FmConfig mFmConfig = new FmConfig();// modified
                mFmConfig.setRadioBand(0); // FM_US_BAND
                mFmConfig.setEmphasis(0); // FM_DE_EMP75
                mFmConfig.setChSpacing(1); // FM_CHSPACE_100_KHZ
                mFmConfig.setRdsStd(2); // FM_RDS_STD_NONE
                mFmConfig.setLowerLimit(87000);
                mFmConfig.setUpperLimit(108000);

                value = mFmReceiver.enable(mFmConfig, mContext);
                MMILog.d(TAG, "turn on radio " + value);
                if (value) {
                    mFMOn = true;
                }
            }
        }

        if (value) {
            //am.setStreamVolume(AudioManager.STREAM_MUSIC,
            //        am.getStreamVolume(AudioManager.STREAM_MUSIC), 0);
            am.setStreamVolume(AudioManager.STREAM_MUSIC, 8, 0);
            /*-------am.setParameters("Fm-radio=true");------*/
            startFM();
            startRecordSink();
        }

    }

    private synchronized void startRecordSink() {
        MMILog.d(TAG, "startRecordSink "
                        + AudioSystem.getForceUse(AudioSystem.FOR_MEDIA));

        if (mAudioRecord != null) {
            mAudioRecord.stop();
        }
        if (mAudioTrack != null) {
            mAudioTrack.stop();
        }
        startAudioRecordSink();
        createRecordSinkThread();

        mIsRecordSink = true;
        synchronized (mRecordSinkLock) {
            mRecordSinkLock.notify();
        }
   }
    private synchronized void startAudioRecordSink() {
        mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.RADIO_TUNER,
                                       AUDIO_SAMPLE_RATE, AUDIO_CHANNEL_CONFIG,
                                       AUDIO_ENCODING_FORMAT, FM_RECORD_BUF_SIZE);
        mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                                     AUDIO_SAMPLE_RATE, AUDIO_CHANNEL_CONFIG,
                                     AUDIO_ENCODING_FORMAT, FM_RECORD_BUF_SIZE,
                                     AudioTrack.MODE_STREAM);
   }

    private synchronized void stopRecordSink() {
        MMILog.d(TAG, "stopRecordSink");
        mRecordSinkLock = false;
        synchronized (mRecordSinkLock) {
            mRecordSinkLock.notify();
        }
    }

    private synchronized void createRecordSinkThread() {
        if (mRecordSinkThread == null) {
            mRecordSinkThread = new RecordSinkThread();
            mRecordSinkThread.start();
        }
    }

    private synchronized void exitRecordSinkThread() {
        stopRecordSink();
        if (mRecordSinkThread != null) {
            mRecordSinkThread.interrupt();
        }
        mRecordSinkThread = null;
    }

    private boolean isRecordSinking() {
        return mIsRecordSink;
    }

    class RecordSinkThread extends Thread {
        private int mCurrentFrame = 0;
        private boolean isAudioFrameNeedIgnore() {
            return mCurrentFrame < AUDIO_FRAMES_COUNT_TO_IGNORE;
        }

        @Override
        public void run() {
            try {
                byte[] buffer = new byte[FM_RECORD_BUF_SIZE];
                while (!Thread.interrupted()) {
                    if (isRecordSinking()) {
                        // Speaker mode or BT a2dp mode will come here and keep reading and writing.
                        // If we want FM sound output from speaker or BT a2dp, we must record data
                        // to AudioRecrd and write data to AudioTrack.
                        if (mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_STOPPED) {
                            mAudioRecord.startRecording();
                        }

                        if (mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_STOPPED) {
                            mAudioTrack.play();
                        }
                        int size = mAudioRecord.read(buffer, 0, FM_RECORD_BUF_SIZE);
                        // check whether need to ignore first 3 frames audio data from AudioRecord
                        // to avoid pop noise.
                        if (isAudioFrameNeedIgnore()) {
                            mCurrentFrame += 1;
                            continue ;
                        }
                        if (size <= 0) {
                            MMILog.e(TAG, "RecordSinkThread read data from AudioRecord "
                                    + "error size: " + size);
                            continue;
                        }
                        byte[] tmpBuf = new byte[size];
                        System.arraycopy(buffer, 0, tmpBuf, 0, size);
                        // Check again to avoid noises, because RecordSink may be changed
                        // while AudioRecord is reading.
                        if (isRecordSinking()) {
                            mAudioTrack.write(tmpBuf, 0, tmpBuf.length);
                        }
                    } else {
                        // Earphone mode will come here and wait.
                        mCurrentFrame = 0;

                        if (mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
                            mAudioTrack.stop();
                        }

                        if (mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                            mAudioRecord.stop();
                        }

                        synchronized (mRecordSinkLock) {
                            mRecordSinkLock.wait();
                        }
                    }
                }
            } catch (InterruptedException e) {
                MMILog.d(TAG, "RecordSinkThread.run, thread is interrupted, need exit thread");
            } finally {
                if (mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                    mAudioRecord.stop();
                }
                if (mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
                    mAudioTrack.stop();
                }
            }
        }
    }


    /*
     * Turn OFF FM: Disable the FM Host and hardware . .
     *
     * @return true if fm Disable api was invoked successfully, false if the api
     * failed.
     */
    public boolean setFmOff() {
        boolean bStatus = false;
        // This will disable the FM radio device
        exitRecordSinkThread();
        if (mFmReceiver != null) {
            //am.setStreamMute(AudioManager.STREAM_MUSIC,true);
            bStatus = mFmReceiver.disable();
            if (bStatus) {
                mFMOn = false;
                stopFM();
                mFmReceiver = null;
            }
        }
        return bStatus;
    }

    public void startFM() {
        MMILog.d(TAG, "In startFM");

        if (mFmReceiver != null) {
            mFmReceiver.setMuteMode(FmReceiver.FM_RX_UNMUTE);
            am.setStreamMute(AudioManager.STREAM_MUSIC, false);

            boolean set = mFmReceiver.setStation(mFrequency);
            if (set) {
                sendStatus("true");
            } else {
                sendStatus("false");
            }
        }

        AudioSystem.setDeviceConnectionState(AudioSystem.DEVICE_OUT_FM,
                AudioSystem.DEVICE_STATE_AVAILABLE, "", "");
    }

    private void stopFM() {
        AudioSystem.setDeviceConnectionState(AudioSystem.DEVICE_OUT_FM,
                AudioSystem.DEVICE_STATE_UNAVAILABLE, "", "");
    }

    private boolean isFmOn() {
        return mFMOn;
    }
}
