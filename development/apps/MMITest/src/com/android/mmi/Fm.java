/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Fm.java            */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioSystem;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Message;

import com.android.mmi.util.MMILog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import qcom.fmradio.FmReceiver;
import qcom.fmradio.FmRxEvCallbacksAdaptor;
import qcom.fmradio.FmConfig;
import qcom.fmradio.FmTransmitter;
import qcom.fmradio.FmTransmitterCallbacksAdaptor;
import qcom.fmradio.FmRxRdsData;

public class Fm extends TestBase implements OnClickListener{
    public final static int MESSAGE_USERDEFINE = 1024;
    public final static int MESSAGE_TIMEOUT = MESSAGE_USERDEFINE + 1;
    public final static int MESSAGE_TIMECOUNT = MESSAGE_TIMEOUT + 1;
    public final static int LENGTH_TIMEOUT = 15000;
    public final static int LENGTH_SECOND = 1000;
    public final static int LENGTH_SECOND_TIMECOUNT = LENGTH_TIMEOUT
            / LENGTH_SECOND;
    private static final String FMRADIO_DEVICE_FD_STRING = "/dev/radio0";


    private static final int AUDIO_SAMPLE_RATE = 44100;
    private static final int AUDIO_CHANNEL_CONFIG =
                                    AudioFormat.CHANNEL_CONFIGURATION_STEREO;
    private static final int AUDIO_ENCODING_FORMAT =
                                            AudioFormat.ENCODING_PCM_16BIT;
    private static final int FM_RECORD_BUF_SIZE =
                       AudioRecord.getMinBufferSize(AUDIO_SAMPLE_RATE,
                                    AUDIO_CHANNEL_CONFIG, AUDIO_ENCODING_FORMAT);

    private Thread mRecordSinkThread = null;
    private AudioRecord mAudioRecord = null;
    private AudioTrack mAudioTrack = null;
    private boolean mIsRecordSink = false;
    private static final int AUDIO_FRAMES_COUNT_TO_IGNORE = 3;
    private Object mRecordSinkLock = new Object();

    public TextView messageTextview;
    private Handler objHandler = new Handler();
    private AudioManager am;
    private View buttonsView;
    private Button buttonUp;
    private Button buttonDown;

    private FmReceiver mFmReceiver;
    private boolean mRegistered = false;
    private boolean mFMOn = false;

    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;
    private Handler handler;
    private Handler  mrecheck;

    private boolean isHeadsetPlugged;
    private boolean isHeadsetOn;
    private int timeCount;

    private int mFrequency = 103600; // To satisfy HuiZhou factory test
    private int freq1 = 87500;
    private int freq2 = 104700;
    private int freq3 = 103600;

    private int volumeMax;
    private int volumeCurrent;

    private final int FM_HEADSET_INSERT = 0;
    private final int FM_INIT = 1;
    private final int FM_TEST = 2;
    private final int FM_HEADSET_REMOVE = 3;
    private final int FM_HEADSET_REMOVED = 4;
    private int mState;
    private int oldAudioMode;

    static {
        System.loadLibrary("qcomfm_jni");
    }

    @Override
    public void create(CommonActivity a) {
        // TODO Auto-generated method stub
        super.create(a);
        setContentView(R.layout.test_fm, LAYOUTTYPE.LAYOUT_CUST_WITH_PASSFAIL);
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        isHeadsetPlugged = false;
        timeCount = LENGTH_SECOND_TIMECOUNT;
        mState = FM_HEADSET_INSERT;

        messageTextview = (TextView)findViewById(R.id.textview_common_message);
        messageTextview.setText(R.string.fm_insert_wait);

        buttonsView = findViewById(R.id.key_buttons);
        buttonUp = (Button) findViewById(R.id.key_up);
        buttonDown = (Button) findViewById(R.id.key_down);
        buttonUp.setOnClickListener(this);
        buttonDown.setOnClickListener(this);
        buttonUp.setEnabled(true);
        buttonDown.setEnabled(true);

        broadcastReceiver = new MemberReceiver();
        intentFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        handler = new MemberHandler();
        mrecheck = new Handler();

        am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        MMILog.i(TAG,"Maxvolume:"+am.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        volumeMax = 15;
        volumeCurrent = 8;
        saveOldAudioMode();
    }

    private void saveOldAudioMode() {
        oldAudioMode = am.getMode();
    }
    private void loadOldAudioMode() {
        am.setMode(oldAudioMode);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mContext.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mContext.unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        super.destroy();
        loadOldAudioMode();
        setFmOff();
    }

    @Override
    public void onPassClick() {
        if(mState==FM_TEST) {
            processState();
        }
        else {
            super.onPassClick();
        }
    }

    private void onHeadsetPlugChanged() {
        switch (mState) {
        case FM_HEADSET_INSERT:
            if (isHeadsetPlugged) {
                //stopHeadsetPlugCheck();
                processState();
            } else {
                messageTextview.setText(R.string.fm_insert_wait);
            }
            break;
        case FM_INIT:
        case FM_TEST:
            if (!isHeadsetPlugged) {

                //messageTextview.setText("ERROR!!");
                mrecheck.postDelayed(mcloseFM, 1000);
            }
            break;
        case FM_HEADSET_REMOVE:
            if (isHeadsetPlugged) {
                messageTextview.setText(R.string.fm_remove_wait);
            } else {
                //stopHeadsetPlugCheck();
                processState();
            }
            break;
        }
    }

    // Receiver callbacks back from the FM Stack
    FmRxEvCallbacksAdaptor mFmCallbacks = new FmRxEvCallbacksAdaptor() {
        public void FmRxEvEnableReceiver() {
            MMILog.d(TAG, "FmRxEvEnableReceiver");
        }

        public void FmRxEvDisableReceiver() {
            MMILog.d(TAG, "FmRxEvEnableReceiver");
        }

        public void FmRxEvConfigReceiver() {
            MMILog.d(TAG, "FmRxEvConfigReceiver");
        }

        public void FmRxEvMuteModeSet() {
            MMILog.d(TAG, "FmRxEvMuteModeSet");
        }

        public void FmRxEvStereoModeSet() {
            MMILog.d(TAG, "FmRxEvStereoModeSet");
        }

        public void FmRxEvRadioStationSet() {
            MMILog.d(TAG, "FmRxEvRadioStationSet");
        }

        public void FmRxEvPowerModeSet() {
            MMILog.d(TAG, "FmRxEvPowerModeSet");
        }

        public void FmRxEvSetSignalThreshold() {
            MMILog.d(TAG, "FmRxEvSetSignalThreshold");
        }

        public void FmRxEvRadioTuneStatus(int frequency) {
            MMILog.d(TAG, "FmRxEvRadioTuneStatus: Tuned Frequency: " + frequency);

        }

        public void FmRxEvStationParameters() {
            MMILog.d(TAG, "FmRxEvStationParameters");
        }

        public void FmRxEvRdsLockStatus(boolean bRDSSupported) {
            MMILog.d(TAG, "FmRxEvRdsLockStatus: " + bRDSSupported);

        }

        public void FmRxEvStereoStatus(boolean stereo) {
            MMILog.d(TAG, "FmRxEvStereoStatus: " + stereo);

        }

        public void FmRxEvServiceAvailable() {
            MMILog.d(TAG, "FmRxEvServiceAvailable");
        }

        public void FmRxEvGetSignalThreshold() {
            MMILog.d(TAG, "FmRxEvGetSignalThreshold");
        }

        public void FmRxEvSearchInProgress() {
            MMILog.d(TAG, "FmRxEvSearchInProgress");
        }

        public void FmRxEvSearchRdsInProgress() {
            MMILog.d(TAG, "FmRxEvSearchRdsInProgress");
        }

        public void FmRxEvSearchListInProgress() {
            MMILog.d(TAG, "FmRxEvSearchListInProgress");
        }

        public void FmRxEvSearchComplete(int frequency) {
            MMILog.d(TAG, "FmRxEvSearchComplete: Tuned Frequency: " + frequency);

        }

        public void FmRxEvSearchRdsComplete() {
            MMILog.d(TAG, "FmRxEvSearchRdsComplete");
        }

        public void FmRxEvSearchListComplete() {
            MMILog.d(TAG, "FmRxEvSearchListComplete");

        }

        public void FmRxEvSearchCancelled() {
            MMILog.d(TAG, "FmRxEvSearchCancelled");
        }

        public void FmRxEvRdsGroupData() {
            MMILog.d(TAG, "FmRxEvRdsGroupData");
        }

        public void FmRxEvRdsPsInfo() {
            MMILog.d(TAG, "FmRxEvRdsPsInfo: ");

        }

        public void FmRxEvRdsRtInfo() {
            MMILog.d(TAG, "FmRxEvRdsRtInfo");
        }

        public void FmRxEvRdsAfInfo() {
            MMILog.d(TAG, "FmRxEvRdsAfInfo");
        }

        public void FmRxEvRdsPiMatchAvailable() {
            MMILog.d(TAG, "FmRxEvRdsPiMatchAvailable");
        }

        public void FmRxEvRdsGroupOptionsSet() {
            MMILog.d(TAG, "FmRxEvRdsGroupOptionsSet");
        }

        public void FmRxEvRdsProcRegDone() {
            MMILog.d(TAG, "FmRxEvRdsProcRegDone");
        }

        public void FmRxEvRdsPiMatchRegDone() {
            MMILog.d(TAG, "FmRxEvRdsPiMatchRegDone");
        }
    };

    // cross process interact.
    protected Handler eventUpdateHandler = new Handler() {

        public void handleMessage(Message msg) {
            switch (mState) {
            case FM_HEADSET_INSERT:
            case FM_INIT:
                processState();
                break;
            }
        }
    };

    private void setTouchHandler() {
        messageTextview.setClickable(true);
        messageTextview.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                toggleFreq();
            }
        });
    }

    private void setTouchHandler(boolean bo) {
        if (bo == false) {
            messageTextview.setClickable(false);
        } else {
            setTouchHandler();
        }
    }

    private void toggleFreq() {

        if (mFrequency == freq1) {
            mFrequency = freq2;
            am.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0);
            volumeCurrent = 10;
            buttonUp.setEnabled(true);
            buttonDown.setEnabled(true);

        } else if (mFrequency == freq2) {
            mFrequency = freq3;
            am.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0);
            volumeCurrent = 10;
            buttonUp.setEnabled(true);
            buttonDown.setEnabled(true);
        } else{
            mFrequency = freq1;
            am.setStreamVolume(AudioManager.STREAM_MUSIC, 15, 0);
            volumeCurrent = 15;
            buttonUp.setEnabled(false);
            buttonDown.setEnabled(true);
        }
        tuneFreq();
        updateText(0);
    }

    private void tuneFreq() {
        boolean status = false;
        if (mFmReceiver != null)
            status = mFmReceiver.setStation(mFrequency);
        MMILog.d(TAG, "==tuneFreq==" + status);
    }

    private void updateText(int ms) {
        int ff = (mFrequency / 1000);
        int fr = (mFrequency % 1000);

        String s = ff + "." + fr + " MHz." + "\n\nTouch to change frequency";
        messageTextview.setText(s);
    }

    /* *****************************************************************************
     * METHOD check headset insert
     * **********************************************
     * *****************************
     */
    private void startHeadsetPlugCheck() {
        handler.sendEmptyMessageDelayed(MESSAGE_TIMEOUT, LENGTH_TIMEOUT);
        handler.sendEmptyMessageDelayed(MESSAGE_TIMECOUNT, LENGTH_SECOND);
    }

    private void stopHeadsetPlugCheck() {
        handler.removeMessages(MESSAGE_TIMEOUT);
        handler.removeMessages(MESSAGE_TIMECOUNT);
        timeCount = LENGTH_SECOND_TIMECOUNT;
    }

    /* *****************************************************************************
     * METHOD get headset plug state
     * ********************************************
     * *******************************
     */
    private boolean isHeadsetPlugged(Intent intent) {
        int numHeadsetState = intent.getIntExtra(
                Value.KEY_INTENT_STATE_HEADSET,
                Value.STATE_INTENT_HEADSET_UNPLUGGED);
        isHeadsetOn = am.isWiredHeadsetOn();
        if(numHeadsetState == Value.STATE_INTENT_HEADSET_PLUGGED){
            return true;
        }
        else if(isHeadsetOn){
            return true;
        }
        else
            return false;
    }

    /* *****************************************************************************
     * METHOD select pass action
     * ************************************************
     * ***************************
     */

    public void processState() {
        mState++;
        switch (mState) {
        case FM_INIT:
            messageTextview.setText(R.string.fm_init);
            initFm();
            break;
        case FM_TEST:
            buttonsView.setVisibility(View.VISIBLE);
            //mFrequency = freq1;
            //tuneFreq();
            updateText(1500); // set a 1500 ms timer.
            setTouchHandler();
            setPassButtonEnable(true);
            break;
        case FM_HEADSET_REMOVE:
            setPassButtonEnable(false);
            setTouchHandler(false);
            setFmOff();
            messageTextview.setText(R.string.fm_remove_wait);
            buttonsView.setVisibility(View.GONE);
            //startHeadsetPlugCheck();
            break;
        case FM_HEADSET_REMOVED:
            onPassClick();
            break;
        }
    }

    // //////////////////////////////////////////////////////////////////////////////
    // MARK Inner Classes
    // //////////////////////////////////////////////////////////////////////////////
    private class MemberHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
            case MESSAGE_TIMEOUT:
                switch (mState) {
                case FM_HEADSET_INSERT:
                    messageTextview.setText(R.string.fm_insert_fail);
                    break;
                case FM_HEADSET_REMOVE:
                    messageTextview.setText(R.string.fm_remove_fail);
                }
                break;
            case MESSAGE_TIMECOUNT:
                // TODO display timecount(low priority)
                timeCount--;
                handler.sendEmptyMessageDelayed(MESSAGE_TIMECOUNT,
                        LENGTH_SECOND);

                if ((timeCount % 3) == 0) {
                }
                break;
            }
        }
    }

    private class MemberReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_HEADSET_PLUG)) {
                isHeadsetPlugged = isHeadsetPlugged(intent);
                MMILog.d(TAG, "headset plug in:"+isHeadsetPlugged);
                onHeadsetPlugChanged();
            }
        }
    }

    private void initFm() {


        boolean value = false;
        if (mFmReceiver == null) {
            try {
                MMILog.d(TAG, "create FmReceiver instance");

                mFmReceiver = new FmReceiver(FMRADIO_DEVICE_FD_STRING,
                        mFmCallbacks);

            } catch (InstantiationException e) {
                MMILog.e(TAG, "exception when creating FmReceiver instance");
                e.printStackTrace();
            }
        }

        if (mFmReceiver != null) {
            if (isFmOn()) {
                value = true;
            } else {
                FmConfig mFmConfig = new FmConfig();// modified
                mFmConfig.setRadioBand(0); // FM_US_BAND
                mFmConfig.setEmphasis(0); // FM_DE_EMP75
                mFmConfig.setChSpacing(1); // FM_CHSPACE_100_KHZ
                mFmConfig.setRdsStd(2); // FM_RDS_STD_NONE
                mFmConfig.setLowerLimit(87000);
                mFmConfig.setUpperLimit(108000);

                value = mFmReceiver.enable(mFmConfig, mContext);
                MMILog.d(TAG, "turn on radio " + value);
                if (value) {
                    mFMOn = true;
                }
            }
        }

        if (value) {
            //am.setStreamVolume(AudioManager.STREAM_MUSIC,
            //        am.getStreamVolume(AudioManager.STREAM_MUSIC), 0);
            am.setStreamVolume(AudioManager.STREAM_MUSIC, 8, 0);
            /*-------am.setParameters("Fm-radio=true");------*/
            startFM();
            startRecordSink();
        }

    }

    private synchronized void startRecordSink() {
        MMILog.d(TAG, "startRecordSink "
                        + AudioSystem.getForceUse(AudioSystem.FOR_MEDIA));

        if (mAudioRecord != null) {
            mAudioRecord.stop();
        }
        if (mAudioTrack != null) {
            mAudioTrack.stop();
        }
        startAudioRecordSink();
        createRecordSinkThread();

        mIsRecordSink = true;
        synchronized (mRecordSinkLock) {
            mRecordSinkLock.notify();
        }
   }


    private synchronized void startAudioRecordSink() {
        mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.RADIO_TUNER,
                                       AUDIO_SAMPLE_RATE, AUDIO_CHANNEL_CONFIG,
                                       AUDIO_ENCODING_FORMAT, FM_RECORD_BUF_SIZE);
        mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                                     AUDIO_SAMPLE_RATE, AUDIO_CHANNEL_CONFIG,
                                     AUDIO_ENCODING_FORMAT, FM_RECORD_BUF_SIZE,
                                     AudioTrack.MODE_STREAM);
   }

    private synchronized void stopRecordSink() {
        MMILog.d(TAG, "stopRecordSink");
        mRecordSinkLock = false;
        synchronized (mRecordSinkLock) {
            mRecordSinkLock.notify();
        }
    }

    private synchronized void createRecordSinkThread() {
        if (mRecordSinkThread == null) {
            mRecordSinkThread = new RecordSinkThread();
            mRecordSinkThread.start();
        }
    }

    private synchronized void exitRecordSinkThread() {
        stopRecordSink();
        if (mRecordSinkThread != null) {
            mRecordSinkThread.interrupt();
        }
        mRecordSinkThread = null;
    }

    private boolean isRecordSinking() {
        return mIsRecordSink;
    }

    class RecordSinkThread extends Thread {
        private int mCurrentFrame = 0;
        private boolean isAudioFrameNeedIgnore() {
            return mCurrentFrame < AUDIO_FRAMES_COUNT_TO_IGNORE;
        }

        @Override
        public void run() {
            try {
                byte[] buffer = new byte[FM_RECORD_BUF_SIZE];
                while (!Thread.interrupted()) {
                    if (isRecordSinking()) {
                        // Speaker mode or BT a2dp mode will come here and keep reading and writing.
                        // If we want FM sound output from speaker or BT a2dp, we must record data
                        // to AudioRecrd and write data to AudioTrack.
                        if (mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_STOPPED) {
                            mAudioRecord.startRecording();
                        }

                        if (mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_STOPPED) {
                            mAudioTrack.play();
                        }
                        int size = mAudioRecord.read(buffer, 0, FM_RECORD_BUF_SIZE);
                        // check whether need to ignore first 3 frames audio data from AudioRecord
                        // to avoid pop noise.
                        if (isAudioFrameNeedIgnore()) {
                            mCurrentFrame += 1;
                            continue ;
                        }
                        if (size <= 0) {
                            MMILog.e(TAG, "RecordSinkThread read data from AudioRecord "
                                    + "error size: " + size);
                            continue;
                        }
                        byte[] tmpBuf = new byte[size];
                        System.arraycopy(buffer, 0, tmpBuf, 0, size);
                        // Check again to avoid noises, because RecordSink may be changed
                        // while AudioRecord is reading.
                        if (isRecordSinking()) {
                            mAudioTrack.write(tmpBuf, 0, tmpBuf.length);
                        }
                    } else {
                        // Earphone mode will come here and wait.
                        mCurrentFrame = 0;

                        if (mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
                            mAudioTrack.stop();
                        }

                        if (mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                            mAudioRecord.stop();
                        }

                        synchronized (mRecordSinkLock) {
                            mRecordSinkLock.wait();
                        }
                    }
                }
            } catch (InterruptedException e) {
                MMILog.d(TAG, "RecordSinkThread.run, thread is interrupted, need exit thread");
            } finally {
                if (mAudioRecord.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
                    mAudioRecord.stop();
                }
                if (mAudioTrack.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) {
                    mAudioTrack.stop();
                }
            }
        }
    }

    /*
     * Turn OFF FM: Disable the FM Host and hardware . .
     *
     * @return true if fm Disable api was invoked successfully, false if the api
     * failed.
     */
    private boolean setFmOff() {
        boolean bStatus = false;

        // This will disable the FM radio device
        if (mFmReceiver != null) {
            //am.setStreamMute(AudioManager.STREAM_MUSIC,true);
            bStatus = mFmReceiver.disable();
            if (bStatus) {
                mFMOn = false;
                stopFM();
                mFmReceiver = null;
            }
        }
        return bStatus;
    }

    private void startFM() {
        MMILog.d(TAG, "In startFM");

        if (mFmReceiver != null) {
            /*--if ((mFmReceiver.setMuteMode(FmReceiver.FM_RX_UNMUTE))&& --*/
//[BUGFIX]-Add-BEGIN by TCTNJ.hongda.zhu,02/19/2014,583192
            mFmReceiver.setMuteMode(FmReceiver.FM_RX_UNMUTE);
//[BUGFIX]-Add-BEGIN by TCTNJ.hongda.zhu,02/19/2014,583192
            am.setStreamMute(AudioManager.STREAM_MUSIC, false);
            if(mFmReceiver.setStation(mFrequency)) {
                messageTextview.setText(R.string.fm_init_success);
                objHandler.postDelayed(mRestartTask, 500);
            } else {
                messageTextview.setText(R.string.fm_turnon_fail);
                setPassButtonEnable(false);
            }
        }

        /*Intent intent = new Intent(Intent.ACTION_FM);
        intent.putExtra("state", 1);
        intent.putExtra("speaker", 0);
        sendBroadcast(intent);*/
        AudioSystem.setDeviceConnectionState(AudioSystem.DEVICE_OUT_FM,
                AudioSystem.DEVICE_STATE_AVAILABLE, "", "");
    }

    private void stopFM() {
        /*MMILog.d(TAG, "In stopFM");
        Intent intent = new Intent(Intent.ACTION_FM);
        intent.putExtra("state", 0);
        sendBroadcast(intent);*/
        AudioSystem.setDeviceConnectionState(AudioSystem.DEVICE_OUT_FM,
                AudioSystem.DEVICE_STATE_UNAVAILABLE, "", "");
    }

    private boolean isFmOn() {
        return mFMOn;
    }

    private Runnable mRestartTask = new Runnable() {

        @Override
        public void run() {
             // TODO Auto-generated method stub
            processState();
        }
     };

     private Runnable mcloseFM = new Runnable() {

         @Override
         public void run() {
              // TODO Auto-generated method stub
             if(!isHeadsetPlugged){
                 exitRecordSinkThread();
                 setFmOff();
               onFailClick();
             }

         }
      };

     public void onClick(View v) {
         // TODO Auto-generated method stub
         switch(v.getId()) {
         case R.id.key_up:
             if (volumeCurrent == 0) {
                 buttonDown.setEnabled(true);
             }
             am.setStreamVolume(AudioManager.STREAM_MUSIC, ++volumeCurrent, 0);
             if (volumeCurrent == volumeMax) {
                 buttonUp.setEnabled(false);
             }
             buttonDown.setEnabled(true);
             break;
         case R.id.key_down:
             if (volumeCurrent == volumeMax) {
                 buttonUp.setEnabled(true);
             }
             am.setStreamVolume(AudioManager.STREAM_MUSIC, --volumeCurrent, 0);
             if (volumeCurrent == 0) {
                 buttonDown.setEnabled(false);
             }
             break;
         default:
             break;
         }
     }
}
