/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/Nfc.java           */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 11/12/14| Shishun.Liu    |                    | Create for NPI_DWN status  */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;

import com.android.mmi.util.MMILog;
import com.android.mmi.util.SysClassManager;

public class NpiDownload extends TestBase {
    private IntentFilter mFilter;
    private SysClassManager mSystemClassManager;
    private final USBReceiever mUSBReceiver = new USBReceiever();
    private int mStatus = -1;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        setDefTextMessage("NPI_Down status:\nUnknown");
        mSystemClassManager = new SysClassManager();
        mFilter = new IntentFilter();
        mFilter.addAction(UsbManager.ACTION_USB_STATE);
    }

    private int getNpiDownloadStatus() {
        int status  = -1;
        mSystemClassManager.setFileInputStream(
                mSystemClassManager.fileInputOpen(SysClassManager.NPI_DOWNLOAD));
        byte[] byteArray = mSystemClassManager.readFileInputByte();
        mSystemClassManager.fileInputClose();
        if(byteArray!=null) {
            try {
                status = Integer.parseInt(new String(byteArray).trim());
            }
            catch(NumberFormatException e) {
                MMILog.e(TAG, e.toString());
            }
        }

        return status;
    }

    public class USBReceiever extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(UsbManager.ACTION_USB_STATE)) {
                int current = getNpiDownloadStatus();
                if (mStatus != current) {
                    if (current <= 0) {
                        NpiDownload.this.setDefTextMessage(
                                "NPI_Down status:\nNOK");
                        NpiDownload.this.setPassButtonEnable(false);
                    } else {
                        NpiDownload.this.setDefTextMessage(
                                "NPI_Down status:\nOK");
                        NpiDownload.this.setPassButtonEnable(true);
                    }
                    mStatus = current;
                }
            }
        }
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        mContext.registerReceiver(mUSBReceiver, mFilter);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        mContext.unregisterReceiver(mUSBReceiver);
    }
}
