/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/CameraFlasher.java */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 03/23/15| Shishun.Liu    |                    | Modified for Selinux eanble*/
/*---------|----------------|--------------------|--------------------------- */
/* 12/14/15| Zhang hong     |                    | Control Led from Camera    */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.android.mmi;

import com.android.mmi.util.MMILog;
import com.android.mmi.util.SysClassManager;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Handler;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CameraFlasher extends TestBase {


    private Camera mCamera;
    private Camera.Parameters mParameters;
    private Method openMethod = null;
    private SurfaceTexture mSurfaceTexture;
    private final static int HWI_API = 0X100;
    // TEST_MODE value should be the format: "param1,param2,param3"
    // param1: on/off, 1-on, 0-off
    // param2: current of camera LED1
    // param3: current of camera LED2
    private final String CAMERA_FLASH_TEST_MODE = "test-dual-led-value";
    private final String TAG = "CameraFlasher";

    private enum CAMERALED {
        FRONT_LED,
        BACK_WARM_LED,
        BACK_COLD_LED,
    }
    private CAMERALED mLED = CAMERALED.BACK_WARM_LED;

    protected void haveFrontLED() {
        mLED = CAMERALED.FRONT_LED;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        mSurfaceTexture = new SurfaceTexture(0);
        try {
            openMethod = Class.forName("android.hardware.Camera").getMethod("openLegacy", int.class, int.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        setDefTextMessage(R.string.flash_message);
        setButtonAnimateVisible(1500);
        setPassButtonEnable(true);
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        super.resume();
        MMILog.d(TAG, "CameraFlasher resume " + mLED.toString());
        if (mLED == CAMERALED.FRONT_LED) {
            openCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
            setParameters(SysClassManager.SWITCH_ON, SysClassManager.MAX, SysClassManager.MAX);
        } else {
            openCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
            if (mLED == CAMERALED.BACK_WARM_LED) {
                setParameters(SysClassManager.SWITCH_ON, SysClassManager.MAX, SysClassManager.MIN);
            } else if (mLED == CAMERALED.BACK_COLD_LED) {
                setParameters(SysClassManager.SWITCH_ON, SysClassManager.MIN, SysClassManager.MAX);
            }
        }
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        super.pause();
        stopCamera();
    }

    @Override
    public void onPassClick() {
        // TODO Auto-generated method stub
        if (mLED == CAMERALED.FRONT_LED) {
            openCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
            setParameters(SysClassManager.SWITCH_ON, SysClassManager.MAX, SysClassManager.MIN);
            mLED = CAMERALED.BACK_WARM_LED;
            setDefTextMessage(R.string.flash_message_2);
            setButtonAnimateVisible(1500);

        } else if(mLED == CAMERALED.BACK_WARM_LED) {
            openCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
            setParameters(SysClassManager.SWITCH_ON, SysClassManager.MIN, SysClassManager.MAX);
            mLED = CAMERALED.BACK_COLD_LED;
            setDefTextMessage(R.string.flash_message_2);
            setButtonAnimateVisible(1500);
        } else {
            super.onPassClick();
        }
    }

    private void openCamera(int cameraId) {
        stopCamera();
        MMILog.d(TAG, "do: open camera");
        try {
            mCamera = (Camera) openMethod.invoke(null, cameraId, HWI_API);
            //It'll got some error in release camera without preview texture
            mCamera.setPreviewTexture(mSurfaceTexture);
            mCamera.startPreview();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        MMILog.d(TAG, "done: open camera");
    }

    private void setParameters(int _switch, int warm, int cold) {
        MMILog.d(TAG, "do: setParameters");
        mParameters = mCamera.getParameters();
        mParameters.set(CAMERA_FLASH_TEST_MODE, _switch + "," + warm + "," + cold);
        mCamera.setParameters(mParameters);
        if (_switch == SysClassManager.SWITCH_ON)
            mParameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        else
            mParameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        mCamera.setParameters(mParameters);
        MMILog.d(TAG, "done: setParameters");
    }

    public void stopCamera() {
        if (mCamera != null) {
            MMILog.d(TAG,"do: stop camera");
            setParameters(SysClassManager.SWITCH_OFF, SysClassManager.MIN, SysClassManager.MIN);
            mCamera.release();
            mCamera = null;
            MMILog.d(TAG,"done: stop camera");
        }
    }
}
