/******************************************************************************/
/*                                                            Date:27/09/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Long Na                                                         */
/*  Email  :  na.long@tcl-mobile.com                                          */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : Application using for bluetooth test mode                      */
/*  File     : development/apps/BluetoothTestMode/src/com/BluetoothTestMode/B */
/*             luetoothTestMode.java                                          */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/09/2013|Qianbo Pan            |FR512357              |Add bluetooth test*/
/*           |                      |                      |mode api          */
/* ----------|----------------------|----------------------|----------------- */
/* ========================================================================== */

package com.android.BluetoothTestMode;

import com.android.BluetoothTestMode.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
//[BUGFIX]-Add by TCTNB.Chen Ji,02/28/2013,408621,
//bluetooth test mode channel setting
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Spinner;
import android.bluetooth.TctExtBluetoothAdapter;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.RemoteException;
import android.os.IBinder;
import android.os.Handler;
import android.os.Message;

public class BluetoothTestMode extends Activity implements AdapterView.OnItemSelectedListener {

    private Context mContext;
    private BluetoothAdapter mBtAdapter;
    private final String TAG = "BluetoothTestMode";
    private static final int ENABLE_BT_TEST_MODE_DELAY = 3;

    private Button mButton01 = null;
    private Button mButton02 = null;
//[BUGFIX]-Add by TCTNB.Chen Ji,02/28/2013,408621,
//bluetooth test mode channel setting
    private Spinner mSpinner = null;
    private int mBluetoothChannel = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        // Get the local Bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        setContentView(R.layout.main);

        mButton01 = (Button) findViewById(R.id.Button01);
        mButton02 = (Button) findViewById(R.id.Button02);
//[BUGFIX]-Add-BEGIN by TCTNB.Chen Ji,02/28/2013,408621,
//bluetooth test mode channel setting
        mSpinner = (Spinner) findViewById(R.id.Bluetooth_Channel_Spinner);
        mSpinner.setSelection(0, true);
        mSpinner.setOnItemSelectedListener(this);
//[BUGFIX]-Add-END by TCTNB.Chen Ji
        mButton01.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mButton01.setEnabled(false);
                mButton02.setEnabled(true);
                if (!mBtAdapter.isEnabled()) {
                    mBtAdapter.enable();
                    synchronized(mHandler) {
                        if(mHandler.hasMessages(ENABLE_BT_TEST_MODE_DELAY)){
                            mHandler.removeMessages(ENABLE_BT_TEST_MODE_DELAY);
                        }
                        mHandler.sendEmptyMessageDelayed(ENABLE_BT_TEST_MODE_DELAY, 5000);
                    }
                    Toast.makeText(BluetoothTestMode.this, "BT is turning on, please wait...", Toast.LENGTH_SHORT).show();
                }else {
                    //modify by liangfeng.xu for bluetooth test mode 001 begin
                    TctExtBluetoothAdapter mTctExtBluetoothAdapter = new TctExtBluetoothAdapter(mBtAdapter);
                    int ret = mTctExtBluetoothAdapter.setBtTestMode(1);
                    //modify by liangfeng.xu for bluetooth test mode 001 end
                    if (ret == -1) {
                        mButton01.setEnabled(true);
                        mButton02.setEnabled(false);
                        Toast.makeText(BluetoothTestMode.this, "Test mode enable failed", Toast.LENGTH_SHORT).show();
                        }
                    Log.d(TAG, "BT already ON!   enableBtTestMode " + ret);
                }
            }
        });

        mButton02.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
                mButton01.setEnabled(true);
                mButton02.setEnabled(false);
                if (mBtAdapter.isEnabled()) {
                    //modify by liangfeng.xu for bluetooth test mode 002 begin
                    TctExtBluetoothAdapter mTctExtBluetoothAdapter = new TctExtBluetoothAdapter(mBtAdapter);
                    mTctExtBluetoothAdapter.setBtChannel(-1);
                    mTctExtBluetoothAdapter.setBtTestMode(0);
                    //modify by liangfeng.xu for bluetooth test mode 002 end
                    mBtAdapter.disable();
                    //add by liangfeng.xu for bluetooth test mode begin
                    if(mHandler.hasMessages(ENABLE_BT_TEST_MODE_DELAY)){
                        mHandler.removeMessages(ENABLE_BT_TEST_MODE_DELAY);
                    }
                    //add by liangfeng.xu for bluetooth test mode end
                    Toast.makeText(BluetoothTestMode.this, "BT disabled...", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(BluetoothTestMode.this, "BT already OFF!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /**
     * Called when the activity will start interacting with the user.
     */
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

    /**
     * Called when the system is about to start resuming a previous activity.
     */
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

    }

    /**
     * The final call you receive before your activity is destroyed.
     */
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if (mBtAdapter.isEnabled()) {
            //modify by liangfeng.xu for bluetooth test mode 003 begin
            TctExtBluetoothAdapter mTctExtBluetoothAdapter = new TctExtBluetoothAdapter(mBtAdapter);
            mTctExtBluetoothAdapter.setBtChannel(-1);
            mTctExtBluetoothAdapter.setBtTestMode(0);
            //modify by liangfeng.xu for bluetooth test mode 003 end
            mBtAdapter.disable();
         }
    }

//[BUGFIX]-Add-BEGIN by TCTNB.Chen Ji,02/28/2013,408621,
//bluetooth test mode channel setting
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == mSpinner) {
            mBluetoothChannel = position;
            if (!mBtAdapter.isEnabled()) {
                mBtAdapter.enable();
             }
            //modify by liangfeng.xu for bluetooth test mode 004 begin
            TctExtBluetoothAdapter mTctExtBluetoothAdapter = new TctExtBluetoothAdapter(mBtAdapter);
            int ret = mTctExtBluetoothAdapter.setBtChannel(-1);
            Log.d(TAG,"setBtChannel end test " + ret);
            ret = mTctExtBluetoothAdapter.setBtChannel(mBluetoothChannel);
            //modify by liangfeng.xu for bluetooth test mode 004 end
            Log.d(TAG,"setBtChannel start test " + ret);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //
    }
//[BUGFIX]-Add-END by TCTNB.Chen Ji

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == ENABLE_BT_TEST_MODE_DELAY){
                //modify by liangfeng.xu for bluetooth test mode 005 begin
                TctExtBluetoothAdapter mTctExtBluetoothAdapter = new TctExtBluetoothAdapter(mBtAdapter);
                int ret = mTctExtBluetoothAdapter.setBtTestMode(1);
                //modify by liangfeng.xu for bluetooth test mode 005 end
                if (ret == -1) {
                    mButton01.setEnabled(true);
                    mButton02.setEnabled(false);
                    Toast.makeText(BluetoothTestMode.this, "Test mode enable failed", Toast.LENGTH_SHORT).show();
                }
                Log.d(TAG, "enableBtTestMode " + ret);
            }
        }
    };

}