/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.providers.media;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.StaleDataException;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.ComponentName;
import android.content.DialogInterface.OnKeyListener;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.StaleDataException;
import android.media.AudioManager;
import android.os.Build;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

/**
 * The {@link RingtonePickerActivity} allows the user to choose one from all of the
 * available ringtones. The chosen ringtone's URI will be persisted as a string.
 *
 * @see RingtoneManager#ACTION_RINGTONE_PICKER
 */
public final class RingtonePickerActivity extends AlertActivity implements
        AdapterView.OnItemSelectedListener, Runnable, DialogInterface.OnClickListener,
        AlertController.AlertParams.OnPrepareListViewListener {

    private static final int POS_UNKNOWN = -1;
    private static final int VF_ERR_RTN_VAL = -2;  //[SOLUTION]-ADD-By TCTNB.wen.zhuang, 08/16/2016, SOLUTION-2548012

    private static final String TAG = "RingtonePickerActivity";

    private static final int DELAY_MS_SELECTION_PLAYED = 300;

    private static final String SAVE_CLICKED_POS = "clicked_pos";

    private RingtoneManager mRingtoneManager;
    private int mType;

    private Cursor mCursor;
    private Handler mHandler;

    /** The position in the list of the 'Silent' item. */
    private int mSilentPos = POS_UNKNOWN;

    /** The position in the list of the 'Default' item. */
    private int mDefaultRingtonePos = POS_UNKNOWN;

    /** The position in the list of the last clicked item. */
    private int mClickedPos = POS_UNKNOWN;

    /** The position in the list of the ringtone to sample. */
    private int mSampleRingtonePos = POS_UNKNOWN;

    /** Whether this list has the 'Silent' item. */
    private boolean mHasSilentItem;

    /** The Uri to place a checkmark next to. */
    private Uri mExistingUri;

    /** The number of static items in the list. */
    private int mStaticItemCount;

    /** Whether this list has the 'Default' item. */
    private boolean mHasDefaultItem;

    /** The Uri to play when the 'Default' item is clicked. */
    private Uri mUriForDefaultItem;

    /**
     * A Ringtone for the default ringtone. In most cases, the RingtoneManager
     * will stop the previous ringtone. However, the RingtoneManager doesn't
     * manage the default ringtone for us, so we should stop this one manually.
     */
    private Ringtone mDefaultRingtone;

    /**
     * The ringtone that's currently playing, unless the currently playing one is the default
     * ringtone.
     */
    private Ringtone mCurrentRingtone;

    private int mAttributesFlags;

    /**
     * Keep the currently playing ringtone around when changing orientation, so that it
     * can be stopped later, after the activity is recreated.
     */
    private static Ringtone sPlayingRingtone;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
    private static final int TYPE_MUSIC = 16;

    private int mMoreRingtones = POS_UNKNOWN;
    private int selectedRingtone = -1;
    private int moreClickedPos = -1;
    private int mSelectedPos = -1;
    private int mRingtonePos = -1;
    private int moreRingtonePos = -1;

    private Uri moreRingtoneUri;
    private Uri mOnCreateUri;

    private boolean mTouchMusicTop;

    private boolean mHasRingtoneItem;
    private Uri nRingtoneUri;

    private Ringtone moreRingtone;
    private RingtoneManager moreRingtoneManager;

    private final String MUSIC_PACKAGE = "com.alcatel.music5";
    private final String MUSIC_PICKER = "com.alcatel.music5.TRACK_PICKER";
    private final String MUSIC_PACKAGE_GMS = "com.google.android.music";
    private final String MUSIC_PICKER_GMS = "com.google.android.music.MusicPicker";
    private final int MORE_RINGTONE_REQUEST = 1000;

    private AudioManager.OnAudioFocusChangeListener mAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            switch(focusChange){
                case AudioManager.AUDIOFOCUS_LOSS:
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    stopAnyPlayingRingtone();
            }
        }
    };
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    private DialogInterface.OnClickListener mRingtoneClickListener =
            new DialogInterface.OnClickListener() {

        /*
         * On item clicked
         */
        public void onClick(DialogInterface dialog, int which) {
            // Save the position of most recently clicked item
            mClickedPos = which;

            if(mClickedPos == mMoreRingtones){
                final Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                // add for 3136323 by liang.xiao begin 2016.1019
                if (isMixAvailable()){
                    intent.setAction(MUSIC_PICKER);
                }else if (isGMSAvailable()){
                    intent.setComponent(new ComponentName(MUSIC_PACKAGE_GMS, MUSIC_PICKER_GMS));
                }
                // add for 3136323 by liang.xiao end 2016.1019
                startActivityForResult(intent,MORE_RINGTONE_REQUEST);
            }

            // Play clip
             else {
                 ((AudioManager) getSystemService(AUDIO_SERVICE))
                     .requestAudioFocus(mAudioFocusChangeListener,
                           AudioManager.STREAM_MUSIC,
                           AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                  playRingtone(which, 0);
             }
        }

    };

    //[BUGFIX]-Mod-BEGIN by AMNJ.fanghua.gu,10/16/2015,Task-528418 cannot add local music as ringtone
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case MORE_RINGTONE_REQUEST:
                    quitBackFromMoreRing(data);
                    ((AudioManager) getSystemService(AUDIO_SERVICE)).abandonAudioFocus(mAudioFocusChangeListener);
                    finish();
                    break;
                default:
            }
        }else{
            ((AudioManager) getSystemService(AUDIO_SERVICE)).abandonAudioFocus(mAudioFocusChangeListener);
            finish();
        }
    }
    private void quitBackFromMoreRing(Intent intent) {
        Intent resultIntent = new Intent();
        Uri uri = intent.getData();
        resultIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI, uri);
        setResult(RESULT_OK, resultIntent);
        //Mod-BEGIN by AMNJ.jinxia.dai,04/15/2016,defect:1908508 Cannot set DRM Audio as ringtone in sound of Settings
        if (uri != null) {
            setRingtone(this.getContentResolver(), uri);
        }
        //Mod-END by AMNJ.jinxia.dai
    }
    //Mod-BEGIN by AMNJ.jinxia.dai,04/15/2016,defect:1908508 Cannot set DRM Audio as ringtone in sound of Settings
    private void setRingtone(ContentResolver resolver, Uri uri) {
        try {
            ContentValues values = new ContentValues(1);
            if (RingtoneManager.TYPE_RINGTONE == mType) {
                values.put(MediaStore.Audio.Media.IS_RINGTONE, "1");
            } else if (RingtoneManager.TYPE_ALARM == mType) {
                values.put(MediaStore.Audio.Media.IS_ALARM, "1");
            } else if (RingtoneManager.TYPE_NOTIFICATION == mType) {
                values.put(MediaStore.Audio.Media.IS_NOTIFICATION, "1");
            } else {
                return;
            }
            resolver.update(uri, values, null, null);
            mExistingUri = uri;
        } catch (UnsupportedOperationException ex) {
            Log.e(TAG, "couldn't set ringtone flag for uri " + uri);
        } catch(IllegalArgumentException ex){
            Log.e(TAG, "couldn't set ringtone flag for uri " + uri);
        }
    }
    //Mod-END by AMNJ.jinxia.dai
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mHandler = new Handler();

        Intent intent = getIntent();
        //Add Begin by yunchong.zhou 01/25/2016 for PR 1449863
        // Give the Activity so it can do managed queries
        mRingtoneManager = new RingtoneManager(this);
     // Get the types of ringtones to show
        mType = intent.getIntExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, -1);
        if (mType != -1) {
            mRingtoneManager.setType(mType);
        }
      //Add End by yunchong.zhou 01/25/2016 for PR 1449863

        /*
         * Get whether to show the 'Default' item, and the URI to play when the
         * default is clicked
         */
        mHasDefaultItem = intent.getBooleanExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
        mUriForDefaultItem = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI);
        if (mUriForDefaultItem == null) {
            //Add by yunchong.zhou 01/25/2016 for PR 1449863
            if (mType == RingtoneManager.TYPE_ALARM) {
                mUriForDefaultItem = Settings.System.DEFAULT_ALARM_ALERT_URI;
            } else {
                mUriForDefaultItem = Settings.System.DEFAULT_RINGTONE_URI;
            }
        }

        if (savedInstanceState != null) {
            mClickedPos = savedInstanceState.getInt(SAVE_CLICKED_POS, POS_UNKNOWN);
        }
        // Get whether to show the 'Silent' item
        mHasSilentItem = intent.getBooleanExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);
        // AudioAttributes flags
        mAttributesFlags |= intent.getIntExtra(
                RingtoneManager.EXTRA_RINGTONE_AUDIO_ATTRIBUTES_FLAGS,
                0 /*defaultValue == no flags*/);

        // Give the Activity so it can do managed queries
        mRingtoneManager = new RingtoneManager(this);

        // Get the types of ringtones to show
        mType = intent.getIntExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, -1);
        if (mType != -1) {
            mRingtoneManager.setType(mType);
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
        mTouchMusicTop = intent.getBooleanExtra("android.intent.extra.ringtone.MUSIC_TOP", true);


        //[BUGFIX]-Mod-BEGIN by AMNJ.fanghua.gu,10/16/2015,Task-528418 cannot add local music as ringtone
        moreRingtoneManager = new RingtoneManager(getBaseContext());
        moreRingtoneManager.setType(TYPE_MUSIC);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        mCursor = mRingtoneManager.getCursor();

        // The volume keys will control the stream that we are choosing a ringtone for
        setVolumeControlStream(mRingtoneManager.inferStreamType());

        // Get the URI whose list item should have a checkmark
        mExistingUri = intent
                .getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
        /* backup the uri */
        mOnCreateUri = mExistingUri;
        Log.v(TAG, "onCreate: mExistingUri=" + mExistingUri);

        showRingItem();
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        final AlertController.AlertParams p = mAlertParams;
        p.mCursor = mCursor;
        p.mOnClickListener = mRingtoneClickListener;
        p.mLabelColumn = MediaStore.Audio.Media.TITLE;
        p.mIsSingleChoice = true;
        p.mOnItemSelectedListener = this;
        p.mPositiveButtonText = getString(com.android.internal.R.string.ok);
        p.mPositiveButtonListener = this;
        p.mNegativeButtonText = getString(com.android.internal.R.string.cancel);
        p.mPositiveButtonListener = this;
        p.mOnPrepareListViewListener = this;

        p.mTitle = intent.getCharSequenceExtra(RingtoneManager.EXTRA_RINGTONE_TITLE);
        if (p.mTitle == null) {
            p.mTitle = getString(com.android.internal.R.string.ringtone_picker_title);
        }

        setupAlert();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVE_CLICKED_POS, mClickedPos);
    }

    public void onPrepareListView(ListView listView) {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
        Log.d(TAG, "onPrepareListView>>>: mClickedPos = " + mClickedPos + ", mExistingUri=" + mExistingUri);
        if(mTouchMusicTop && (isMixAvailable() || isGMSAvailable()) && mType != RingtoneManager.TYPE_ALARM) { // add for 3136323 by liang.xiao begin 2016.1019 // MODIFIED by yong.cai, 2016-11-18,BUG-3421591
            mMoreRingtones = addMoreRingtonesItem(listView);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (mHasDefaultItem) {
            mDefaultRingtonePos = addDefaultRingtoneItem(listView);
            if (mClickedPos == POS_UNKNOWN && RingtoneManager.isDefault(mExistingUri)) {
                mClickedPos = mDefaultRingtonePos;
            }
        }

        if (mHasSilentItem) {
            mSilentPos = addSilentItem(listView);

            // The 'Silent' item should use a null Uri

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
            if (mClickedPos == POS_UNKNOWN && (mExistingUri == null || mExistingUri.toString().equals(""))) {
                mClickedPos = mSilentPos;
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
        if (mHasRingtoneItem) {
            nRingtoneUri = mExistingUri;
            if (mRingtoneManager.getRingtonePosition(nRingtoneUri) == -1 && !isRingtoneTitleEmpty(nRingtoneUri)) {//Defect 1398580 NJ_AM Mitchell Dong 2016-1-14  MOD

                mRingtonePos = addRingtoneItem(listView);
                mClickedPos = mRingtonePos;
                moreRingtonePos = mRingtonePos;
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (mClickedPos == POS_UNKNOWN) {
            mClickedPos = getListPosition(mRingtoneManager.getRingtonePosition(mExistingUri));
        }

        // Put a checkmark next to an item.
        mAlertParams.mCheckedItem = mClickedPos;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
        mSelectedPos = mClickedPos;
        if(!mTouchMusicTop && (isMixAvailable() || isGMSAvailable()) && mType != RingtoneManager.TYPE_ALARM) {  // add for 3136323 by liang.xiao begin 2016.1019 // MODIFIED by yong.cai, 2016-11-18,BUG-3421591
            mMoreRingtones = addMoreRingtonesItem(listView);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

  //Add Begin By yunchong.zhou 02/22/2016 for 1101242
    private boolean isMixAvailable() {
        Intent intent = new Intent();
        intent.setAction(MUSIC_PICKER);
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        return (list.size() > 0);
    }
  //Add End By yunchong.zhou 02/22/2016 for 1101242
    // add for 3136323 by liang.xiao begin 2016.1019
    private boolean isGMSAvailable() {
        Intent intent = new Intent();
        intent.setClassName(MUSIC_PACKAGE_GMS,MUSIC_PICKER_GMS);
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        return (list.size() > 0);
    }
    // add for 3136323 by liang.xiao end 2016.1019
    //Defect 1398580 NJ_AM Mitchell Dong 2016-1-14  ADD BEGIN
    private boolean isRingtoneTitleEmpty(Uri ringtoneUri) {
        String ringtoneTitle = mRingtoneManager.getRingtoneTitle(ringtoneUri);
        return TextUtils.isEmpty(ringtoneTitle);
    }
    //Defect 1398580 NJ_AM Mitchell Dong 2016-1-14  ADD END

    /**
     * Adds a static item to the top of the list. A static item is one that is not from the
     * RingtoneManager.
     *
     * @param listView The ListView to add to.
     * @param textResId The resource ID of the text for the item.
     * @return The position of the inserted item.
     */
    private int addStaticItem(ListView listView, int textResId) {
        TextView textView = (TextView) getLayoutInflater().inflate(
                com.android.internal.R.layout.select_dialog_singlechoice_material, listView, false);
        textView.setText(textResId);
        listView.addHeaderView(textView);
        mStaticItemCount++;
        return listView.getHeaderViewsCount() - 1;
    }

    private int addDefaultRingtoneItem(ListView listView) {
        if (mType == RingtoneManager.TYPE_NOTIFICATION) {
            return addStaticItem(listView, R.string.notification_sound_default);
        } else if (mType == RingtoneManager.TYPE_ALARM) {
            return addStaticItem(listView, R.string.alarm_sound_default);
        }

        return addStaticItem(listView, R.string.ringtone_default);
    }

    private int addSilentItem(ListView listView) {
        return addStaticItem(listView, com.android.internal.R.string.ringtone_silent);
    }

    //[BUGFIX]-Mod-BEGIN by AMNJ.fanghua.gu,10/16/2015,Task-528418 cannot add local music as ringtone
    private int addMoreRingtonesItem(ListView listView) {
        TextView textView = (TextView) getLayoutInflater().inflate(
                R.layout.tct_moreringtone_item, listView, false);
        textView.setText(R.string.add_ringtone);
        //Mod by heng.cao@tcl.com 2015.12.11 for PR1005215 end.
      //Add Begin By yunchong.zhou 01/13/2016 for 1202084
        if (mTouchMusicTop) {
         // modify for 3136333 2016/10/31, we did not has GAPP Mix music ,only has GMS paly music. change the string begin
            textView.setText(R.string.add_ringtone_from_music);
         // modify for 3136333 2016/10/31, we did not has GAPP Mix music ,only has GMS paly music. change the string end
            listView.addHeaderView(textView);
            mStaticItemCount ++;
            return listView.getHeaderViewsCount() - 1;
        } else {
             //Add by yunchong.zhou for 1202084
            textView.setText(R.string.more_ringtone);
            listView.addFooterView(textView);
            return mCursor.getCount()+mStaticItemCount;
        }
        //Add End By yunchong.zhou 01/13/2016 for 1202084
    }

    private int addRingtoneItem(ListView listView) {
        String ringtoneTitle = mRingtoneManager.getRingtoneTitle(nRingtoneUri);
        TextView textView;
        textView = (TextView) getLayoutInflater().inflate(
                    com.android.internal.R.layout.select_dialog_singlechoice_material, listView, false);
        textView.setText(ringtoneTitle);
        listView.addHeaderView(textView);
        mStaticItemCount ++;
        return listView.getHeaderViewsCount() - 1;
    }
    //[BUGFIX]-Mod-END by AMNJ.fanghua.gu

    /*
     * On click of Ok/Cancel buttons
     */
    public void onClick(DialogInterface dialog, int which) {
        boolean positiveResult = which == DialogInterface.BUTTON_POSITIVE;

        // Stop playing the previous ringtone
        mRingtoneManager.stopPreviousRingtone();

        if (positiveResult) {
            Intent resultIntent = new Intent();
            Uri uri = null;

            if (mClickedPos == mDefaultRingtonePos) {
                // Set it to the default Uri that they originally gave us
                uri = mUriForDefaultItem;
            } else if (mClickedPos == mSilentPos) {
                // A null Uri is for the 'Silent' item
                uri = null;
            } else {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
                if (mClickedPos == moreRingtonePos) {
                    uri = nRingtoneUri;
                } else {
                    uri = mRingtoneManager.getRingtoneUri(getRingtoneManagerPosition(mClickedPos));
                }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

            }

            resultIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI, uri);
            setResult(RESULT_OK, resultIntent);
        } else {
            setResult(RESULT_CANCELED);
        }

        finish();
    }

    /*
     * On item selected via keys
     */
    public void onItemSelected(AdapterView parent, View view, int position, long id) {
        playRingtone(position, DELAY_MS_SELECTION_PLAYED);
    }

    public void onNothingSelected(AdapterView parent) {
    }

    private void playRingtone(int position, int delayMs) {
        mHandler.removeCallbacks(this);
        mSampleRingtonePos = position;
        mHandler.postDelayed(this, delayMs);
    }

    public void run() {
        stopAnyPlayingRingtone();
        if (mSampleRingtonePos == mSilentPos) {
            return;
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
        if(mMoreRingtones == mSampleRingtonePos) {
            return;
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        Ringtone ringtone;
        if (mSampleRingtonePos == mDefaultRingtonePos) {
            if (mDefaultRingtone == null) {
                mDefaultRingtone = RingtoneManager.getRingtone(this, mUriForDefaultItem);
            }
           /*
            * Stream type of mDefaultRingtone is not set explicitly here.
            * It should be set in accordance with mRingtoneManager of this Activity.
            */
            if (mDefaultRingtone != null) {
                mDefaultRingtone.setStreamType(mRingtoneManager.inferStreamType());
            }
            ringtone = mDefaultRingtone;
            mCurrentRingtone = null;
        } else {
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
            if (mSampleRingtonePos == moreRingtonePos) {
                ringtone = mRingtoneManager.getRingtone(this,nRingtoneUri);
            } else {
                try{
                    ringtone = mRingtoneManager.getRingtone(getRingtoneManagerPosition(mSampleRingtonePos));
                } catch(StaleDataException e) {
                    Log.v(TAG, "getRingtone fail");
                    ringtone = null;
                } catch (IllegalStateException illegalStateException) {
                    ringtone = null;
                }
            }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
            mCurrentRingtone = ringtone;
        }

        if (ringtone != null) {
            if (mAttributesFlags != 0) {
                ringtone.setAudioAttributes(
                        new AudioAttributes.Builder(ringtone.getAudioAttributes())
                                .setFlags(mAttributesFlags)
                                .build());
            }
            ringtone.play();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandler.removeCallbacks(this);
        mCursor.deactivate();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
        ((AudioManager) getSystemService(AUDIO_SERVICE)).abandonAudioFocus(mAudioFocusChangeListener);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        if (!isChangingConfigurations()) {
            stopAnyPlayingRingtone();
        } else {
            saveAnyPlayingRingtone();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!isChangingConfigurations()) {
            stopAnyPlayingRingtone();
        }
    }

    private void saveAnyPlayingRingtone() {
        if (mDefaultRingtone != null && mDefaultRingtone.isPlaying()) {
            sPlayingRingtone = mDefaultRingtone;
        } else if (mCurrentRingtone != null && mCurrentRingtone.isPlaying()) {
            sPlayingRingtone = mCurrentRingtone;
        }
    }

    private void stopAnyPlayingRingtone() {
        if (sPlayingRingtone != null && sPlayingRingtone.isPlaying()) {
            sPlayingRingtone.stop();
        }
        sPlayingRingtone = null;

        if (mDefaultRingtone != null && mDefaultRingtone.isPlaying()) {
            mDefaultRingtone.stop();
        }

        if (mRingtoneManager != null) {
            mRingtoneManager.stopPreviousRingtone();
        }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/05/2016, SOLUTION-2638493
//Porting MediaProvider and RingtonePicker Activity
        if(mCurrentRingtone != null && mCurrentRingtone.isPlaying()){
            mCurrentRingtone.stop();
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    private int getRingtoneManagerPosition(int listPos) {
        return listPos - mStaticItemCount;
    }

    private int getListPosition(int ringtoneManagerPos) {

        // If the manager position is -1 (for not found), return that
        if (ringtoneManagerPos < 0) return ringtoneManagerPos;

        return ringtoneManagerPos + mStaticItemCount;
    }

    //[BUGFIX]-Mod-BEGIN by AMNJ.fanghua.gu,10/16/2015,Task-528418 cannot add local music as ringtone
    private void showRingItem() {
        int mExistingType = 0;
        mHasRingtoneItem = false;
        if (mExistingUri == null || mExistingUri.toString().isEmpty() || !checkRingtoneExistOnSD( mExistingUri)) {
            mHasRingtoneItem = false;
            return;
        }

        mExistingType = moreRingtoneManager.getRingtoneType(mExistingUri);
        //[SOLUTION]-Add-BEGIN by TCTNB.wen.zhuang, 08/16/2016, SOLUTION-2548012
        if (VF_ERR_RTN_VAL == mExistingType) {
            return;
        }
        //[SOLUTION]-Add-END by TCTNB.wen.zhuang

        if (mType == RingtoneManager.TYPE_ALARM) {
            if (mType == mExistingType) {
                mHasRingtoneItem = false;
            } else {
                mHasRingtoneItem = true;
            }
        } else if (mType == RingtoneManager.TYPE_NOTIFICATION) {
            if (mType == mExistingType) {
                mHasRingtoneItem = false;
            } else {
                mHasRingtoneItem = true;
            }
        } else if (mType == RingtoneManager.TYPE_RINGTONE) {
            if (mType == mExistingType) {
                mHasRingtoneItem = false;
            } else {
                mHasRingtoneItem = true;
            }
        }
    }

    private  boolean checkRingtoneExistOnSD(Uri uri) {
        if (uri != null && !uri.toString().startsWith("content://media/external")) {
            return true;
        }

        boolean ringtoneExist = false;
        Cursor cursor = null;
        cursor = this.getContentResolver().query(uri, new String[] {
            MediaStore.Audio.Media.DATA
        }, null, null, null);

        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    String filePath = cursor.getString(0);
                    if (new File(filePath).exists()) {
                        ringtoneExist = true;
                    }
                }
            } finally {
                cursor.close();
            }
        }
        //[BUGFIX]-Mod-BEGIN by AMNJ.fanghua.gu,10/21/2015,Task-528812
        if (!ringtoneExist) {
            mExistingUri = mRingtoneManager.getDefaultUri(mType);
            ringtoneExist = true;
        }
        //[BUGFIX]-Mod-END by AMNJ.fanghua.gu
        return ringtoneExist;
    }
    //[BUGFIX]-Mod-END by AMNJ.fanghua.gu
}
