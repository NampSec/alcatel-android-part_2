LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-subdir-java-files) \
	src/com/android/providers/media/IMtpService.aidl

# MODIFIED-BEGIN by zhongting.cai, 2016-10-20,BUG-2905494
#modify by caizhongting
ifeq ($(SMCN_ROM_CONTROL_FLAG),true)
LOCAL_AAPT_FLAGS += -I out/target/common/obj/APPS/mst-framework-res_intermediates/package-export.apk
LOCAL_JAVA_LIBRARIES += mst-framework
endif
#end
# MODIFIED-END by zhongting.cai,BUG-2905494

LOCAL_PACKAGE_NAME := MediaProvider
LOCAL_CERTIFICATE := media
LOCAL_PRIVILEGED_MODULE := true

include $(BUILD_PACKAGE)
