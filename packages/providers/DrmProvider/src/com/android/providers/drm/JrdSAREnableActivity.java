package com.android.providers.drm;

import android.app.Activity;
import android.app.ActivityManager;
import android.os.Bundle;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * This is a demo for enable TCT sar feature
 * */

public class JrdSAREnableActivity extends Activity {
    private static final String SAR_PROPERTY = "persist.sys.tct_sar.enabled";
    private Button mEnableButton;
    private Button mDisableButton;

    private TelephonyManager mTelephonyManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mEnableButton = (Button) findViewById(R.id.enable);
        mDisableButton = (Button) findViewById(R.id.disable);
        mEnableButton.setOnClickListener(mEnableListener);
        mDisableButton.setOnClickListener(mDisableListener);

        mTelephonyManager = TelephonyManager.getDefault();
    }

    private OnClickListener mEnableListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ActivityManager.isUserAMonkey()) {
                return;
            }

            //mTelephonyManager.setRF(1);
            SystemProperties.set(SAR_PROPERTY, "1");
            Toast.makeText(JrdSAREnableActivity.this, "Enable TCT SAR", Toast.LENGTH_LONG).show();
        }
    };

    private OnClickListener mDisableListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ActivityManager.isUserAMonkey()) {
                return;
            }

            //mTelephonyManager.setRF(0);
            SystemProperties.set(SAR_PROPERTY, "0");
            Toast.makeText(JrdSAREnableActivity.this, "Disable TCT SAR", Toast.LENGTH_LONG).show();
        }
    };
}
