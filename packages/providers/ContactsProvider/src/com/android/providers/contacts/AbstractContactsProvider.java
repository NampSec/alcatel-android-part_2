/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.providers.contacts;

import com.android.providers.contacts.ContactsDatabaseHelper.AccountsColumns;
import com.android.providers.contacts.ContactsDatabaseHelper.ContactsColumns;
import com.android.providers.contacts.ContactsDatabaseHelper.RawContactsColumns;
import com.android.providers.contacts.ContactsDatabaseHelper.Tables;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteTransactionListener;
import android.net.Uri;
import android.os.Binder;
import android.os.SystemClock;
import android.provider.BaseColumns;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.SparseLongArray;

import java.io.PrintWriter;
import java.util.ArrayList;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/30/2016, SOLUTION-2466254
//Porting ContactsProviders to Next Platform
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.provider.CalendarContract.Events;
import android.text.format.Time;
import android.util.TctLog;

import java.lang.Exception;
import java.util.Random;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

/**
 * A common base class for the contacts and profile providers.  This handles much of the same
 * logic that SQLiteContentProvider does (i.e. starting transactions on the appropriate database),
 * but exposes awareness of batch operations to the subclass so that cross-database operations
 * can be supported.
 */
public abstract class AbstractContactsProvider extends ContentProvider
        implements SQLiteTransactionListener {

    public static final String TAG = "ContactsProvider";

    public static final boolean VERBOSE_LOGGING = Log.isLoggable(TAG, Log.VERBOSE);

    /** Set true to enable detailed transaction logging. */
    public static final boolean ENABLE_TRANSACTION_LOG = false; // Don't submit with true.

    /**
     * Duration in ms to sleep after successfully yielding the lock during a batch operation.
     */
    protected static final int SLEEP_AFTER_YIELD_DELAY = 4000;

    /**
     * Maximum number of operations allowed in a batch between yield points.
     */
    private static final int MAX_OPERATIONS_PER_YIELD_POINT = 500;

    /**
     * Number of inserts performed in bulk to allow before yielding the transaction.
     */
    private static final int BULK_INSERTS_PER_YIELD_POINT = 50;

    /**
     * The contacts transaction that is active in this thread.
     */
    private ThreadLocal<ContactsTransaction> mTransactionHolder;

    /**
     * The DB helper to use for this content provider.
     */
    private SQLiteOpenHelper mDbHelper;

    /**
     * The database helper to serialize all transactions on.  If non-null, any new transaction
     * created by this provider will automatically retrieve a writable database from this helper
     * and initiate a transaction on that database.  This should be used to ensure that operations
     * across multiple databases are all blocked on a single DB lock (to prevent deadlock cases).
     *
     * Hint: It's always {@link ContactsDatabaseHelper}.
     *
     * TODO Change the structure to make it obvious that it's actually always set, and is the
     * {@link ContactsDatabaseHelper}.
     */
    private SQLiteOpenHelper mSerializeOnDbHelper;

    /**
     * The tag corresponding to the database used for serializing transactions.
     *
     * Hint: It's always the contacts db helper tag.
     *
     * See also the TODO on {@link #mSerializeOnDbHelper}.
     */
    private String mSerializeDbTag;

    /**
     * The transaction listener used with {@link #mSerializeOnDbHelper}.
     *
     * Hint: It's always {@link ContactsProvider2}.
     *
     * See also the TODO on {@link #mSerializeOnDbHelper}.
     */
    private SQLiteTransactionListener mSerializedDbTransactionListener;

    private final long mStartTime = SystemClock.elapsedRealtime();

    private final Object mStatsLock = new Object();
    protected final SparseBooleanArray mAllCallingUids = new SparseBooleanArray();
    protected final SparseLongArray mQueryStats = new SparseLongArray();
    protected final SparseLongArray mBatchStats = new SparseLongArray();
    protected final SparseLongArray mInsertStats = new SparseLongArray();
    protected final SparseLongArray mUpdateStats = new SparseLongArray();
    protected final SparseLongArray mDeleteStats = new SparseLongArray();
    protected final SparseLongArray mInsertInBatchStats = new SparseLongArray();
    protected final SparseLongArray mUpdateInBatchStats = new SparseLongArray();
    protected final SparseLongArray mDeleteInBatchStats = new SparseLongArray();

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/30/2016, SOLUTION-2466254
//Porting ContactsProviders to Next Platform
    private boolean insertEvent = false;
    private long contact_id = 0;
    private int eventType = 100;
    private String lastInsertName = null;
    private String lastEventName = null;
    private String account = null;
    private final int MESSAGE_INSERT = 1;
    private final int MESSAGE_update = 2;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MESSAGE_INSERT:
                    if (insertEvent) {
                        updateCalendarName(contact_id);
                    }
                    break;
                case MESSAGE_update:
                    updateCalendarName((long) msg.arg1);
            }

        }
    };

    private boolean checkAccount(Uri uri) {
        if (uri.toString().contains("com.android.contacts/raw_contacts")) {
            return true;
        }
        return false;
    }

    private boolean checkEvent(ContentValues values) {
        if (values.containsKey("mimetype")) {
            if ("vnd.android.cursor.item/contact_event".equals(values.getAsString("mimetype"))) {
                return true;
            }
        }
        return false;
    }

    private String geNameByID(long id) {
        String name = "";
        try {
            final ContactsDatabaseHelper contactsDatabaseHelper = ContactsDatabaseHelper.getInstance(getContext());
            SQLiteDatabase db = contactsDatabaseHelper.getWritableDatabase();
            Cursor cursor = db.query(Tables.RAW_CONTACTS, null, "_id=?", new String[]{String.valueOf(id)}, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex("display_name"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
        return name;
    }

    private void updateCalendarName(long id) {
        String displayName = geNameByID(id);
        try {
            Intent intent = new Intent();
            intent.putExtra("displayName", displayName);
            intent.putExtra("contact_id", id);
            intent.putExtra("flag", 2);
            intent.setAction("com.ContactsProvider.CalendarEvent");
            getContext().sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    @Override
    public boolean onCreate() {
        Context context = getContext();
        mDbHelper = getDatabaseHelper(context);
        mTransactionHolder = getTransactionHolder();
        return true;
    }

    public SQLiteOpenHelper getDatabaseHelper() {
        return mDbHelper;
    }

    /**
     * Specifies a database helper (and corresponding tag) to serialize all transactions on.
     *
     * See also the TODO on {@link #mSerializeOnDbHelper}.
     */
    public void setDbHelperToSerializeOn(SQLiteOpenHelper serializeOnDbHelper, String tag,
            SQLiteTransactionListener listener) {
        mSerializeOnDbHelper = serializeOnDbHelper;
        mSerializeDbTag = tag;
        mSerializedDbTransactionListener = listener;
    }

    protected final void incrementStats(SparseLongArray stats) {
        final int callingUid = Binder.getCallingUid();
        synchronized (mStatsLock) {
            stats.put(callingUid, stats.get(callingUid) + 1);
            mAllCallingUids.put(callingUid, true);
        }
    }

    protected final void incrementStats(SparseLongArray statsNonBatch,
            SparseLongArray statsInBatch) {
        final ContactsTransaction t = mTransactionHolder.get();
        final boolean inBatch = t != null && t.isBatch();
        incrementStats(inBatch ? statsInBatch : statsNonBatch);
    }

    public ContactsTransaction getCurrentTransaction() {
        return mTransactionHolder.get();
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        incrementStats(mInsertStats, mInsertInBatchStats);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/30/2016, SOLUTION-2466254
//Porting ContactsProviders to Next Platform
        Message msg = new Message();
        msg.what = MESSAGE_INSERT;
        mHandler.sendMessageDelayed(msg, 100);
        ContactsTransaction transaction = startTransaction(false);
        ContentValues calValues = new ContentValues();
        contact_id = 0;
        try {
            if (checkAccount(uri)) {
                account = values.getAsString("account_type");
            }
            if (uri != null && uri.toString().contains("content://com.android.contacts/data")) {
                if (values.getAsLong("raw_contact_id") != null) {
                    contact_id = values.getAsLong("raw_contact_id");
                }
            }
            if (checkEvent(values)) {
                insertEvent = true;
                String date = values.getAsString("data1");
                date = date.replace("-", "");
                date = date.replace(":", "");
                date = date.replace(".", "");
                date = date.substring(0, 8);
                Time startTime = new Time(Time.TIMEZONE_UTC);
                startTime.parse(date);
                startTime.allDay = true;
                eventType = values.getAsInteger("data2");
                calValues.put("contact_id", contact_id);
                calValues.put(Events.RRULE, "FREQ=YEARLY;WKST=MO");
                calValues.put(Events.ALL_DAY, 1);
                calValues.put(Events.DTSTART, startTime.toMillis(false));
                calValues.put(Events.CALENDAR_ID, 1);
                calValues.put(Events.EVENT_TIMEZONE, Time.TIMEZONE_UTC);
                calValues.put(Events.DURATION, "P1D");
                lastInsertName = null;
                if (eventType == 0) {
                    lastEventName = values.getAsString("data3");
                } else if (eventType == 1) {
                    lastEventName = getContext().getString(R.string.event_anniversary);
                } else if (eventType == 2) {
                    lastEventName = getContext().getString(R.string.event_other);
                } else if (eventType == 3) {
                    lastEventName = getContext().getString(R.string.event_birthday);
                } else {
                    lastEventName = null;
                }
            }
        } catch (Exception e) {
            insertEvent = false;
            TctLog.e(TAG, "insert exception");
            Log.e(TAG, e.getMessage());
        }
        try {
            Uri result = insertInTransaction(uri, values);
            if (result != null) {
                transaction.markDirty();
            }
            transaction.markSuccessful(false);
            if (insertEvent) {
                String strEventId = result.toString()
                        .substring(result.toString().lastIndexOf('/') + 1);
                Long eventId = 0l;
                try {
                    eventId = Long.parseLong(strEventId);
                } catch (Exception ex) {
                    // Sometimes eventId was not long variant, need gen an eventId.
                    String sE[] = strEventId.split("\\?");
                    try {
                        eventId = Long.parseLong(sE[0]);
                    } catch (Exception exx) {
                        eventId = new Random(999).nextLong();
                    }
                }
                if (lastEventName != null || lastInsertName != null) {
                    calValues.put(Events.TITLE, lastInsertName + " " + lastEventName);
                }
                if (account != null) {
                    calValues.put("contact_account_type", account);
                }
                calValues.put("contact_data_id", eventId);
                calValues.put("contactEventType", eventType);
                Intent intent = new Intent();
                intent.putExtra("flag", 0);
                intent.putExtra("values", calValues);
                intent.setAction("com.ContactsProvider.CalendarEvent");
                getContext().sendBroadcast(intent);
            }
            return result;
        } finally {
            endTransaction(false);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        incrementStats(mDeleteStats, mDeleteInBatchStats);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/30/2016, SOLUTION-2466254
//Porting ContactsProviders to Next Platform
        try {
            String id = null;
            Intent intent = new Intent();
            intent.putExtra("flag", 3);
            if (selection == null && selectionArgs == null) {
                id = uri.toString().substring(uri.toString().lastIndexOf('/') + 1);
            } else if (uri.toString().equals("content://com.android.contacts/raw_contacts")) {
                //when we sync contacts,delete calendar event befor
                if (selection != null) {
                    intent.putExtra("delete_selection", selection);
                    intent.putExtra("flag", 4);
                    intent.setAction("com.ContactsProvider.CalendarEvent");
                    getContext().sendBroadcast(intent);
                }
            } else if (uri != null && uri.toString().contains("content://com.android.contacts/data")
                    && (!uri.toString().contains("com.android.contacts/data/usagefeedback"))) {
                ContactsDatabaseHelper contactsDatabaseHelper = ContactsDatabaseHelper.getInstance(getContext());
                SQLiteDatabase db = contactsDatabaseHelper.getWritableDatabase();
                Cursor cursor = db.query(Tables.RAW_CONTACTS, null, selection, selectionArgs, null, null, null, null);
                if (cursor.moveToFirst()) {
                    if ("vnd.android.cursor.item/contact_event".equals(cursor.getString(cursor
                            .getColumnIndex("mimetype")))) {
                        int dataId = cursor.getInt(cursor.getColumnIndex("_id"));
                        intent.putExtra("contact_data_id", dataId);
                        intent.putExtra("temp", "1");
                        intent.setAction("com.ContactsProvider.CalendarEvent");
                        getContext().sendBroadcast(intent);
                    }
                }
                cursor.close();
            }
            if (id != null) {
                Log.d(TAG, "ContactsProvider-----id======="+id);//[FEATURE]-ADD-BEGIN by TCTNB.(Huan_Liu),02/22/2016,Defect  1545955 ,
                intent.putExtra("id", id);
                intent.putExtra("temp", "2");
                intent.setAction("com.ContactsProvider.CalendarEvent");
                getContext().sendBroadcast(intent);
            }
        } catch (Exception e) {
            TctLog.e(TAG, "delete exception");
            e.printStackTrace();
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        ContactsTransaction transaction = startTransaction(false);
        try {
            int deleted = deleteInTransaction(uri, selection, selectionArgs);
            if (deleted > 0) {
                transaction.markDirty();
            }
            transaction.markSuccessful(false);
            return deleted;
        } finally {
            endTransaction(false);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        incrementStats(mUpdateStats, mUpdateInBatchStats);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 07/30/2016, SOLUTION-2466254
//Porting ContactsProviders to Next Platform
        //porting from PR1050063,
		// [BUGFIX]-ADD-Begin by TCTNB.(kang.yu), 2014-12-09,PR1047716
        Cursor cursor=null;
        try {
            if (uri != null && uri.toString().contains("com.android.contacts/data")
                    && (!uri.toString().contains("com.android.contacts/data/usagefeedback"))) {
                ContactsDatabaseHelper contactsDatabaseHelper = ContactsDatabaseHelper.getInstance(getContext());
                SQLiteDatabase db = contactsDatabaseHelper.getReadableDatabase();
                cursor = db.query(Tables.DATA, null, selection, selectionArgs, null, null, null, null);
                if (cursor.moveToFirst()){
                    String name = null;
                    long date = 0;
                    String eventName = null;
                    int eventType = 100;
                    long eventId = 0;
                    try {
                        if (values.getAsString("data1") != null) {
                            String dateTemp = values.getAsString("data1");
                            dateTemp = dateTemp.replace("-", "");

                            Time startTime = new Time(Time.TIMEZONE_UTC);
                            startTime.parse(dateTemp);
                            startTime.allDay = true;
                            date = startTime.toMillis(false);
                        }
                        if (values.getAsInteger("data2") != null) {
                            eventType = values.getAsInteger("data2");
                            if (eventType == 0) {
                                if (values.getAsString("data3") != null) {
                                    eventName = values.getAsString("data3");
                                } else {
                                    eventName = cursor.getString(cursor.getColumnIndex("data3"));
                                }
                            } else if (eventType == 1) {
                                eventName = getContext().getString(R.string.event_anniversary);
                            } else if (eventType == 2) {
                                eventName = getContext().getString(R.string.event_other);
                            } else if (eventType == 3) {
                                eventName = getContext().getString(R.string.event_birthday);
                            }
                        } else {
                            eventType = cursor.getInt(cursor.getColumnIndex("data2"));
                            if (eventType == 0) {
                                eventName = cursor.getString(cursor.getColumnIndex("data3"));
                            } else if (eventType == 1) {
                                eventName = getContext().getString(R.string.event_anniversary);
                            } else if (eventType == 2) {
                                eventName = getContext().getString(R.string.event_other);
                            } else if (eventType == 3) {
                                eventName = getContext().getString(R.string.event_birthday);
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                    eventId = cursor.getLong(cursor.getColumnIndex("_id"));
                    int rawContactId = cursor.getInt(cursor.getColumnIndex("raw_contact_id"));
                    Intent intent = new Intent();
                    intent.putExtra("flag", 1);
                    intent.putExtra("eventType", eventType);
                    intent.putExtra("eventName", eventName);
                    intent.putExtra("contact_data_id", eventId);
                    intent.putExtra("name", name);
                    intent.putExtra("date", date);
                    intent.setAction("com.ContactsProvider.CalendarEvent");
                    getContext().sendBroadcast(intent);
                    Message msg = new Message();
                    msg.what = MESSAGE_update;
                    msg.arg1 = rawContactId;
                    mHandler.sendMessageDelayed(msg, 100);
                }
            }
			// [BUGFIX]-END-Begin by TCTNB.(kang.yu)
        } catch (Exception e) {
            TctLog.e(TAG, "update exception");
            Log.e(TAG, e.getMessage());
        } finally {
            if (cursor!=null) {
                cursor.close();
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        ContactsTransaction transaction = startTransaction(false);
        try {
            int updated = updateInTransaction(uri, values, selection, selectionArgs);
            if (updated > 0) {
                transaction.markDirty();
            }
            transaction.markSuccessful(false);
            return updated;
        } finally {
            endTransaction(false);
        }
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        incrementStats(mBatchStats);
        ContactsTransaction transaction = startTransaction(true);
        int numValues = values.length;
        int opCount = 0;
        try {
            for (int i = 0; i < numValues; i++) {
                insert(uri, values[i]);
                if (++opCount >= BULK_INSERTS_PER_YIELD_POINT) {
                    opCount = 0;
                    try {
                        yield(transaction);
                    } catch (RuntimeException re) {
                        transaction.markYieldFailed();
                        throw re;
                    }
                }
            }
            transaction.markSuccessful(true);
        } finally {
            endTransaction(true);
        }
        return numValues;
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        incrementStats(mBatchStats);
        if (VERBOSE_LOGGING) {
            Log.v(TAG, "applyBatch: " + operations.size() + " ops");
        }
        int ypCount = 0;
        int opCount = 0;
        ContactsTransaction transaction = startTransaction(true);
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                if (++opCount >= MAX_OPERATIONS_PER_YIELD_POINT) {
                    throw new OperationApplicationException(
                            "Too many content provider operations between yield points. "
                                    + "The maximum number of operations per yield point is "
                                    + MAX_OPERATIONS_PER_YIELD_POINT, ypCount);
                }
                final ContentProviderOperation operation = operations.get(i);
                if (i > 0 && operation.isYieldAllowed()) {
                    if (VERBOSE_LOGGING) {
                        Log.v(TAG, "applyBatch: " + opCount + " ops finished; about to yield...");
                    }
                    opCount = 0;
                    try {
                        if (yield(transaction)) {
                            ypCount++;
                        }
                    } catch (RuntimeException re) {
                        transaction.markYieldFailed();
                        throw re;
                    }
                }

                results[i] = operation.apply(this, results, i);
            }
            transaction.markSuccessful(true);
            return results;
        } finally {
            endTransaction(true);
        }
    }

    /**
     * If we are not yet already in a transaction, this starts one (on the DB to serialize on, if
     * present) and sets the thread-local transaction variable for tracking.  If we are already in
     * a transaction, this returns that transaction, and the batch parameter is ignored.
     * @param callerIsBatch Whether the caller is operating in batch mode.
     */
    private ContactsTransaction startTransaction(boolean callerIsBatch) {
        if (ENABLE_TRANSACTION_LOG) {
            Log.i(TAG, "startTransaction " + getClass().getSimpleName() +
                    "  callerIsBatch=" + callerIsBatch, new RuntimeException("startTransaction"));
        }
        ContactsTransaction transaction = mTransactionHolder.get();
        if (transaction == null) {
            transaction = new ContactsTransaction(callerIsBatch);
            if (mSerializeOnDbHelper != null) {
                transaction.startTransactionForDb(mSerializeOnDbHelper.getWritableDatabase(),
                        mSerializeDbTag, mSerializedDbTransactionListener);
            }
            mTransactionHolder.set(transaction);
        }
        return transaction;
    }

    /**
     * Ends the current transaction and clears out the member variable.  This does not set the
     * transaction as being successful.
     * @param callerIsBatch Whether the caller is operating in batch mode.
     */
    private void endTransaction(boolean callerIsBatch) {
        if (ENABLE_TRANSACTION_LOG) {
            Log.i(TAG, "endTransaction " + getClass().getSimpleName() +
                    "  callerIsBatch=" + callerIsBatch, new RuntimeException("endTransaction"));
        }
        ContactsTransaction transaction = mTransactionHolder.get();
        if (transaction != null && (!transaction.isBatch() || callerIsBatch)) {
            boolean notify = false;
            try {
                if (transaction.isDirty()) {
                    notify = true;
                }
                transaction.finish(callerIsBatch);
                if (notify) {
                    notifyChange();
                }
            } finally {
                // No matter what, make sure we clear out the thread-local transaction reference.
                mTransactionHolder.set(null);
            }
        }
    }

    /**
     * Gets the database helper for this contacts provider.  This is called once, during onCreate().
     */
    protected abstract SQLiteOpenHelper getDatabaseHelper(Context context);

    /**
     * Gets the thread-local transaction holder to use for keeping track of the transaction.  This
     * is called once, in onCreate().  If multiple classes are inheriting from this class that need
     * to be kept in sync on the same transaction, they must all return the same thread-local.
     */
    protected abstract ThreadLocal<ContactsTransaction> getTransactionHolder();

    protected abstract Uri insertInTransaction(Uri uri, ContentValues values);

    protected abstract int deleteInTransaction(Uri uri, String selection, String[] selectionArgs);

    protected abstract int updateInTransaction(Uri uri, ContentValues values, String selection,
            String[] selectionArgs);

    protected abstract boolean yield(ContactsTransaction transaction);

    protected abstract void notifyChange();

    private static final String ACCOUNTS_QUERY =
            "SELECT * FROM " + Tables.ACCOUNTS + " ORDER BY " + BaseColumns._ID;

    private static final String NUM_INVISIBLE_CONTACTS_QUERY =
            "SELECT count(*) FROM " + Tables.CONTACTS;

    private static final String NUM_VISIBLE_CONTACTS_QUERY =
            "SELECT count(*) FROM " + Tables.DEFAULT_DIRECTORY;

    private static final String NUM_RAW_CONTACTS_PER_CONTACT =
            "SELECT _id, count(*) as c FROM " + Tables.RAW_CONTACTS
                    + " GROUP BY " + RawContacts.CONTACT_ID;

    private static final String MAX_RAW_CONTACTS_PER_CONTACT =
            "SELECT max(c) FROM (" + NUM_RAW_CONTACTS_PER_CONTACT + ")";

    private static final String AVG_RAW_CONTACTS_PER_CONTACT =
            "SELECT avg(c) FROM (" + NUM_RAW_CONTACTS_PER_CONTACT + ")";

    private static final String NUM_RAW_CONTACT_PER_ACCOUNT_PER_CONTACT =
            "SELECT " + RawContactsColumns.ACCOUNT_ID + " AS aid"
                    + ", " + RawContacts.CONTACT_ID + " AS cid"
                    + ", count(*) AS c"
                    + " FROM " + Tables.RAW_CONTACTS
                    + " GROUP BY aid, cid";

    private static final String RAW_CONTACTS_PER_ACCOUNT_PER_CONTACT =
            "SELECT aid, sum(c) AS s, max(c) AS m, avg(c) AS a"
                    + " FROM (" + NUM_RAW_CONTACT_PER_ACCOUNT_PER_CONTACT + ")"
                    + " GROUP BY aid";

    private static final String DATA_WITH_ACCOUNT =
            "SELECT d._id AS did"
            + ", d." + Data.RAW_CONTACT_ID + " AS rid"
            + ", r." + RawContactsColumns.ACCOUNT_ID + " AS aid"
            + " FROM " + Tables.DATA + " AS d JOIN " + Tables.RAW_CONTACTS + " AS r"
            + " ON d." + Data.RAW_CONTACT_ID + "=r._id";

    private static final String NUM_DATA_PER_ACCOUNT_PER_RAW_CONTACT =
            "SELECT aid, rid, count(*) AS c"
                    + " FROM (" + DATA_WITH_ACCOUNT + ")"
                    + " GROUP BY aid, rid";

    private static final String DATA_PER_ACCOUNT_PER_RAW_CONTACT =
            "SELECT aid, sum(c) AS s, max(c) AS m, avg(c) AS a"
                    + " FROM (" + NUM_DATA_PER_ACCOUNT_PER_RAW_CONTACT + ")"
                    + " GROUP BY aid";

    protected void dump(PrintWriter pw, String dbName) {
        pw.print("Database: ");
        pw.println(dbName);

        pw.print("  Uptime: ");
        pw.print((SystemClock.elapsedRealtime() - mStartTime) / (60 * 1000));
        pw.println(" minutes");

        synchronized (mStatsLock) {
            pw.println();
            pw.println("  Client activities:");
            pw.println("    UID        Query  Insert Update Delete   Batch Insert Update Delete:");
            for (int i = 0; i < mAllCallingUids.size(); i++) {
                final int pid = mAllCallingUids.keyAt(i);
                pw.println(String.format(
                        "    %-9d %6d  %6d %6d %6d  %6d %6d %6d %6d",
                        pid,
                        mQueryStats.get(pid),
                        mInsertStats.get(pid),
                        mUpdateStats.get(pid),
                        mDeleteStats.get(pid),
                        mBatchStats.get(pid),
                        mInsertInBatchStats.get(pid),
                        mUpdateInBatchStats.get(pid),
                        mDeleteInBatchStats.get(pid)
                ));
            }
        }

        if (mDbHelper == null) {
            pw.println("mDbHelper is null");
            return;
        }
        try {
            pw.println();
            pw.println("  Accounts:");
            final SQLiteDatabase db = mDbHelper.getReadableDatabase();

            try (Cursor c = db.rawQuery(ACCOUNTS_QUERY, null)) {
                c.moveToPosition(-1);
                while (c.moveToNext()) {
                    pw.print("    ");
                    dumpLongColumn(pw, c, BaseColumns._ID);
                    pw.print(" ");
                    dumpStringColumn(pw, c, AccountsColumns.ACCOUNT_NAME);
                    pw.print(" ");
                    dumpStringColumn(pw, c, AccountsColumns.ACCOUNT_TYPE);
                    pw.print(" ");
                    dumpStringColumn(pw, c, AccountsColumns.DATA_SET);
                    pw.println();
                }
            }

            pw.println();
            pw.println("  Contacts:");
            pw.print("    # of visible: ");
            pw.print(longForQuery(db, NUM_VISIBLE_CONTACTS_QUERY));
            pw.println();

            pw.print("    # of invisible: ");
            pw.print(longForQuery(db, NUM_INVISIBLE_CONTACTS_QUERY));
            pw.println();

            pw.print("    Max # of raw contacts: ");
            pw.print(longForQuery(db, MAX_RAW_CONTACTS_PER_CONTACT));
            pw.println();

            pw.print("    Avg # of raw contacts: ");
            pw.print(doubleForQuery(db, AVG_RAW_CONTACTS_PER_CONTACT));
            pw.println();

            pw.println();
            pw.println("  Raw contacts (per account):");
            try (Cursor c = db.rawQuery(RAW_CONTACTS_PER_ACCOUNT_PER_CONTACT, null)) {
                c.moveToPosition(-1);
                while (c.moveToNext()) {
                    pw.print("    ");
                    dumpLongColumn(pw, c, "aid");
                    pw.print(" total # of raw contacts: ");
                    dumpStringColumn(pw, c, "s");
                    pw.print(", max # per contact: ");
                    dumpLongColumn(pw, c, "m");
                    pw.print(", avg # per contact: ");
                    dumpDoubleColumn(pw, c, "a");
                    pw.println();
                }
            }

            pw.println();
            pw.println("  Data (per account):");
            try (Cursor c = db.rawQuery(DATA_PER_ACCOUNT_PER_RAW_CONTACT, null)) {
                c.moveToPosition(-1);
                while (c.moveToNext()) {
                    pw.print("    ");
                    dumpLongColumn(pw, c, "aid");
                    pw.print(" total # of data:");
                    dumpLongColumn(pw, c, "s");
                    pw.print(", max # per raw contact: ");
                    dumpLongColumn(pw, c, "m");
                    pw.print(", avg # per raw contact: ");
                    dumpDoubleColumn(pw, c, "a");
                    pw.println();
                }
            }
        } catch (Exception e) {
            pw.println("Error: " + e);
        }
    }

    private static void dumpStringColumn(PrintWriter pw, Cursor c, String column) {
        final int index = c.getColumnIndex(column);
        if (index == -1) {
            pw.println("Column not found: " + column);
            return;
        }
        final String value = c.getString(index);
        if (value == null) {
            pw.print("(null)");
        } else if (value.length() == 0) {
            pw.print("\"\"");
        } else {
            pw.print(value);
        }
    }

    private static void dumpLongColumn(PrintWriter pw, Cursor c, String column) {
        final int index = c.getColumnIndex(column);
        if (index == -1) {
            pw.println("Column not found: " + column);
            return;
        }
        if (c.isNull(index)) {
            pw.print("(null)");
        } else {
            pw.print(c.getLong(index));
        }
    }

    private static void dumpDoubleColumn(PrintWriter pw, Cursor c, String column) {
        final int index = c.getColumnIndex(column);
        if (index == -1) {
            pw.println("Column not found: " + column);
            return;
        }
        if (c.isNull(index)) {
            pw.print("(null)");
        } else {
            pw.print(c.getDouble(index));
        }
    }

    private static long longForQuery(SQLiteDatabase db, String query) {
        return DatabaseUtils.longForQuery(db, query, null);
    }

    private static double doubleForQuery(SQLiteDatabase db, String query) {
        try (final Cursor c = db.rawQuery(query, null)) {
            if (!c.moveToFirst()) {
                return -1;
            }
            return c.getDouble(0);
        }
    }
}
