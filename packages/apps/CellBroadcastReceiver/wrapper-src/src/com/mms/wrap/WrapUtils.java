package com.mms.wrap;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;
import android.util.Xml;

class WrapUtils {
    private static final String TAG = WrapConstants.TAG;
    private static final String PATH = "/system/custpack/plf/wrapper-mms/";
    private static final String FILE = "isdm_wrapper-mms_defaults.xml";

    /**
     * get isdm value which is bool
     *
     * @param def_name : the name of isdmID
     * @return
     */
    static boolean getBooleanFromXml(String defName) {
        boolean result = false;
        try {
            String value = getISDMString(new File(PATH + FILE), defName, "bool");
            if (null != value) {
                result = Boolean.parseBoolean(value);
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * get isdm value which is string
     *
     * @param def_name : the name of isdmID
     * @return
     */
    public static String getStringFromXml(String defName) {
        String result = null;
        try {
            String value = getISDMString(new File(PATH + FILE), defName, "string");
            if (null != value) {
                result = value.replace("\"", "");
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * parser the XML file to get the isdmID value
     *
     * @param file : xml file
     * @param name : isdmID
     * @param type : isdmID type like bool and string
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private static String getISDMString(File file, String name, String type)
            throws XmlPullParserException,
            IOException {
        if (!file.exists() || null == file) {
            Log.e(TAG, "file not exist : " + file);
            return null;
        }
        String result = null;
        InputStream inputStream = new FileInputStream(file);
        XmlPullParser xmlParser = Xml.newPullParser();
        xmlParser.setInput(inputStream, "utf-8");

        int evtType = xmlParser.getEventType();
        boolean query_end = false;
        while (evtType != XmlPullParser.END_DOCUMENT && !query_end) {

            switch (evtType) {
                case XmlPullParser.START_TAG:

                    String start_tag = xmlParser.getAttributeValue(null, "name");
                    String start_type = xmlParser.getName();
                    if (null != start_tag && type.equals(start_type) && start_tag.equals(name)) {
                        result = xmlParser.nextText();
                        query_end = true;
                    }
                    break;

                case XmlPullParser.END_TAG:

                    break;

                default:
                    break;
            }
            // move to next node if not tail
            evtType = xmlParser.next();
        }
        inputStream.close();
        return result;
    }
}
