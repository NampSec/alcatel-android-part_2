package com.mms.wrap;

import android.content.Context;
import android.util.Log;

import java.util.regex.Matcher;

import com.mms.wrap.WrapConstants.Project;

import java.util.regex.Pattern;

import android.telephony.SmsManager;
import android.os.SystemProperties;

public final class WrapManager {
    private static final String TAG = WrapConstants.TAG;

    private static WrapManager mWrapManager;

    private static IWrapCommon mIWrapCommon;

    /**
      * get WrapManager instance
      *
      * @return WrapManager instance
      */
    public static WrapManager getInstance() {
        if (mWrapManager == null) {
            // create object by platform and project
            mWrapManager = new WrapManager();
            mIWrapCommon = getWrapInstance();
        }
        return mWrapManager;
    }

    private static IWrapCommon getWrapInstance() {
        IWrapCommon iWrap = null;
        try {
            //String platformName = WrapUtils.getStringFromXml(WrapConstants.PLATFORM_PLF_KEY);// unused here
            //Log.d(TAG, "platformName = " + platformName);

            String projectName = SystemProperties.get("ro.tct.product",null);
            Log.d(TAG, "projectName = " + projectName);
            if (projectName != null) {
                for (Project project : WrapConstants.Project.values()) {
                    Log.d(TAG, "project = " + project.getName());
                    if (project.getName().equalsIgnoreCase(projectName)) {
                        switch (project) {
                        case ALTO5:
                            iWrap = new WrapAlto5();
                            break;
                        case ALTO4:
                            iWrap = new WrapAlto4();
                            break;
                        default:
                            iWrap = new WrapCommon();
                            break;
                        }
                    }
                }
            }
            if (iWrap == null){
                iWrap = new WrapCommon();//default
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "create iwrap instance error");
        }
        return iWrap;
    }

    /**
      * set CB range to empty
      */
    public void setCellBroadcastRangesEmpty() {
        mIWrapCommon.setCellBroadcastRangesEmpty();
    }

    /**
      * get CB config
      *
      * @param manager
      *           sms manager object
      */
    public void getCellBroadcastConfig(SmsManager manager) {
        mIWrapCommon.getCellBroadcastConfig(manager);
    }

    /**
     * Convenience method to return only the digits and plus signs
     * in the matching string.
     *
     * @param matcher      The Matcher object from which digits and plus will
     *                     be extracted
     *
     * @return             A String comprising all of the digits and plus in
     *                     the match
     */
    public String getTctDigitsAsteriskAndPlusOnly(Matcher matcher) {
        return ((WrapQcom)mIWrapCommon).getTctDigitsAsteriskAndPlusOnly(matcher);
    }

    /**
     * get tct israel pattern
     *
     * @return             israel pattern
     */
    public Pattern getTctIsraelPattern() {
        return ((WrapQcom)mIWrapCommon).getTctIsraelPattern();
    }

    /**
      * weather the device allow the user to make a direct call when put the cellphone near your ear
      *
      * @return 1 means allow
      */
    public int getSettingSystemFlipMonitorValue(Context context) {
        return mIWrapCommon.getSettingSystemFlipMonitorValue(context);
    }

    /**
      * weather USERPIN configuration menu is on
      *
      * @return 1 means allow
      */
    public int getSettingSystemMessageUserpinValue(Context context) {
        return mIWrapCommon.getSettingSystemMessageUserpinValue(context);
    }

    /**
      * weather  Wap message enable
      *
      * @return 1 means allow
      */
    public int getSettingSystemWapMessageValue(Context context) {
        return mIWrapCommon.getSettingSystemWapMessageValue(context);
    }

    /**
      * set weather the device allow the user to make a direct call when put the cellphone near your ear
      *
      */
    public void putSettingSystemFlipMonitorValue(Context context, int value) {
        mIWrapCommon.putSettingSystemFlipMonitorValue(context, value);
    }

    /**
      * set weather USERPIN configuration menu is on
      *
      */
    public void putSettingSystemMessageUserpinValue(Context context,int value) {
        mIWrapCommon.putSettingSystemMessageUserpinValue(context, value);
    }

    /**
      * set weather Wap message enable
      *
      */
    public void putSettingSystemWapMessageValue(Context context,int value) {
        mIWrapCommon.putSettingSystemWapMessageValue(context, value);
    }

    /**
     * get boolean from tct frameworks plf
     *
     * @return        boolean
     */
    public boolean getBooleanFromFrameworksPlf(Context context,String key) {
        return ((WrapQcom)mIWrapCommon).getBooleanFromFrameworksPlf(context,key);
    }

}
