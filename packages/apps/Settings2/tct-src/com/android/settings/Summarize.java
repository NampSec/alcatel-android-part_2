/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import mst.app.MstActivity;
import android.view.View;
import android.os.Bundle;

/* Create by TCTNB(Guoqiang.Qiu)
 * This Activity is to save emergency message of One Finger Alarm
 */
public class Summarize extends MstActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMstContentView(R.layout.activity_summarize);
        showBackIcon(true);
        setTitle(R.string.summarize);
    }
    @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
    }
}
