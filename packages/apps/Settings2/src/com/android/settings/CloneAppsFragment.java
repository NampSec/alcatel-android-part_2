/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings;

import com.android.internal.logging.MetricsProto;
import com.cmx.cmplus.SmartContainerConfig;
import com.cmx.cmplus.sdk.CloneHelper;

import org.xmlpull.v1.XmlPullParser;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import mst.preference.SwitchPreference;
import mst.preference.Preference;
import mst.preference.PreferenceCategory;
import mst.preference.PreferenceScreen;
import android.util.AtomicFile;
import android.util.Log;
import android.util.Xml;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class CloneAppsFragment extends SettingsPreferenceFragment implements Preference.OnPreferenceChangeListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener{
    private static final String CLONE_APP_PREFERENCE_KEY = "clone_app_preference";
    private static final String FACTORY_CLONE_APP_PREFERENCE_KEY = "factory_clone_app_preference";
    private static final String CLONE_TIP_PREFERENCE_KEY = "clone_app_tip";
    private Dialog mCloneDialog;
    private Boolean mDialogClicked;
    private CloneHelper mCloneHelper;
    private SwitchPreference mPreference;
    private static final boolean DEBUG = false;
    private static final String TAG = "CloneAppsFragment";
    private static HashMap<String,PackageInfo> mPackageInfoMap = null;
    private static final String PACKAGE_INFO_FILE_PATH = "/system/etc/container/package_info.xml";

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if(newValue == Boolean.TRUE){
            String pkgName = preference.getKey();
            if(!mCloneHelper.isPackageCloned(pkgName)){
                mCloneHelper.createCloneForPackage(pkgName);
            }
            return true;
        }else if(newValue == Boolean.FALSE){
            mDialogClicked = false;
            if (mCloneDialog != null) dismissDialogs();
            mPreference = (SwitchPreference)preference;
            mCloneDialog = new AlertDialog.Builder(getActivity()).setMessage(
                    getActivity().getResources().getString(R.string.app_clone_remove_clone_summary))
                    .setTitle(R.string.app_clone_remove_clone_title)
                    .setPositiveButton(R.string.btn_continue, this)
                    .setNegativeButton(android.R.string.no, this)
                    .show();
            mCloneDialog.setOnDismissListener(this);
            return false;
        }
        return false;
    }

    private void dismissDialogs(){
        if(mCloneDialog != null){
            mCloneDialog.dismiss();
            mCloneDialog = null;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (dialog == mCloneDialog){
            if (which == DialogInterface.BUTTON_POSITIVE) {
                mDialogClicked = true;
                String pkgName = mPreference.getKey();
                if(mCloneHelper.isPackageCloned(pkgName)){
                    mCloneHelper.deleteCloneForPackage(pkgName);
                }
                mPreference.setChecked(false);
            }else{
                mPreference.setChecked(true);
            }
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (dialog == mCloneDialog){
            if (!mDialogClicked){
                mPreference.setChecked(true);
            }
            mCloneDialog = null;
        }
    }


    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        mCloneHelper = CloneHelper.get(getActivity());
        PreferenceScreen preferences = getPreferenceScreen();
        if (preferences == null) {
            preferences = getPreferenceManager().createPreferenceScreen(getActivity());
            setPreferenceScreen(preferences);
        }
        loadPreferences();
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView emptyView = (TextView) getView().findViewById(android.R.id.empty);
        setEmptyView(emptyView);
        if (emptyView != null) {
            emptyView.setText(R.string.app_clone_disabled_summary);
        }
        loadPreferences();
    }

    public static boolean isAppCloneEnabled() {
        SmartContainerConfig.init();
        return !SmartContainerConfig.WITH_OUT_APP_CLONE;
    }

    private void loadPreferences(){
        PreferenceScreen screen = getPreferenceScreen();
        screen.removeAll();
        if(isAppCloneEnabled()){
            screen.setEnabled(true);
            final Context context = getActivity();
            if (context == null) {
                return;
            }
            ArrayList<CloneablePackage> installedCloneablePackageArrayList = new ArrayList<>();
            ArrayList<CloneablePackage> installedOEMCloneablePackageArrayList = new ArrayList<>();
            List<CloneablePackage> DefaultCloneablePackageArrayList = getDefaultCloneablePackageList();
            List<CloneablePackage> OEMCloneablePackageArrayList = getOEMCloneablePackageList();
            if(DEBUG){
                Log.i(TAG,"DefaultCloneablePackageArrayList");
                for(CloneablePackage cloneablePackage: DefaultCloneablePackageArrayList){
                    Log.i(TAG, "name "+cloneablePackage.getPackageName()+" icon "+cloneablePackage.getIconName()+" label "+cloneablePackage.getLabel());

                }
                Log.i(TAG,"OEMCloneablePackageArrayList");
                for(CloneablePackage cloneablePackage: OEMCloneablePackageArrayList){
                    Log.i(TAG, "name "+cloneablePackage.getPackageName()+" icon "+cloneablePackage.getIcon()+" label "+cloneablePackage.getLabel());

                }
            }
            Intent launcherIntent = new Intent(Intent.ACTION_MAIN);
            launcherIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> resolveInfos = getPackageManager().queryIntentActivities(launcherIntent, 0);
            outer:
            for (ResolveInfo ri : resolveInfos) {
                ApplicationInfo applicationInfo = ri.activityInfo.applicationInfo;
                if (mCloneHelper.canPackageBeCloned(applicationInfo.packageName)) {
                    int uid = applicationInfo.uid;
                    if(mCloneHelper.isMasterInstance(uid)) {
                        String packageName = applicationInfo.packageName;
                        Drawable icon = applicationInfo.loadIcon(context.getPackageManager());
                        String label = context.getPackageManager().getApplicationLabel(applicationInfo).toString();
                        boolean cloned = mCloneHelper.isPackageCloned(applicationInfo.packageName);
                        Iterator<CloneablePackage> DefaultCloneablePackageIterator =  DefaultCloneablePackageArrayList.iterator();
                        while(DefaultCloneablePackageIterator.hasNext()){
                            CloneablePackage cloneablePackage = DefaultCloneablePackageIterator.next();
                            if(cloneablePackage.getPackageName().equals(applicationInfo.packageName)){
                                installedCloneablePackageArrayList.add(new CloneablePackage(packageName,icon,label,false,cloned));
                                DefaultCloneablePackageIterator.remove();
                                continue outer;
                            }
                        }
                        Iterator<CloneablePackage> OEMCloneableAppInfoIterator =  OEMCloneablePackageArrayList.iterator();
                        while(OEMCloneableAppInfoIterator.hasNext()){
                            CloneablePackage cloneablePackage = OEMCloneableAppInfoIterator.next();
                            if(cloneablePackage.getPackageName().equals(packageName)){
                                installedOEMCloneablePackageArrayList.add(new CloneablePackage(packageName,icon,label,true,cloned));
                                OEMCloneableAppInfoIterator.remove();
                                continue outer;
                            }
                        }
                    }
                }
            }
            if(DEBUG){
                Log.i(TAG, "installedCloneablePackageArrayList");
                for (CloneablePackage cloneablePackage : installedCloneablePackageArrayList) {
                    Log.i(TAG,cloneablePackage.toString());
                }
                Log.i(TAG,"installedOEMCloneablePackageArrayList");
                for (CloneablePackage cloneablePackage : installedOEMCloneablePackageArrayList) {
                    Log.i(TAG,cloneablePackage.toString());
                }
                Log.i(TAG,"DefaultCloneablePackageArrayList");
                for (CloneablePackage cloneablePackage : DefaultCloneablePackageArrayList) {
                    Log.i(TAG,cloneablePackage.toString());
                }
                Log.i(TAG,"OEMCloneablePackageArrayList");
                for (CloneablePackage cloneablePackage : OEMCloneablePackageArrayList) {
                    Log.i(TAG,cloneablePackage.toString());
                }
            }
            final PreferenceCategory preferenceCategory = new PreferenceCategory(context);

            screen.addPreference(preferenceCategory);
            preferenceCategory.setEnabled(true);
            preferenceCategory.setKey(CLONE_APP_PREFERENCE_KEY);
            preferenceCategory.setTitle(R.string.app_clone_installed_white_list_label);
            SwitchPreference switchPreference;
            for (CloneablePackage cloneablePackage : installedCloneablePackageArrayList) {
                String packageName = cloneablePackage.getPackageName();
                removeOldPreference(packageName);
                switchPreference = new SwitchPreference(context);
                preferenceCategory.addPreference(switchPreference);
                switchPreference.setKey(packageName);
                switchPreference.setEnabled(true);
                switchPreference.setTitle(cloneablePackage.getLabel());
                switchPreference.setIcon(cloneablePackage.getIcon());
                switchPreference.setOnPreferenceChangeListener(this);
                switchPreference.setChecked(cloneablePackage.isCloned());
            }
            for (CloneablePackage cloneablePackage : installedOEMCloneablePackageArrayList) {
                String packageName = cloneablePackage.getPackageName();
                removeOldPreference(packageName);
                switchPreference = new SwitchPreference(context);
                preferenceCategory.addPreference(switchPreference);
                switchPreference.setKey(packageName);
                switchPreference.setEnabled(true);
                switchPreference.setTitle(cloneablePackage.getLabel());
                switchPreference.setIcon(cloneablePackage.getIcon());
                switchPreference.setOnPreferenceChangeListener(this);
                switchPreference.setChecked(cloneablePackage.isCloned());
            }

            final PreferenceCategory notInstalledPreferenceCategory = new PreferenceCategory(context);

            screen.addPreference(notInstalledPreferenceCategory);
            notInstalledPreferenceCategory.setEnabled(false);
            notInstalledPreferenceCategory.setKey(FACTORY_CLONE_APP_PREFERENCE_KEY);
            notInstalledPreferenceCategory.setTitle(R.string.app_clone_not_installed_white_list_label);
            Preference notInstalledPreference;
            for (CloneablePackage cloneablePackage : DefaultCloneablePackageArrayList) {
                if(cloneablePackage.getLabel() != null){
                    String packageName = cloneablePackage.getPackageName();
                    removeOldPreference(packageName);
                    notInstalledPreference= new Preference(context);
                    notInstalledPreferenceCategory.addPreference(notInstalledPreference);
                    notInstalledPreference.setKey(packageName);
                    notInstalledPreference.setEnabled(false);
                    notInstalledPreference.setTitle(cloneablePackage.getLabel());
                    Drawable icon = cloneablePackage.getIconDrawable();
                    notInstalledPreference.setIcon(icon);
                }
            }
            for (CloneablePackage cloneablePackage : OEMCloneablePackageArrayList) {
                if(cloneablePackage.getLabel() != null){
                    String packageName = cloneablePackage.getPackageName();
                    removeOldPreference(packageName);
                    notInstalledPreference= new Preference(context);
                    notInstalledPreferenceCategory.addPreference(notInstalledPreference);
                    notInstalledPreference.setKey(packageName);
                    notInstalledPreference.setEnabled(false);
                    notInstalledPreference.setTitle(cloneablePackage.getLabel());
                    Drawable icon = cloneablePackage.getIconDrawable();
                    notInstalledPreference.setIcon(icon);
                }
            }
            final Preference cloneTipPreference = new Preference(context);
            cloneTipPreference.setSelectable(false);
            cloneTipPreference.setKey(CLONE_TIP_PREFERENCE_KEY);
            cloneTipPreference.setSummary(R.string.app_clone_tip);
            screen.addPreference(cloneTipPreference);
        }else{
            screen.setEnabled(false);
        }
    }

    private void removeOldPreference(String key){
        Preference preference = getPreferenceScreen().findPreference(key);
        if(preference != null){
            getPreferenceScreen().removePreference(preference);
        }
    }


    private List<CloneablePackage> getDefaultCloneablePackageList(){
        List<CloneablePackage> CloneablePackageList = new ArrayList<>();
        List<String> CloneablePackageStringList = mCloneHelper.getDefaultCloneablePackages();
        if(CloneablePackageStringList != null) {
            for (String cloneablePackageString : CloneablePackageStringList) {
                CloneablePackage cloneablePackage = getClonePackage(cloneablePackageString, false);
                if (cloneablePackage != null) {
                    CloneablePackageList.add(cloneablePackage);
                }
            }
        }
        return CloneablePackageList;
    }

    private List<CloneablePackage> getOEMCloneablePackageList(){
        List<CloneablePackage> CloneablePackageList = new ArrayList<>();
        List<String> CloneablePackageStringList = mCloneHelper.getOEMCloneablePackages();
        if(CloneablePackageStringList != null){
            for(String cloneablePackageString: CloneablePackageStringList){
                CloneablePackage cloneablePackage = getClonePackage(cloneablePackageString,true);
                if(cloneablePackage != null){
                    CloneablePackageList.add(cloneablePackage);
                }
            }
        }
        return CloneablePackageList;
    }

    private void loadFromXml() {
        if (mPackageInfoMap == null) {
            mPackageInfoMap = new HashMap<>(10);
            final File configXML= new File(PACKAGE_INFO_FILE_PATH);
            if (!configXML.exists()){
                Log.e(TAG, "package info file " + PACKAGE_INFO_FILE_PATH + " not exist");
                return;
            }
            try {
                AtomicFile userListFile = new AtomicFile(configXML);
                FileInputStream fis  = userListFile.openRead();
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(fis, StandardCharsets.UTF_8.name());
                int type;
                while ((type = parser.next()) != XmlPullParser.START_TAG
                        && type != XmlPullParser.END_DOCUMENT);
                if (type != XmlPullParser.START_TAG) {
                    Log.e(TAG, "Unable to parse " + PACKAGE_INFO_FILE_PATH);
                    return;
                }

                if (parser.getName().equals("policy")) {
                    while ((type = parser.next()) != XmlPullParser.END_DOCUMENT) {
                        if (type == XmlPullParser.START_TAG) {
                            final String name = parser.getName();
                            if (name.equals("inbox-package")) {
                                String pkg = parser.getAttributeValue(null, "name");
                                String iconName = parser.getAttributeValue(null, "icon");
                                String label = parser.getAttributeValue(null, "label");
                                mPackageInfoMap.put(pkg, new PackageInfo(pkg,iconName,label));
                            }
                        }
                    }
                }
            }catch (Exception e){
                Log.e(TAG, e.toString());
            }
        }
    }

    private CloneablePackage getClonePackage(String pkg,boolean oem){
        if(mPackageInfoMap == null){
            loadFromXml();
        }
        PackageInfo packageInfo = mPackageInfoMap.get(pkg);
        if (packageInfo != null){
            return new CloneablePackage(packageInfo.getPackageName(),packageInfo.getIcon(),packageInfo.getLabel(),oem);
        }else{
            return new CloneablePackage(pkg,null,null,oem);
        }
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsProto.MetricsEvent.ACCESSIBILITY;
    }

    private class PackageInfo{
        private String mPackageName;
        private String mLabel;
        private String mIcon;

        public PackageInfo(String name,String icon,String label){
            this.mPackageName = name;
            this.mLabel = label;
            this.mIcon = icon;
        }


        public String getPackageName() {
            return mPackageName;
        }

        public void setPackageName(String packageName) {
            mPackageName = packageName;
        }

        public String getLabel() {
            return mLabel;
        }

        public void setLabel(String label) {
            mLabel = label;
        }

        public String getIcon() {
            return mIcon;
        }

        public void setIcon(String icon) {
            mIcon = icon;
        }
    }

    private class CloneablePackage{
        /**
         * icon directory path
         */
        private static final String CONFIG_ICON_DIR_PATH = "/system/etc/container/icon/";
        /**
         * package name
         */
        private String mPackageName;
        /**
         * package icon
         */
        private Drawable mIcon;
        /**
         * package icon name for packages not installed
         */
        private String mIconName;
        /**
         * package label
         */
        private String mLabel;
        /**
         * whether package is in OEM white list
         */
        private boolean mOEM;
        /**
         * whether package has been installed
         */
        private boolean mInstalled;
        /**
         * Whether package has been cloned.
         */
        private boolean mCloned;


        /**
         * Constructor for installed cloneable package
         */
        public CloneablePackage(String packageName,Drawable icon,String label,boolean oem,boolean cloned){
            this.mPackageName = packageName;
            this.mLabel = label;
            this.mOEM = oem;
            this.mInstalled = true;
            this.mCloned = cloned;
            if(oem){
                this.mIcon = getWarningDrawable(icon);
            }else{
                this.mIcon = icon;
            }
            this.mIconName = null;
        }

        /**
         * Constructor for not installed cloneable package
         * @param iconName file name of icon
         */
        public CloneablePackage(String packageName,String iconName,String label,boolean oem){
            this.mPackageName = packageName;
            this.mLabel = label;
            this.mInstalled = false;
            this.mCloned = false;
            this.mIconName = iconName;
            this.mIcon =null;
            this.mOEM = oem;
        }

        public String toString(){
            return "name="+mPackageName+" label="+mLabel+" mInstalled="+mInstalled+" mCloned="+mCloned+" mOEM="+mOEM;
        }

        public String getPackageName() {
            return mPackageName;
        }

        public void setPackageName(String packageName) {
            mPackageName = packageName;
        }

        public Drawable getIcon() {
            return mIcon;
        }

        public void setIcon(Drawable icon) {
            mIcon = icon;
        }

        public String getLabel() {
            return mLabel;
        }

        public void setLabel(String label) {
            mLabel = label;
        }

        public boolean isInstalled() {
            return mInstalled;
        }

        public void setInstalled(boolean installed) {
            mInstalled = installed;
        }

        public boolean isCloned() {
            return mCloned;
        }

        public void setCloned(boolean cloned) {
            mCloned = cloned;
        }

        public boolean isOEM() {
            return mOEM;
        }

        public void setOEM(boolean OEM) {
            mOEM = OEM;
        }

        public String getIconName() {
            return mIconName;
        }

        public void setIconName(String iconName) {
            mIconName = iconName;
        }

        /**
         * add a warning badge to the icon
         * @param drawable icon
         * @return icon with warning badge
         */
        private Drawable getWarningDrawable(Drawable drawable){
            final int badgedWidth = drawable.getIntrinsicWidth();
            final int badgedHeight = drawable.getIntrinsicHeight();
            Rect badgeLocation = new Rect(badgedWidth * 3 / 5, badgedHeight * 3 / 5, badgedWidth, badgedHeight);
            Drawable badgeIcon = getActivity().getDrawable(R.drawable.ic_clone_warning);
            return getBadgedDrawable(drawable, badgeIcon, badgeLocation, true);
        }

        private Drawable getBadgedDrawable(Drawable drawable, Drawable badgeDrawable,
                                           Rect badgeLocation, boolean tryBadgeInPlace) {
            final int badgedWidth = drawable.getIntrinsicWidth();
            final int badgedHeight = drawable.getIntrinsicHeight();
            final boolean canBadgeInPlace = tryBadgeInPlace
                    && (drawable instanceof BitmapDrawable)
                    && ((BitmapDrawable) drawable).getBitmap().isMutable();

            final Bitmap bitmap;
            if (canBadgeInPlace) {
                bitmap = ((BitmapDrawable) drawable).getBitmap();
            } else {
                bitmap = Bitmap.createBitmap(badgedWidth, badgedHeight, Bitmap.Config.ARGB_8888);
            }
            Canvas canvas = new Canvas(bitmap);

            if (!canBadgeInPlace) {
                drawable.setBounds(0, 0, badgedWidth, badgedHeight);
                drawable.draw(canvas);
            }

            if (badgeLocation != null) {
                if (badgeLocation.left < 0 || badgeLocation.top < 0
                        || badgeLocation.width() > badgedWidth || badgeLocation.height() > badgedHeight) {
                    throw new IllegalArgumentException("Badge location " + badgeLocation
                            + " not in badged drawable bounds "
                            + new Rect(0, 0, badgedWidth, badgedHeight));
                }
                badgeDrawable.setBounds(0, 0, badgeLocation.width(), badgeLocation.height());

                canvas.save();
                canvas.translate(badgeLocation.left, badgeLocation.top);
                badgeDrawable.draw(canvas);
                canvas.restore();
            } else {
                badgeDrawable.setBounds(0, 0, badgedWidth, badgedHeight);
                badgeDrawable.draw(canvas);
            }

            if (!canBadgeInPlace) {
                BitmapDrawable mergedDrawable = new BitmapDrawable(getActivity().getResources(), bitmap);

                if (drawable instanceof BitmapDrawable) {
                    BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                    mergedDrawable.setTargetDensity(bitmapDrawable.getBitmap().getDensity());
                }

                return mergedDrawable;
            }

            return drawable;
        }


        /**
         * get drawable of icon from icon name
         */
        Drawable getIconDrawable(){
            Drawable icon;
            if(mIconName != null) {
                Bitmap bMap = BitmapFactory.decodeFile(CONFIG_ICON_DIR_PATH + mIconName);
                final float scale = getActivity().getResources().getDisplayMetrics().density;
                int px = (int) (48 * scale + 0.5f);
                icon = new BitmapDrawable(getActivity().getResources(), Bitmap.createScaledBitmap(bMap, px, px, true));
            }else {
                icon = getActivity().getDrawable(R.drawable.ic_clone_default_icon);
            }
            if(mOEM){
                 icon = getWarningDrawable(icon);
            }
            return toGray(icon);
        }

        private Drawable toGray(Drawable drawableOriginal)
        {
            if(drawableOriginal != null){
                int width, height;
                Bitmap bmpOriginal = drawableToBitmap(drawableOriginal);
                height = bmpOriginal.getHeight();
                width = bmpOriginal.getWidth();
                Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(bmpGrayscale);
                Paint paint = new Paint();
                ColorMatrix cm = new ColorMatrix();
                cm.setSaturation(0);
                ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
                paint.setColorFilter(f);
                c.drawBitmap(bmpOriginal, 0, 0, paint);
                return  new BitmapDrawable(getActivity().getResources(), bmpGrayscale);
            }else{
                return null;
            }

        }

        public Bitmap drawableToBitmap (Drawable drawable) {
            Bitmap bitmap;
            if (drawable instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                if(bitmapDrawable.getBitmap() != null) {
                    return bitmapDrawable.getBitmap();
                }
            }

            if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        }
    }


}
