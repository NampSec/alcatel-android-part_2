/* Copyright (C) 2016 Tcl Corporation Limited */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/03/2015|     jianguang.sun    |     task-706238      |  Related settings*/
/*           |                      |                      |      of the      */
/*           |                      |                      |     fingerprint  */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings.fingerprint;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.ContentObserver;
import android.graphics.Color;
import android.hardware.fingerprint.Fingerprint;
import android.hardware.fingerprint.FingerprintManager;
import android.hardware.fingerprint.FingerprintManager.RemovalCallback;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.internal.logging.MetricsProto.MetricsEvent;

import com.android.settings.ChooseLockSettingsHelper;
import com.android.settings.InstrumentedActivity;
import com.android.settings.R;
import com.android.settings.SubSettings;

import android.os.UserHandle;
import mst.widget.toolbar.Toolbar;
import android.widget.LinearLayout;
//import com.tct.util.AlertDialogHelper;

/*MODIFIED-BEGIN by jianguang.sun, 2016-04-14,BUG-1938007*/
/*MODIFIED-END by jianguang.sun,BUG-1938007*/
//[BUGFIX]-Add-BEGIN by TCTNB.caixia.chen,03/21/2016,task 1427498
//[BUGFIX]-Add-END by TCTNB.caixia.chen

public class RenameDeleteActivity extends InstrumentedActivity {
    private LinearLayout bt_rename;
    private Button bt_delete;
    private Fingerprint mFingerprint;
    private int FPRENAME = 1;
    private int FPDELETE = 2;
    protected static final boolean DEBUG = true;
    private static final String TAG = "RenameDeleteActivity";
    private FingerprintManager mFingerprintManager;
    private static final int CONFIRM_REQUEST = 10;
    private byte[] mToken;
    private TextView mTitle;//[FEATURE]-ADD by TSNJ.heng.zhang1,2016-03-17,Task-1546321

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setMstContentView(R.layout.activity_rename_delete);
        setTitle(getString(R.string.fingerprint_rename_delate_title));
        showBackIcon(true);

        mFingerprintManager = (FingerprintManager) getSystemService(Context.FINGERPRINT_SERVICE);

        mFingerprint = (Fingerprint) getIntent().getParcelableExtra(
                "fingerprint");
        mTitle = (TextView) findViewById(R.id.rename_delete_title);
        if (mFingerprint != null) {
            CharSequence fpName = mFingerprint.getName();
            mTitle.setText(fpName);
        } else {
            Log.e(TAG, "null fingerprint");
            finish();
        }

        bt_rename = (LinearLayout)findViewById(R.id.bt_fp_rename);
        bt_rename.setOnClickListener(new MyOnClickListener(FPRENAME));
        bt_delete = (Button) findViewById(R.id.bt_fp_delete);
        bt_delete.setOnClickListener(new MyOnClickListener(FPDELETE));
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        mToken = getIntent().getByteArrayExtra(
                ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
//        if (mToken == null) {
//            launchChooseOrConfirmLock();
//        }

        //[BUGFIX]-Add-BEGIN by TCTNB.caixia.chen,03/21/2016,task 1427498
        if (getIntent().getIntExtra(
                ChooseLockSettingsHelper.EXTRA_KEY_ENROLL_FINGERPRINT_TAG, 0) == FingerprintManager.FP_TAG_PRIVATE_MODE) {
            mModeObserver = new OwnerAndPrivacyModeObserver();
            getContentResolver().registerContentObserver(Settings.System.getUriFor(Settings.System.CURRENT_PHONE_MODE), true, mModeObserver);
        }
        //[BUGFIX]-Add-END by TCTNB.caixia.chen
    }

    private void launchChooseOrConfirmLock() {
        Intent intent = new Intent();
        long challenge = getSystemService(FingerprintManager.class).preEnroll();
        ChooseLockSettingsHelper helper = new ChooseLockSettingsHelper(this);
        if (!helper
                .launchConfirmationActivity(
                        CONFIRM_REQUEST,
                        getString(R.string.security_settings_fingerprint_preference_title),
                        null, null, challenge)) {
            finish();
        }
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.FINGERPRINT;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            finish();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //CharSequence fpName = mFingerprint.getName();
        //setTitle(fpName);
    }

    private RemovalCallback mRemoveCallback = new RemovalCallback() {

        @Override
        public void onRemovalSucceeded(Fingerprint fingerprint) {
            Intent intent = new Intent();
            intent.setAction("delete");
            intent.putExtra("fingerprint", fingerprint.getFingerId());
            setResult(RESULT_OK, intent);
            finish();
        }

        @Override
        public void onRemovalError(Fingerprint fp, int errMsgId,
                CharSequence errString) {
            Toast.makeText(RenameDeleteActivity.this, errString,
                    Toast.LENGTH_SHORT);
        }
    };
    //task 1487712 mod begin by jianguang.sun
    private class MyOnClickListener implements View.OnClickListener {

        private EditText et_rename;
        private TextView tx; //MODIFIED by jianguang.sun, 2016-04-14,BUG-1938007
        private Button btnCancel;
        private int index = 0;

        public MyOnClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {

            if (index == FPRENAME) {
                /*MODIFIED-BEGIN by jianguang.sun, 2016-04-14,BUG-1938007*/
                final int max_length = 16;
                LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View view = layoutInflater.inflate(R.layout.alert_rename_fp, null);
                tx = (TextView) view.findViewById(R.id.alert_limit);
                et_rename = (EditText)view.findViewById(R.id.et_name);
                CharSequence name = mFingerprint.getName();
                //[FEATURE]-ADD-BEGIN by liang.zhang1-nb,11/14/2016
                //InputFilter[] filter = new InputFilter[1];
//                filter[0] = new InputFilter.LengthFilter(max_length){
//                    @Override
//                    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
//                        if (dstart == max_length){
//                            tx.setText(R.string.fp_rename_limit);
//                            tx.setVisibility(View.VISIBLE);
//                        }else{
//                            tx.setVisibility(View.GONE);
//                        }
//                        return super.filter(source, start, end, dest, dstart, dend);
//                    }
//                };
                et_rename.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        int charLen = getStringLength(s.toString());
                        if(btnCancel != null) {
                            if (charLen > 16 || charLen == 0) {
                                btnCancel.setEnabled(false);
                            } else {
                                btnCancel.setEnabled(true);
                            }
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                //et_rename.setFilters(filter);
                //[FEATURE]-ADD-END by liang.zhang1-nb,11/14/2016
                et_rename.setText(name);
                et_rename.selectAll();

                final AlertDialog alertDialog = new AlertDialog.Builder(
                        RenameDeleteActivity.this)
                        .setTitle(R.string.fingerprint_name) //MODIFIED by heng.zhang1, 2016-04-07,BUG-1546321
                        .setView(view)
                        /*MODIFIED-END by jianguang.sun,BUG-1938007*/
                        .setPositiveButton(R.string.fingerprint_delete_sure,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        onRenameClick(dialog);

                                    }

                                })
                        .setNegativeButton(R.string.fingerprint_delete_cancel,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        dialog.dismiss();
                                    }
                                }).show();
                /*MODIFIED-BEGIN by heng.zhang1, 2016-04-07,BUG-1546321*/
                btnCancel = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                //AlertDialogHelper.setTitleTextAlignment(alertDialog,TextView.TEXT_ALIGNMENT_CENTER);
                //[BUGFIX]-ADD by TSNJ.heng.zhang1,2016-06-06,Defect-2252136
                //AlertDialogHelper.setButtonColor(alertDialog, AlertDialogHelper.BUTTON_POSITIVE, Color.parseColor("#45a142"));

            } else if (index == FPDELETE) {
                CharSequence name = getResources().getString(R.string.security_settings_fingerprint_preference_title);
                //[BUGFIX]-ADD_BEGIN by TSNJ.heng.zhang1,2016-05-25,Defect-1962405
                AlertDialog alertDialog = new AlertDialog.Builder(RenameDeleteActivity.this)
                        //.setTitle(R.string.fingerprint_delete)
                        .setMessage(getResources().getString(R.string.fingerprint_delete_dialog, name))
                        /*MODIFIED-END by heng.zhang1,BUG-1546321*/
                        .setPositiveButton(R.string.fingerprint_delete_sure,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        onDeleteClick(dialog);
                                    }

                                })

                        .setNegativeButton(R.string.fingerprint_delete_cancel,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                            int which) {
                                        dialog.dismiss();
                                    }

                                }).show();
                //AlertDialogHelper.setButtonColor(alertDialog, AlertDialogHelper.BUTTON_POSITIVE, AlertDialogHelper.COLOR_NEGATIVE);
                //AlertDialogHelper.setButtonColor(alertDialog, AlertDialogHelper.BUTTON_NEGATIVE, AlertDialogHelper.COLOR_NEUTRAL);
                //[BUGFIX]-ADD_END by TSNJ.heng.zhang1,2016-05-25,Defect-1962405
            }
        }

        private void onDeleteClick(DialogInterface dialog) {
            if (DEBUG)
                Log.v(TAG, "Removing fpId=" + mFingerprint.getFingerId());
             mFingerprintManager.remove(mFingerprint, UserHandle.myUserId(), mRemoveCallback);
            dialog.dismiss();
        }

        private void onRenameClick(DialogInterface dialog) {
            if (DEBUG)
                Log.v(TAG, "Rename fpId=" + mFingerprint.getFingerId());
            final String newName = et_rename.getText().toString();
            final CharSequence name = mFingerprint.getName();
            if (!newName.equals(name)) {
                if (newName.equals("")) {
                    Toast.makeText(RenameDeleteActivity.this,
                            R.string.name_empty_message, Toast.LENGTH_SHORT)
                            .show();
                } else {
                    if (DEBUG) {
                        Log.v(TAG, "rename " + name + " to " + newName);
                    }
                    mFingerprintManager.rename(mFingerprint.getFingerId(),UserHandle.myUserId(),
                            newName);
                    //setTitle(newName);
                    mTitle.setText(newName);//[FEATURE]-ADD by TSNJ.heng.zhang1,2016-03-17,Task-1546321
                    // 通知修改管理界面显示
                    Intent intent = new Intent();
                    intent.setAction("rename");
                    setResult(RESULT_OK, intent);
                }

            }
            dialog.dismiss();
        }
    }
    //task 1487712 mod end by jianguang.sun
  //[TASK]-Mod-BEGIN by TCTNB.jianguang.sun,02/22/2016,TASK-1487712
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CONFIRM_REQUEST) {
            if (resultCode == RESULT_OK) {
                // The lock pin/pattern/password was set. Start enrolling!
                if (data != null) {
                    mToken = data.getByteArrayExtra(
                            ChooseLockSettingsHelper.EXTRA_KEY_CHALLENGE_TOKEN);
                }
            }
        }
        if (mToken == null) {
            // Didn't get an authentication, finishing
            finish();
        }
    }
  //[TASK]-Mod-END by TCTNB.jianguang.sun,02/22/2016,TASK-1487712

    //[BUGFIX]-Add-BEGIN by TCTNB.caixia.chen,03/21/2016,task 1427498
    //screen that privacy mode settings invoke in owner mode should be destroyed when phone switch to privacy mode
    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mModeObserver != null) {
                getContentResolver().unregisterContentObserver(mModeObserver);
                mModeObserver = null;
            }
        } catch (Exception e) {
        }
    }

    private ContentObserver mModeObserver;
    private class OwnerAndPrivacyModeObserver extends ContentObserver {
        public OwnerAndPrivacyModeObserver() {
            super(new Handler());
        }

        @Override
        public void onChange(boolean selfChange) {
            if (Settings.System.getInt(getContentResolver(), Settings.System.CURRENT_PHONE_MODE, 0) == 1) {
                finish();
            }
        }
    }
    //[BUGFIX]-Add-END by TCTNB.caixia.chen

    @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
    }
    //[FEATURE]-ADD-BEGIN by liang.zhang1-nb,11/14/2016
    private int getStringLength(String str) {
        int length = 0;
        for(int i=0;i<str.length();i++) {
            int ascii = Character.codePointAt(str,i);
            if(ascii >=0 && ascii <= 255)
                length++;
            else
                length+=2;
        }
        return length;
    }
    //[FEATURE]-ADD-END by liang.zhang1-nb,11/14/2016
}
