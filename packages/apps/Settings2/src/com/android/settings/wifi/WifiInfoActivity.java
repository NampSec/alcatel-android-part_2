/* Copyright (C) 2016 Tcl Corporation Limited */

/******************************************************************************/
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 2016/10/11|     jianhong.yang    |     task 3046476     |  Ergo development*/
/*           |                      |                      |  create new file */
/* ----------|----------------------|----------------------|----------------- */

package com.android.settings.wifi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import mst.preference.CheckBoxPreference;
import mst.preference.Preference;
import mst.preference.PreferenceScreen;
import mst.preference.SwitchPreference; // MODIFIED by yiwei.zhu, 2016-11-17,BUG-3082231


import com.android.settings.WidgetPreference;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.RestrictedSettingsFragment;
import com.android.settings.applications.LayoutPreference;
import com.android.settingslib.wifi.AccessPoint;

public class WifiInfoActivity extends RestrictedSettingsFragment implements View.OnClickListener{
    public static final String TAG ="WifiInfoActivity";
    private Context mContext;

    private Bundle mApInfo;

    private String mSsid;
    private String mBssid;
    private String mSecurity;
    private DetailedState mState;
    private WifiConfiguration mApConfig;
    private WifiInfo mInfo;
    private int mLevel;
    private int mSpeed;
    private int mFrequencyInt;
    private int mIpStyle;
    private String mIpAddr;
    private int mNetworkId;
    private int mSecurityInt;
    private String mSecurityString;
    private boolean mIsActive;
    private boolean mIsEphemeral;

    private String[] mLevels;

    private WidgetPreference mSsidPreference;
    private WidgetPreference mStatePreference;
    private WidgetPreference mSignalreference;
    private WidgetPreference mLinkSpeedPreference;
    private WidgetPreference mFrequencyPreference;
    private WidgetPreference mSecurityference;
    private SwitchPreference mMarkPayAPPreference; // MODIFIED by yiwei.zhu, 2016-11-17,BUG-3082231
    private WidgetPreference mAdvancedPreference;
    private LayoutPreference mForgetNetworkPreference;
    private Button mForgetNetwork;

    private Resources mRes;
    private MarkPayApManager mMarkPayApManager;
    private WifiManager.ActionListener mForgetListener;
    private WifiManager mWifiManager;

    private static final String KEY_WIFI_SSID = "wifi_ssid";
    private static final String KEY_WIFI_STATUS = "wifi_status";
    private static final String KEY_WIFI_SIGNAL = "wifi_signal";
    private static final String KEY_WIFI_SPEED = "wifi_speed";
    private static final String KEY_WIFI_FREQUENCY = "wifi_frequency";
    private static final String KEY_WIFI_SECURITY = "wifi_security";
    private static final String KEY_MARK_PAY_AP = "mark_pay_ap";
    private static final String KEY_WIFI_MENU_ADVANCED = "wifi_menu_advanced";
    private static final String KEY_FORGET_NETWORK = "forget_network";

    private String mFetchMessageUnknown;

    public WifiInfoActivity(String restrictionKey) {
        super(restrictionKey);
        // TODO Auto-generated constructor stub
    }

    public WifiInfoActivity() {
        super(UserManager.DISALLOW_CONFIG_WIFI);
    }

    @Override
    public void onCreate(Bundle icicle) {
        // TODO Auto-generated method stub
        super.onCreate(icicle);

        addPreferencesFromResource(R.xml.wifi_info_activity);
        getNetworkInfo();
        initView();
    }

    private void getNetworkInfo() {
        mApInfo = getArguments();

        if (mApInfo != null) {
            mIsActive = mApInfo.getBoolean("IS_ACTIVE");
            mIsEphemeral = mApInfo.getBoolean("IS_EPHEMERAL");
            mSsid = mApInfo.getString("SSID");
            mSecurityString = mApInfo.getString("SECURITY_STR");
            mSecurityInt = mApInfo.getInt("SECURITY_INT");
            mLevel = mApInfo.getInt("LEVEL");
            mApConfig = mApInfo.getParcelable("CONFIG");
            mInfo = mApInfo.getParcelable("WIFI_INFO");
            mState = "".equals(mApInfo.getString("STATE")) ? null
                    : DetailedState.valueOf(mApInfo.getString("STATE"));
            mSpeed = mApInfo.getInt("SPEED");
            mFrequencyInt = mApInfo.getInt("FREQUENCY_INT");
            mIpStyle = mApInfo.getInt("IPSTYLE");
            mIpAddr = mApInfo.getString("IPADRR");
            mNetworkId = mApInfo.getInt("NETWORK_ID");

            mBssid = mInfo.getBSSID();
        }
    }

    private void initView() {
        mContext = getContext();
        mRes = getActivity().getResources();
        mMarkPayApManager = new MarkPayApManager(getContext());
        mWifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

        mFetchMessageUnknown = mRes.getString(R.string.fetch_message_unknown);

        mSsidPreference = (WidgetPreference)findPreference(KEY_WIFI_SSID);
        mStatePreference = (WidgetPreference)findPreference(KEY_WIFI_STATUS);
        mSignalreference = (WidgetPreference)findPreference(KEY_WIFI_SIGNAL);
        mLinkSpeedPreference = (WidgetPreference)findPreference(KEY_WIFI_SPEED);
        mFrequencyPreference = (WidgetPreference)findPreference(KEY_WIFI_FREQUENCY);
        mSecurityference = (WidgetPreference)findPreference(KEY_WIFI_SECURITY);
        mMarkPayAPPreference = (SwitchPreference)findPreference(KEY_MARK_PAY_AP); // MODIFIED by yiwei.zhu, 2016-11-17,BUG-3082231

        mForgetNetworkPreference = (LayoutPreference)findPreference(KEY_FORGET_NETWORK);
        mForgetNetwork = (Button)mForgetNetworkPreference.findViewById(R.id.forget_network_button);
        mForgetNetwork.setOnClickListener(this);

        mForgetListener = new WifiManager.ActionListener() {
            @Override
            public void onSuccess() {
            }
            @Override
            public void onFailure(int reason) {
                Activity activity = getActivity();
                if (activity != null) {
                    Toast.makeText(activity,
                        R.string.wifi_failed_forget_message,
                        Toast.LENGTH_SHORT).show();
                }
            }
        };

        mSsidPreference.setSelectable(false);
        mStatePreference.setSelectable(false);
        mSignalreference.setSelectable(false);
        mLinkSpeedPreference.setSelectable(false);
        mFrequencyPreference.setSelectable(false);
        mSecurityference.setSelectable(false);
        mForgetNetworkPreference.setSelectable(false);

        mMarkPayAPPreference.setChecked(mMarkPayApManager.isMarkedPayAp(mNetworkId, mSsid));

        mSsidPreference.setGone(true);
        mStatePreference.setGone(true);
        mSignalreference.setGone(true);
        mLinkSpeedPreference.setGone(true);
        mFrequencyPreference.setGone(true);
        mSecurityference.setGone(true);

        mSsidPreference.setTitle(mSsid);
        mStatePreference.setSummary(getApState());
        mSignalreference.setSummary(getSignalString());
        mLinkSpeedPreference.setSummary(getLinkSpeed());
        mFrequencyPreference.setSummary(getFrequency());
        mSecurityference.setSummary(mSecurityString);
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        if (mApInfo != null && mWifiManager != null) {
            for (WifiConfiguration config : mWifiManager.getConfiguredNetworks()) {
                if (mNetworkId == config.networkId) {
                    mApInfo.putParcelable("CONFIG", config);
                }
            }
        }
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
                        Preference preference) {
        // TODO Auto-generated method stub
        String key = preference.getKey();
        if(KEY_MARK_PAY_AP.equals(key)) {
            if(mMarkPayAPPreference.isChecked()) {
                if(! mMarkPayApManager.add(mNetworkId, mSsid)) {
                    Toast.makeText(mContext, R.string.add_pay_ap_failed, Toast.LENGTH_SHORT).show();
                }
            } else {
                if(!mMarkPayApManager.remove(mNetworkId)) {
                    Toast.makeText(mContext, R.string.remove_pay_ap_failed, Toast.LENGTH_SHORT).show();
                }
            }
        } else if(KEY_WIFI_MENU_ADVANCED.equals(key)) {
            startActivity();
        }
        return super.onPreferenceTreeClick(preferenceScreen,preference);
    }

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.WIFI_ADVANCED;
    }

    private void startActivity() {
        // TODO Auto-generated method stub
        Intent intent = new Intent();

        intent.putExtra("SelectedAccessPoint", mApInfo);
        intent.setClassName(getActivity(), "com.android.settings.wifi.ApAdvancedSettings");
        startActivity(intent);
    }

    private String getApState() {
        String state = null;
        if (mState != null) {
            boolean isEphemeral = mIsEphemeral;
            WifiConfiguration config = mApConfig;
            String providerFriendlyName = null;
            if (config != null && config.isPasspoint()) {
                providerFriendlyName = config.providerFriendlyName;
            }
            state = AccessPoint.getSummary(
                    getContext(), mState, isEphemeral, providerFriendlyName);
        }

        return state != null ? state : mFetchMessageUnknown;
    }

    private String getSignalString() {
        mLevels = mRes.getStringArray(R.array.wifi_signal);

        return (mLevel > -1 && mLevel < mLevels.length) ? mLevels[mLevel] : null;
    }

    private String getFrequency() {
        String band = null;
        if (mFrequencyInt >= AccessPoint.LOWER_FREQ_24GHZ
                && mFrequencyInt < AccessPoint.HIGHER_FREQ_24GHZ) {
          band = mRes.getString(R.string.wifi_band_24ghz);
        } else if (mFrequencyInt >= AccessPoint.LOWER_FREQ_5GHZ
                && mFrequencyInt < AccessPoint.HIGHER_FREQ_5GHZ) {
          band = mRes.getString(R.string.wifi_band_5ghz);
        }

        return band != null ? band : mFetchMessageUnknown;
    }

    private String getLinkSpeed() {
        String linkSpeed = null;
        if (mInfo != null && mInfo.getLinkSpeed() != -1) {
            linkSpeed = String.format(mRes.getString(R.string.link_speed), mInfo.getLinkSpeed());
        }

        return linkSpeed != null ? linkSpeed : mFetchMessageUnknown;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        mWifiManager.forget(mNetworkId, mForgetListener);
        if(!mMarkPayApManager.remove(mNetworkId)) {
            Toast.makeText(mContext, R.string.remove_pay_ap_failed, Toast.LENGTH_SHORT).show();
        }
        finish();
    }
}
