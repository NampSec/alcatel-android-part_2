/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.lockapp.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.hardware.fingerprint.FingerprintManager.AuthenticationCallback;
import android.hardware.fingerprint.FingerprintManager.AuthenticationResult;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.Toast;

import com.android.settings.R;
import com.android.settings.lockapp.activity.ApplicationLockActivity;
import com.android.settings.lockapp.ui.ChooseLockPatternActivity.Stage;
import com.android.settings.lockapp.ui.LockPatternView.Cell;
import com.android.settings.lockapp.ui.LockPatternView.DisplayMode;
import com.android.settings.lockapp.utils.ApplicationLockUtils;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;

import android.provider.Settings;

public class PatternLockActivity extends Activity {
    private final static String TAG = "PatternLockActivity";
    protected LockPatternView mLockPatternView;
    private FingerprintManager mFpm;
    private CancellationSignal mFingerprintCancelSignal;
    private ImageView mFingerprintView;
    private Button mForgotPassword;
    private WindowManager windowManager;
    private Window window;
    private WindowManager.LayoutParams layoutParams;
    private String mPackageName = null;
    private PackageManager localPackageManager;
    private static Toast toast;

    private enum Stage {
        NeedToUnlock,
        NeedToUnlockWrong,
        LockedOut
    }

    private Runnable mClearPatternRunnable = new Runnable() {
        public void run() {
            mLockPatternView.clearPattern();
        }
    };

    protected LockPatternView.OnPatternListener mChooseNewLockPatternListener =
            new LockPatternView.OnPatternListener() {

            public void onPatternStart() {
                mLockPatternView.removeCallbacks(mClearPatternRunnable);
                patternInProgress();
            }

            public void onPatternCleared() {
                mLockPatternView.removeCallbacks(mClearPatternRunnable);
            }

            public void onPatternDetected(List<LockPatternView.Cell> pattern) {
                mLockPatternView.setEnabled(false);
                startCheckPattern(pattern);
                return;
            }

            private void patternInProgress() {

            }

            @Override
            public void onPatternCellAdded(List<Cell> pattern) {

            }
     };
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.parseColor("#f8f8f8"));
        setContentView(R.layout.activity_confirm_lock_pattern);
        getActionBar().setTitle(R.string.app_lock_pw);
        mLockPatternView = (LockPatternView) findViewById(R.id.lockPattern);
        mFingerprintView = (ImageView) findViewById(R.id.lock_icon);
        mForgotPassword = (Button) findViewById(R.id.forgot);
        if (!ApplicationLockUtils.shouldForgotPasswordShowed(PatternLockActivity.this)) {
            mForgotPassword.setVisibility(View.GONE);
        }
        mPackageName = getIntent().getStringExtra("package_name");
        Log.i(TAG,"PatternLock. packageName = " + mPackageName);
        localPackageManager = getPackageManager();
        mFpm = (FingerprintManager) this.getSystemService(Context.FINGERPRINT_SERVICE);
        //authenticateFingerPrint();
        mLockPatternView.setOnPatternListener(mChooseNewLockPatternListener);
        mLockPatternView.setTactileFeedbackEnabled(true);
        updateStage(Stage.NeedToUnlock);
        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PatternLockActivity.this,
                		ForgotPasswordActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private FingerprintManager.AuthenticationCallback mAuthenticationCallback = new AuthenticationCallback() {
        @Override
        public void onAuthenticationFailed() {
            Log.d(TAG, "onAuthenticationFailed");
            showToast(PatternLockActivity.this.getResources().
                    getString(R.string.fingerprint_failed));
        };

        @Override
        public void onAuthenticationSucceeded(AuthenticationResult result) {
            Log.d(TAG, "onAuthenticationSucceeded");
            mFingerprintCancelSignal = null;
            int fpid = result.getFingerprint().getFingerId();
            int tag = mFpm.tctGetTagForFingerprintId(fpid);

            if (tag == FingerprintManager.FP_TAG_COMMON) {
                if (mPackageName != null) {
                    localPackageManager.setApplicationLockStatus(mPackageName, 2);
                }
                finish();
            } else {
            	Log.d(TAG, "onAuthenticationFailed");
                showToast(PatternLockActivity.this.getResources().
                    getString(R.string.fingerprint_failed));
                authenticateFingerPrint();
            }
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            Log.d(TAG, "onAuthenticationHelp");
        }

        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            Log.d(TAG, "onAuthenticationError -- errMsgId: " + errMsgId);
            mFingerprintCancelSignal = null;
            if (errMsgId == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
                showToast(PatternLockActivity.this.getResources().
                    getString(R.string.fingerprint_error));
                mFingerprintView.setVisibility(View.INVISIBLE);
                SharedPreferenceUtil.editFingerPrintError(PatternLockActivity.this,true);
            }
        }

        @Override
        public void onAuthenticationAcquired(int acquireInfo) {
            Log.d(TAG, "onAuthenticationAcquired");
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        //[BUGFIX]-Add-BEGIN by TCTNB.caixia.chen,06/12/2016,2114657
        boolean shouldFinish = ApplicationLockUtils.isApplicationKilled(this, mPackageName);
        if (shouldFinish) {
            finish();
        }
        //[BUGFIX]-Add-END by TCTNB.caixia.chen
        authenticateFingerPrint();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        if (mFingerprintCancelSignal != null && !mFingerprintCancelSignal.isCanceled()) {
            mFingerprintCancelSignal.cancel();
        }
        mFingerprintCancelSignal = null;

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown");
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(SystemProperties.getInt("sys.security.power.mode", 2) == 1){
                Log.e(TAG,"sys.security.power.mode = 1");
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.tct.securitycenter","com.tct.securitycenter.batterymanager.LauncherInPowerSaver"));
                startActivityAsUser(intent, UserHandle.CURRENT);
                return false;
            }
            goHome();
            //super.onKeyDown(keyCode, event);
            return false;
        } else if (keyCode == KeyEvent.KEYCODE_MENU) {
            return true;
        }
        return true;
    }
    public void goHome() {
        Log.d(TAG, "goHome");
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        startActivity(homeIntent);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 500);
    }
    
    private void updateStage(Stage stage) {
        switch (stage) {
            case NeedToUnlock:
                Log.i(TAG,"updateStage  1");
                mLockPatternView.setEnabled(true);
                mLockPatternView.enableInput();
                mLockPatternView.clearPattern();
                break;
            case NeedToUnlockWrong:
                Log.i(TAG,"updateStage  2");
                Toast.makeText(this,
                        this.getResources().getString(R.string.wrong_password_input_again),
                        Toast.LENGTH_SHORT).show();
                mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
                mLockPatternView.setEnabled(true);
                mLockPatternView.enableInput();
                break;
            case LockedOut:
                Log.i(TAG,"updateStage  3");
                //mLockPatternView.clearPattern();
                // enabled = false means: disable input, and have the
                // appearance of being disabled.
                mLockPatternView.setEnabled(false); // appearance of being disabled
                break;
        }
    }

    private void startCheckPattern(final List<LockPatternView.Cell> pattern) {//处理图形
        String patternStr = LockPatternUtilsSelf.patternToString(pattern);
        Log.d("yshlock","PasswordLockActivity handleNext:  "+patternStr);
        String systemPatern = Settings.System.getString(getContentResolver(), "normal_password");
        if (patternStr != null && patternStr.equals(systemPatern)) {
            updateStage(Stage.LockedOut);

            if (mPackageName != null) {
                localPackageManager.setApplicationLockStatus(mPackageName, 2);
            }
            Log.i(TAG,"startCheckPattern");
            finish();
            SharedPreferenceUtil.editFingerPrintError(PatternLockActivity.this,false);
        } else {
            updateStage(Stage.NeedToUnlockWrong);
        }
    }

    private void authenticateFingerPrint() {
        Log.d(TAG, "authenticateFingerPrint");
        if (ApplicationLockUtils.isUnlockWithFingerprintPossible(0, mFpm, this)
                && ApplicationLockUtils.isFingerPrint4ApplicationLockOn(this)
                && !SharedPreferenceUtil.readFingerPrintError(this)) {
            if (mFingerprintCancelSignal != null && !mFingerprintCancelSignal.isCanceled()) {
                mFingerprintCancelSignal.cancel();
            }
            mFingerprintView.setVisibility(View.VISIBLE);
            mFingerprintCancelSignal = new CancellationSignal();
            mFpm.authenticate(null, mFingerprintCancelSignal, 0, mAuthenticationCallback, null, 0);
            //setFingerprintRunningState(FINGERPRINT_STATE_RUNNING);
        } else {
            mFingerprintView.setVisibility(View.INVISIBLE);
        }
    }

    private void showToast(String text) {
            if (null != toast){
                toast.setText(text);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.show();
	    } else {
                toast = Toast.makeText(PatternLockActivity.this, text, Toast.LENGTH_SHORT);
                toast.show();
            }
    }

}
