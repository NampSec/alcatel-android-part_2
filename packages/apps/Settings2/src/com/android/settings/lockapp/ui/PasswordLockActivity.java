/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.lockapp.ui;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.hardware.fingerprint.FingerprintManager.AuthenticationCallback;
import android.hardware.fingerprint.FingerprintManager.AuthenticationResult;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

import com.android.internal.widget.LockPatternUtils;

import android.view.inputmethod.InputMethodManager;
import android.os.UserManager;

import com.android.internal.widget.TextViewInputDisabler;

import com.android.settings.lockapp.activity.ApplicationLockActivity;
import com.android.settings.lockapp.utils.ApplicationLockUtils;
import com.android.settings.lockapp.utils.SharedPreferenceUtil;
import com.android.settings.lockapp.utils.StringUtils;
import com.android.settings.R;
import com.android.settings.Utils;

import android.provider.Settings;

public class PasswordLockActivity extends Activity implements TextWatcher, OnEditorActionListener {
    private final static String TAG = "PasswordLockActivity";
    private TextView mPasswordEntry;
    private FingerprintManager mFpm;
    private CancellationSignal mFingerprintCancelSignal;
    private ImageView mFingerprintView;
    private Button mForgotPassword;
    private WindowManager windowManager;
    private Window window;
    private WindowManager.LayoutParams layoutParams;
    private ImageButton mOkButton;
    private Button mDeleteButton;
    private ArrayList<Button> buttonArray = new ArrayList<Button>();
    private String mPasswordConfirmStr = "";
    private ArrayList<TextView> mPasswordEntryArray = new ArrayList<TextView>();
    private String mPackageName = null;
    private PackageManager localPackageManager;
    private static Toast toast;
    private boolean mIsAlpha;
    protected LockPatternUtils mLockPatternUtils;//
    protected int mEffectiveUserId;
    protected int mUserId;
    private TextViewInputDisabler mPasswordEntryInputDisabler;
    private InputMethodManager mImm;//软件盘

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.parseColor("#f8f8f8"));
        setContentView(R.layout.activity_confirm_lock_password);
        getActionBar().setTitle(R.string.app_lock_pw);
        mImm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        mPasswordEntry = (TextView) findViewById(R.id.password_entry);
        mPasswordEntry.setOnEditorActionListener(this);
        mPasswordEntryInputDisabler = new TextViewInputDisabler(mPasswordEntry);
        mLockPatternUtils = new LockPatternUtils(this);

        mFingerprintView = (ImageView) findViewById(R.id.lock_icon);
        mForgotPassword = (Button) findViewById(R.id.forgot);
        if (!ApplicationLockUtils.shouldForgotPasswordShowed(PasswordLockActivity.this)) {
            mForgotPassword.setVisibility(View.INVISIBLE);
        }
        mPackageName = getIntent().getStringExtra("package_name");
        Log.i(TAG, "PasswordLock. packageName = " + mPackageName);
        localPackageManager = getPackageManager();

        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PasswordLockActivity.this,
                        ForgotPasswordActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mFpm = (FingerprintManager) this.getSystemService(Context.FINGERPRINT_SERVICE);
        //authenticateFingerPrint();

        init();
    }

    public void init() {
        //判断当前数字密码类型，简单数字还是负责数字+字母
        Intent intent = getIntent();
        mUserId = Utils.getUserIdFromBundle(this, intent.getExtras());
        final UserManager userManager = UserManager.get(this);
        mEffectiveUserId = userManager.getCredentialOwnerProfile(mUserId);

        final int storedQuality = mLockPatternUtils.getKeyguardStoredPasswordQuality(
                mEffectiveUserId);
        mIsAlpha = DevicePolicyManager.PASSWORD_QUALITY_ALPHABETIC == storedQuality
                || DevicePolicyManager.PASSWORD_QUALITY_ALPHANUMERIC == storedQuality
                || DevicePolicyManager.PASSWORD_QUALITY_COMPLEX == storedQuality
                || DevicePolicyManager.PASSWORD_QUALITY_MANAGED == storedQuality;

        int currentType = mPasswordEntry.getInputType();
        mPasswordEntry.setInputType(mIsAlpha ? currentType
                : (InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD));

//        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry1));
//        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry2));
//        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry3));
//        mPasswordEntryArray.add((TextView)findViewById(R.id.password_entry4));
//        mDeleteButton = (Button)findViewById(R.id.mDeleteButton);
//        mOkButton = (ImageButton) findViewById(R.id.mOkButton);
//        buttonArray.add((Button)findViewById(R.id.mButton0));
//        buttonArray.add((Button)findViewById(R.id.mButton1));
//        buttonArray.add((Button)findViewById(R.id.mButton2));
//        buttonArray.add((Button)findViewById(R.id.mButton3));
//        buttonArray.add((Button)findViewById(R.id.mButton4));
//        buttonArray.add((Button)findViewById(R.id.mButton5));
//        buttonArray.add((Button)findViewById(R.id.mButton6));
//        buttonArray.add((Button)findViewById(R.id.mButton7));
//        buttonArray.add((Button)findViewById(R.id.mButton8));
//        buttonArray.add((Button)findViewById(R.id.mButton9));
//        changeButton();
    }

//    public void changeButton(){
//        for(int i=0;i<10;i++){
//            Button button = buttonArray.get(i);
//            button.setText(String.valueOf(i));
//            final int j = i;
//            button.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (mPasswordConfirmStr.length() < 4) {
//                        mPasswordConfirmStr = mPasswordConfirmStr+j;
//                        updateEditView();
//                    }
//                }
//            });
//        }
//        mDeleteButton.getBackground().setAlpha(0);
////        mOkButton.getBackground().setAlpha(0); // MODIFIED by kai.wang1, 2016-07-22,BUG-2570987
//        mOkButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                deletePassword();
//            }
//        });
//    }

//    private void updateEditView() {
//        if (mPasswordConfirmStr.length() > 0) {
//            for (int n=0;n<mPasswordConfirmStr.length();n++) {
//                TextView textView = mPasswordEntryArray.get(n);
//                textView.setText("*");
//            }
//            if (mPasswordConfirmStr.length() == 4) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        // TODO Auto-generated method stub
//                        handleNext();
//                    }
//                }, 300);
//            }
//        }
//    }

//    private void clearEditView(){
//        for (int n=0;n<4;n++) {
//            TextView textView = mPasswordEntryArray.get(n);
//            mPasswordConfirmStr = "";
//            textView.setText("");
//        }
//    }

    //    private void deletePassword() {
//        int n = mPasswordConfirmStr.length();
//        if (n > 0) {
//            mPasswordEntryArray.get(n-1).setText("");
//            mPasswordConfirmStr = mPasswordConfirmStr.substring(0, n-1);
//        }
//    }
    private FingerprintManager.AuthenticationCallback mAuthenticationCallback = new AuthenticationCallback() {
        @Override
        public void onAuthenticationFailed() {
            Log.d(TAG, "onAuthenticationFailed");
            showToast(PasswordLockActivity.this.getResources().
                    getString(R.string.fingerprint_failed));
        }

        ;

        @Override
        public void onAuthenticationSucceeded(AuthenticationResult result) {
            Log.d(TAG, "onAuthenticationSucceeded");
            mFingerprintCancelSignal = null;
            int fpid = result.getFingerprint().getFingerId();
            int tag = mFpm.tctGetTagForFingerprintId(fpid);

            if (tag == FingerprintManager.FP_TAG_COMMON) {
                if (mPackageName != null) {
                    localPackageManager.setApplicationLockStatus(mPackageName, 2);
                }
                finish();
            } else {
                Log.d(TAG, "onAuthenticationFailed");
                showToast(PasswordLockActivity.this.getResources().
                        getString(R.string.fingerprint_failed));
                authenticateFingerPrint();
            }
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            Log.d(TAG, "onAuthenticationHelp");
        }

        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            Log.d(TAG, "onAuthenticationError");
            mFingerprintCancelSignal = null;
            if (errMsgId == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
                showToast(PasswordLockActivity.this.getResources().
                        getString(R.string.fingerprint_error));
                mFingerprintView.setVisibility(View.INVISIBLE);
                SharedPreferenceUtil.editFingerPrintError(PasswordLockActivity.this, true);
            }
        }

        @Override
        public void onAuthenticationAcquired(int acquireInfo) {
            Log.d(TAG, "onAuthenticationAcquired");
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        //[BUGFIX]-Add-BEGIN by TCTNB.caixia.chen,06/12/2016,2114657
        boolean shouldFinish = ApplicationLockUtils.isApplicationKilled(this, mPackageName);
        if (shouldFinish) {
            finish();
        }
        //[BUGFIX]-Add-END by TCTNB.caixia.chen
        authenticateFingerPrint();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        if (mFingerprintCancelSignal != null && !mFingerprintCancelSignal.isCanceled()) {
            mFingerprintCancelSignal.cancel();
        }
        mFingerprintCancelSignal = null;

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (SystemProperties.getInt("sys.security.power.mode", 2) == 1) {
                Log.e(TAG, "sys.security.power.mode = 1");
                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.tct.securitycenter", "com.tct.securitycenter.batterymanager.LauncherInPowerSaver"));
                startActivityAsUser(intent, UserHandle.CURRENT);
                return false;
            }
            goHome();
            //super.onKeyDown(keyCode, event);
            return false;
        } else if (keyCode == KeyEvent.KEYCODE_MENU) {
            return true;
        }
        return true;
    }

    public void goHome() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        homeIntent.addCategory(Intent.CATEGORY_HOME);
        startActivity(homeIntent);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 500);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub

    }

    public void handleNext() {//处理输入的密码
        final String pin = mPasswordEntry.getText().toString();
        Log.d(TAG, "PasswordLockActivity handleNext: " + pin);
        String systemPatern = Settings.System.getString(getContentResolver(), "normal_password");
        if (pin != null && pin.equals(systemPatern)) {//和系统密码一致
            if (mPackageName != null) {
                localPackageManager.setApplicationLockStatus(mPackageName, 2);
            }
            finish();
            SharedPreferenceUtil.editFingerPrintError(PasswordLockActivity.this, false);
        } else {
            mPasswordEntry.setText(null);
            // clearEditView();
            // mPasswordEntryInputDisabler.setInputEnabled(false);静止输入
            Toast.makeText(this, PasswordLockActivity.this.getResources().
                            getString(R.string.wrong_password_input_again),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void authenticateFingerPrint() {
        if (ApplicationLockUtils.isUnlockWithFingerprintPossible(0, mFpm, this)
                && ApplicationLockUtils.isFingerPrint4ApplicationLockOn(this)
                && !SharedPreferenceUtil.readFingerPrintError(this)) {
            if (mFingerprintCancelSignal != null && !mFingerprintCancelSignal.isCanceled()) {
                mFingerprintCancelSignal.cancel();
            }
            mFingerprintView.setVisibility(View.VISIBLE);
            mFingerprintCancelSignal = new CancellationSignal();
            mFpm.authenticate(null, mFingerprintCancelSignal, 0, mAuthenticationCallback, null, 0);
            //setFingerprintRunningState(FINGERPRINT_STATE_RUNNING);
        } else {
            mFingerprintView.setVisibility(View.INVISIBLE);
        }

    }

    private void showToast(String text) {
        if (null != toast) {
            toast.setText(text);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.show();
        } else {
            toast = Toast.makeText(PasswordLockActivity.this, text, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    // {@link OnEditorActionListener} methods.
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        // Check if this was the result of hitting the enter or "done" key
        if (actionId == EditorInfo.IME_NULL
                || actionId == EditorInfo.IME_ACTION_DONE
                || actionId == EditorInfo.IME_ACTION_NEXT) {
            Log.d(TAG, "onEditorAction");
            handleNext();
            return true;
        }
        return false;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {//软件盘自动弹出
        super.onWindowFocusChanged(hasFocus);
        showSoftInputMethod(hasFocus);
    }

    public void showSoftInputMethod(boolean hasFocus) {
        if (!hasFocus) {
            return;
        }
        // Post to let window focus logic to finish to allow soft input show/hide properly.
        mPasswordEntry.post(new Runnable() {
            @Override
            public void run() {
                // if (shouldAutoShowSoftKeyboard()) { 判断是否使用软件盘还是指纹锁，后续完成
                mImm.showSoftInput(mPasswordEntry, InputMethodManager.SHOW_IMPLICIT);

            }
        });
    }

}
