/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.settingslib.wifi;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.wifi.WifiConfiguration;
import android.os.Looper;
import android.os.UserHandle;
import mst.preference.Preference;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

//[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/13,task 3046476
import android.view.View;
import android.widget.ProgressBar;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.widget.ImageView;
import android.util.Log;
//[FEATURE]Add-END TCTNB.jianhong.yang

import com.android.settingslib.R;

public class AccessPointPreference extends Preference {

    private static final int[] STATE_SECURED = {
            R.attr.state_encrypted
    };
    private static final int[] STATE_NONE = {};

    private static int[] wifi_signal_attributes = { R.attr.wifi_signal };

    private final StateListDrawable mWifiSld;
    private final int mBadgePadding;
    private final UserBadgeCache mBadgeCache;

    private TextView mTitleView;
    private boolean mForSavedNetworks = false;
    private AccessPoint mAccessPoint;
    private Drawable mBadge;
    private int mLevel;
    private CharSequence mContentDescription;

    //[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/13,task 3046476
    public static final String TAG = "AccessPointPreference";
    private final boolean DEBUG = false;
    private boolean isLondonProject;
    private boolean mIsMarkedPayAp = false;
    private final int[] mPayApIcon = {
            R.drawable.icon_wifi__pay_signal_1,
            R.drawable.icon_wifi__pay_signal_2,
            R.drawable.icon_wifi__pay_signal_3,
            R.drawable.icon_wifi__pay_signal_4,
    };
    //[FEATURE]Add-END by TCTNB.jianhong.yang

    static final int[] WIFI_CONNECTION_STRENGTH = {
            R.string.accessibility_wifi_one_bar,
            R.string.accessibility_wifi_two_bars,
            R.string.accessibility_wifi_three_bars,
            R.string.accessibility_wifi_signal_full
    };

    // Used for dummy pref.
    public AccessPointPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mWifiSld = null;
        mBadgePadding = 0;
        mBadgeCache = null;
    }

    public AccessPointPreference(AccessPoint accessPoint, Context context, UserBadgeCache cache,
            boolean forSavedNetworks) {
        super(context);
        mBadgeCache = cache;
        mAccessPoint = accessPoint;

        //[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/13,task 3046476
        isLondonProject = true;//context.getResources().getBoolean(com.android.internal.R.bool.feature_wifi_tctfw_rom_enabled);

        if(DEBUG) {
            Log.d(TAG, "isLondonProject = " + isLondonProject);
        }

        mForSavedNetworks = forSavedNetworks;

        if(isLondonProject) {
            mForSavedNetworks = false;
            mIsMarkedPayAp = forSavedNetworks;
            setLayoutResource(R.layout.preference_access_point);
        }
        //[FEATURE]Mod-END by TCTNB.jianhong.yang

        mAccessPoint.setTag(this);
        mLevel = -1;

        mWifiSld = (StateListDrawable) context.getTheme()
                .obtainStyledAttributes(wifi_signal_attributes).getDrawable(0);

        // Distance from the end of the title at which this AP's user badge should sit.
        mBadgePadding = context.getResources()
                .getDimensionPixelSize(R.dimen.wifi_preference_badge_padding);
        refresh();
    }

    public AccessPoint getAccessPoint() {
        return mAccessPoint;
    }

    @Override
    public void onBindView(final View view) {
        super.onBindView(view);
        if (mAccessPoint == null) {
            // Used for dummy pref.
            return;
        }

        //[FEATURE]Add-BEGIN TCTNB.jianhong.yang,2016/10/13,task 3046476
        if(isLondonProject) {
            ImageView linkIcon = (ImageView) view.findViewById(R.id.link_icon);
            ProgressBar isConnectingAP = (ProgressBar) view.findViewById(R.id.scanning_progress);

            if (mAccessPoint.isConnected()) {
                if(getOrder() == 0) {
                    linkIcon.setImageResource(R.drawable.icon_wlan_link);
                    linkIcon.setVisibility(View.VISIBLE);
                    isConnectingAP.setVisibility(View.GONE);
                }
            } else if(mAccessPoint.isConnecting()) {
                if(getOrder() == 0) {
                    linkIcon.setVisibility(View.GONE);
                    isConnectingAP.setVisibility(View.VISIBLE);
                }
           }

            NetworkInfo info = mAccessPoint.getNetworkInfo();
            if(info == null || info.getState() == State.DISCONNECTED ||
                    info.getState() == State.UNKNOWN ||
                    info.getState() == State.SUSPENDED ) {
                isConnectingAP.setVisibility(View.GONE);
                linkIcon.setVisibility(View.INVISIBLE);
            }
        }
       //[FEATURE]Add-END by TCTNB.jianhong.yang

        Drawable drawable = getIcon();
        if (drawable != null) {
            drawable.setLevel(mLevel);
        }

        mTitleView = (TextView) view.findViewById(com.android.internal.R.id.title);
        if (mTitleView != null) {
            // Attach to the end of the title view
            mTitleView.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, mBadge, null);
            mTitleView.setCompoundDrawablePadding(mBadgePadding);
        }
        view.setContentDescription(mContentDescription);
    }

    protected void updateIcon(int level, Context context) {
        if (level == -1) {
            setIcon(null);
        } else {
            if (getIcon() == null) {
                // To avoid a drawing race condition, we first set the state (SECURE/NONE) and then
                // set the icon (drawable) to that state's drawable.
                // If sld is null then we are indexing and therefore do not have access to
                // (nor need to display) the drawable.
                if (mWifiSld != null) {
                    mWifiSld.setState((mAccessPoint.getSecurity() != AccessPoint.SECURITY_NONE)
                            ? STATE_SECURED
                            : STATE_NONE);
                    Drawable drawable = mWifiSld.getCurrent();
                    //[FEATURE]Mod-BEGIN TCTNB.jianhong.yang,2016/10/13,task 3046476
                    if(isLondonProject) {
                        if(mAccessPoint.isSaved() && mIsMarkedPayAp) {
                             drawable = context.getDrawable(mPayApIcon[level]);
                         }
                         setIcon(drawable);
                    } else {
                        if (!mForSavedNetworks) {
                            setIcon(drawable);
                        } else {
                            setIcon(null);
                        }
                    }
                    //[FEATURE]Mod-END by TCTNB.jianhong.yang
                }
            }
        }
    }

    protected void updateBadge(Context context) {
        WifiConfiguration config = mAccessPoint.getConfig();
        if (config != null) {
            // Fetch badge (may be null)
            // Get the badge using a cache since the PM will ask the UserManager for the list
            // of profiles every time otherwise.
            mBadge = mBadgeCache.getUserBadge(config.creatorUid);
        }
    }

    /**
     * Updates the title and summary; may indirectly call notifyChanged().
     */
    public void refresh() {
        if (mForSavedNetworks) {
            setTitle(mAccessPoint.getConfigName());
        } else {
            setTitle(mAccessPoint.getSsid());
        }

        final Context context = getContext();
        int level = mAccessPoint.getLevel();
        if (level != mLevel) {
            mLevel = level;
            updateIcon(mLevel, context);
            notifyChanged();
        }
        updateBadge(context);

        setSummary(mForSavedNetworks ? mAccessPoint.getSavedNetworkSummary()
                : mAccessPoint.getSettingsSummary());

        mContentDescription = getTitle();
        if (getSummary() != null) {
            mContentDescription = TextUtils.concat(mContentDescription, ",", getSummary());
        }
        if (level >= 0 && level < WIFI_CONNECTION_STRENGTH.length) {
            mContentDescription = TextUtils.concat(mContentDescription, ",",
                    getContext().getString(WIFI_CONNECTION_STRENGTH[level]));
        }
    }

    @Override
    protected void notifyChanged() {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            // Let our BG thread callbacks call setTitle/setSummary.
            postNotifyChanged();
        } else {
            super.notifyChanged();
        }
    }

    public void onLevelChanged() {
        postNotifyChanged();
    }

    private void postNotifyChanged() {
        if (mTitleView != null) {
            mTitleView.post(mNotifyChanged);
        } // Otherwise we haven't been bound yet, and don't need to update.
    }

    private final Runnable mNotifyChanged = new Runnable() {
        @Override
        public void run() {
            notifyChanged();
        }
    };

    public static class UserBadgeCache {
        private final SparseArray<Drawable> mBadges = new SparseArray<>();
        private final PackageManager mPm;

        public UserBadgeCache(PackageManager pm) {
            mPm = pm;
        }

        private Drawable getUserBadge(int userId) {
            int index = mBadges.indexOfKey(userId);
            if (index < 0) {
                Drawable badge = mPm.getUserBadgeForDensity(new UserHandle(userId), 0 /* dpi */);
                mBadges.put(userId, badge);
                return badge;
            }
            return mBadges.valueAt(index);
        }
    }
}
