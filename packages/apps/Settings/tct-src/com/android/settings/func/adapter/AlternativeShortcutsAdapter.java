package com.android.settings.func.adapter;

import java.util.List;

import com.android.settings.R;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log; // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AlternativeShortcutsAdapter extends BaseAdapter {

    private final static String TAG = "DragAdapter";
    private Context context;
    public List<String> listData;
    private FuncAddListener funcAddListener;

    public AlternativeShortcutsAdapter(Context context,
            List<String> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData == null ? 0 : listData.size();
    }

    @Override
    public String getItem(int position) {
        if (listData != null && listData.size() != 0) {
            return listData.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderNormal holderNormal = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.alternativeshortcuts_listview_item, null);
            holderNormal = new ViewHolderNormal();
            holderNormal.funcIcon = (ImageView) convertView
                    .findViewById(R.id.funcicon);
            holderNormal.funcText = (TextView) convertView
                    .findViewById(R.id.functext);
            holderNormal.func_addIcon = (ImageView) convertView
                    .findViewById(R.id.func_add);
            convertView.setTag(holderNormal);
        } else {
            holderNormal = (ViewHolderNormal) convertView.getTag();
        }
        String item = listData.get(position);
        Resources res = context.getResources();
        int id = res.getIdentifier("id/" + item, null, "android");

        if (id != 0) { // MODIFIED by beibei.yang, 2016-10-28,BUG-2830318
            int descId = res.getIdentifier("string/" + item, null, context.getPackageName());
            holderNormal.funcText.setText(descId != 0 ? res.getString(descId) : item);
            int imgId = res.getIdentifier("drawable/" + item, null, context.getPackageName());
            if (imgId != 0) {
                holderNormal.funcIcon.setImageResource(imgId);
            }
        }
        final int clickposition = position;
        holderNormal.func_addIcon.setOnClickListener(view -> {
            if (funcAddListener != null) {
                funcAddListener.addFuncItem(clickposition);
            }
        });
        return convertView;
    }

    public void setFuncAddListener(FuncAddListener funcAddListener) {
        this.funcAddListener = funcAddListener;
    }

    static class ViewHolderNormal {
        ImageView funcIcon;
        TextView funcText;
        ImageView func_addIcon;
    }

    public interface FuncAddListener {
        void addFuncItem(int position);
    }

}
