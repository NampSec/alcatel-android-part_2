/* ----------|----------------------|----------------------|----------------- */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* -------------------------------------------------------------------------- */
/* 9/29/2015 |     rurong.zhang     |        CR 674472     | Going directly   */
/*           |                      |                      | into a specific  */
/*           |                      |                      | function of      */
/*           |                      |                      | an app           */
/*           |                      |                      |                  */
/******************************************************************************/

package com.android.settings.func.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class AlternativeShortcutslistView extends ListView {

    public AlternativeShortcutslistView(Context context) {
        super(context);
    }

    public AlternativeShortcutslistView(Context paramContext,
            AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }

}
