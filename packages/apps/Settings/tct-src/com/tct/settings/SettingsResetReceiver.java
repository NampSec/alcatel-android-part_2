/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:3/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Reference documents : AT&T req doc                                        */
/* -------------------------------------------------------------------------- */
/*  Comments : This file is used to reset related settings menu configures    */
/*  File     : packages/apps/Settings/tct-src/com/tct/settings/settingsResetR */
/*             eceiver.java                                                   */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 3/08/2016 |    rurong.zhang      |FR1716095             |<13340Track><42>  */
/*           |                      |                      |<CDR-MRMC-090>    */
/*           |                      |                      |Result of Invoking*/
/*           |                      |                      |the Device Reset Function*/
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.tct.settings;

import android.app.INotificationManager;
import android.app.Notification;
import android.app.NotificationManager; // MODIFIED by feng.tang, 2016-11-07,BUG-3311763
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.LauncherApps;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources.NotFoundException;
import android.content.res.XmlResourceParser;
import android.net.Uri;
import android.os.Binder;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.security.KeyStore;
import android.service.notification.NotificationListenerService;
import android.util.Log;

import com.android.internal.telephony.SmsApplication;
import com.android.settings.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class SettingsResetReceiver extends BroadcastReceiver {
    private boolean isParserd = false;
    private HashMap<String, setData> data = null;
    private setData curSetData = null;
    private cVariant curVar = null;
    private String curSetName = null;
    private ContentValues mValues;
    private String resultData = null;

    private final String TAG = "settingsConfig";
    private final String RES_PACKAGE_NAME = "com.android.settings";
    static final Uri DATABASE_SYSTEM = Settings.System.CONTENT_URI;
    static final String SHARED_PREFS_SETTINGS = "com.android.settings_preferences";
    static final String SHARED_PREFS_DATA = "data_usage";
    static final String SHARED_PREFS_FIREWALL = "Firewall";

    /* All base data tag type */
    static final int TYPE_UNBASE = 0;
    static final int TYPE_INT = 1;
    static final int TYPE_STRING = 2;
    static final int TYPE_BOOL = 3;
    static final int TYPE_DEFAULT = 4;
    static final int TYPE_DELETE = 5;
    static final int TYPE_CUSTOM = 6;

    private static final Intent APP_NOTIFICATION_PREFS_CATEGORY_INTENT = new Intent(
            Intent.ACTION_MAIN)
            .addCategory(Notification.INTENT_CATEGORY_NOTIFICATION_PREFERENCES);

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        String mDefaultSmsApp = context.getSharedPreferences(
                "FROM_DEVICE_RESET", Context.MODE_PRIVATE).getString(
                "com.jrdcom.settings.defaultsms", "");
        if (!mDefaultSmsApp.equals(""))
            SmsApplication.setDefaultApplication(mDefaultSmsApp, context);
        int count = this.getResultCode() + 1;
        this.setResultCode(count);
        resultData = this.getResultData();

        Log.i(TAG, "Receive " + action);

        XmlResourceParser parser = context.getResources().getXml(
                R.xml.reset_configures);

        try {
            doParse(parser, context);
        } catch (Exception e) {
            Log.i(TAG, "Parse xml exception " + e.toString());
        }

        if (isParserd && (data != null)) {
            Set<String> sets = data.keySet();
            Iterator<String> iterator = sets.iterator();
            while (iterator.hasNext()) {
                String set = iterator.next();
                if (set.equalsIgnoreCase("db_system")) {
                    doConfigDB(context, data.get(set), DATABASE_SYSTEM);
                } else if (set.equalsIgnoreCase("sp_settings")) {
                    doConfigSP(context, data.get(set), SHARED_PREFS_SETTINGS);
                } else if (set.equalsIgnoreCase("sp_data_usage")) {
                    doConfigSP(context, data.get(set), SHARED_PREFS_DATA);
                } else if (set.equalsIgnoreCase("spfull_Firewall")) {
                    SharedPreferences sharePre = context.getSharedPreferences(
                            SHARED_PREFS_FIREWALL, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharePre.edit();
                    editor.clear().commit();
                }
            }
        }

        //KeyStore.getInstance().reset();//2016-12-5 modify by fan.zhou for Defect 3257016
        this.setResultData(resultData);

        final List<ApplicationInfo> appInfos = new ArrayList<ApplicationInfo>();
        LauncherApps launcherApps = (LauncherApps) context
                .getSystemService(Context.LAUNCHER_APPS_SERVICE);
        final List<LauncherActivityInfo> lais = launcherApps.getActivityList(
                null /* all */, UserHandle.getUserHandleForUid(Binder.getCallingUid()));
        for (LauncherActivityInfo lai : lais) {
            appInfos.add(lai.getApplicationInfo());
        }

        final PackageManager pm = context.getPackageManager();
        final List<ResolveInfo> resolvedConfigActivities = pm
                .queryIntentActivities(APP_NOTIFICATION_PREFS_CATEGORY_INTENT,
                        0 // PackageManager.MATCH_DEFAULT_ONLY
                );
        for (ResolveInfo ri : resolvedConfigActivities) {
            appInfos.add(ri.activityInfo.applicationInfo);
        }

        /*final INotificationManager sINM = INotificationManager.Stub
                .asInterface(ServiceManager
                        .getService(Context.NOTIFICATION_SERVICE));
        for (ApplicationInfo info : appInfos) {
            try {
                sINM.setNotificationsEnabledForPackage(info.packageName,
                        info.uid, true);
                sINM.setPackagePriority(info.packageName, info.uid,
                        Notification.PRIORITY_DEFAULT);
                sINM.setPackageVisibilityOverride(
                        info.packageName,
                        info.uid,
                        NotificationListenerService.Ranking.VISIBILITY_NO_OVERRIDE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
    }

    /*
     * Delete files in designated floder
     */
    public void deleteFloder(File fi) {
        if (fi.isFile()) {
            fi.delete();
        } else {
            File fif[] = fi.listFiles();
            int len = fif.length;
            for (int i = 0; i < len; i++) {
                deleteFloder(fif[i]);
            }
            fi.delete();
        }
    }

    /**
     * Database reset configure.
     */
    private void doConfigDB(Context context, setData curData, Uri uri) {
        ContentResolver resolver = context.getContentResolver();
        Iterator<Entry<String, cVariant>> iterator = curData.getAll();
        mValues = new ContentValues();

        Log.i(TAG, "" + "start doConfigDB()...");

        while (iterator.hasNext()) {
            Map.Entry<String, cVariant> entry = iterator.next();
            String key = entry.getKey();
            cVariant var = entry.getValue();
            Object obj = var.getObj();
            int type = var.getType();

            Log.i(TAG,
                    "type = " + type + ", key = " + key + ", value = "
                            + obj.toString());

            if ((type == TYPE_DEFAULT) || (type == TYPE_INT)
                    || (type == TYPE_BOOL) || (type == TYPE_STRING)) {
                if ((obj != null) && (!key.equals(""))) {
                    mValues.put(Settings.NameValueTable.NAME, key);
                    mValues.put(Settings.NameValueTable.VALUE, obj.toString());
                    resolver.insert(uri, mValues);
                    mValues.clear();
                }
            } else if (type == TYPE_DELETE) {
                if ((obj == null) || (obj.toString().equals(""))) {
                    resolver.delete(uri, Settings.NameValueTable.NAME + " = ?",
                            new String[] { key });
                } else {
                    resolver.delete(uri, Settings.NameValueTable.NAME
                            + " like ?", new String[] { obj.toString() });
                }
            } else if (type == TYPE_CUSTOM) {
                if (key.equalsIgnoreCase("wallpaper")) {
                    WallpaperManager wpm = (WallpaperManager) context
                            .getSystemService(Context.WALLPAPER_SERVICE);
                    try {
                        wpm.clear();
                    } catch (IOException e) {
                        Log.i(TAG, "Reset wallpaper exception: " + e.toString());
                    }
                } else if (key.equalsIgnoreCase("language")) {
                    resultData = resultData + "/language";
                } else if (key.equalsIgnoreCase("bluetooth")) {
                    resultData = resultData + "/bluetooth";
                } else if (key.equalsIgnoreCase("wifi_hotspot_powermode")) {
                    String stringValue = "1";
                    if ((obj != null) && (!obj.toString().equals(""))) {
                        stringValue = obj.toString();
                    }
                    SystemProperties
                            .set("persist.sys.apsleepmode", stringValue);
                }else if (key.equalsIgnoreCase("inputmethod_language")) {
                    Settings.Secure.putString(resolver,
                            Settings.Secure.DEFAULT_INPUT_METHOD,
                            "com.android.inputmethod.latin/.LatinIME");

                    StringBuilder inputMethodStr = new StringBuilder();
                    inputMethodStr.append(
                            "com.android.inputmethod.latin/.LatinIME").append(
                            ":");
                    inputMethodStr
                            .append("com.google.android.inputmethod.latin/com.android.inputmethod.latin.LatinIME")
                            .append(":");
                    inputMethodStr
                            .append("com.google.android.googlequicksearchbox/com.google.android.voicesearch.ime.VoiceInputMethodService");
                    Settings.Secure.putString(resolver,
                            Settings.Secure.ENABLED_INPUT_METHODS,
                            inputMethodStr.toString());
                    SharedPreferences prefs = PreferenceManager
                            .getDefaultSharedPreferences(context);
                    prefs.edit().remove("previously_enabled_subtypes").apply();
                /* MODIFIED-BEGIN by feng.tang, 2016-11-07,BUG-3311763*/
                } else if (key.equalsIgnoreCase("zen_mode")) {
                    NotificationManager notificationManager = context.getSystemService(
                             NotificationManager.class);
                    notificationManager.setZenMode(Settings.Global.ZEN_MODE_OFF, null, TAG);
                    /* MODIFIED-END by feng.tang,BUG-3311763*/
                }
            } else {
                Log.i(TAG, "The type of " + key + " is not supported.");
            }
        }
    }

    /**
     * SharePreference reset configure.
     */
    private void doConfigSP(Context context, setData curData, String sp) {
        SharedPreferences sharePre = context.getSharedPreferences(sp,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePre.edit();
        Iterator<Entry<String, cVariant>> iterator = curData.getAll();

        Log.i(TAG, "" + "start doConfigSP()...");

        while (iterator.hasNext()) {
            Map.Entry<String, cVariant> entry = iterator.next();
            String key = entry.getKey();
            cVariant var = entry.getValue();
            Object obj = var.getObj();
            int type = var.getType();

            Log.i(TAG,
                    "type = " + type + ", key = " + key + ", value = "
                            + obj.toString());

            switch (type) {
            case TYPE_INT:
                if ((obj != null) && (!key.equals(""))) {
                    editor.putInt(key, new Integer(obj.toString())).commit();
                }
                break;
            case TYPE_BOOL:
                if ((obj != null) && (!key.equals(""))) {
                    editor.putBoolean(key, new Boolean(obj.toString()))
                            .commit();
                }
                break;
            case TYPE_STRING:
                if ((obj != null) && (!key.equals(""))) {
                    editor.putString(key, obj.toString()).commit();
                }
                break;
            case TYPE_DELETE:
                editor.remove(key).commit();
                break;
            case TYPE_CUSTOM:
                break;
            }
        }
    }

    /**
     * parse the hole xml file.
     */
    private synchronized void doParse(XmlPullParser parser, Context context)
            throws XmlPullParserException, IOException {
        if (!isParserd) {
            data = new HashMap<String, setData>();
            int eventType = parser.getEventType();
            while (true) {
                switch (eventType) {
                case XmlPullParser.END_DOCUMENT:
                    isParserd = true;
                    return;
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    processStartTag(parser, context);
                    break;
                case XmlPullParser.TEXT:
                    break;
                case XmlPullParser.END_TAG:
                    String name = parser.getName();
                    if (name.equalsIgnoreCase("set")) {
                        curSetData = null;
                        curSetName = null;
                    } else if (getBaseType(name) != TYPE_UNBASE) {
                        curVar = null;
                    }
                    break;
                default:
                    break;
                }
                eventType = parser.next();
            }
        }
    }

    /**
     * process the start tag.
     */
    private void processStartTag(XmlPullParser parser, Context context)
            throws XmlPullParserException, IOException {
        String name = parser.getName();
        int type = getBaseType(name);
        if (name.equalsIgnoreCase("set")) {
            curSetData = new setData();
            curSetName = parser.getAttributeValue(0).trim();
            if (!curSetName.equals("")) {
                data.put(curSetName, curSetData);
            }
        } else if (type != TYPE_UNBASE) {
            String curBaseName = parser.getAttributeValue(0).trim();
            String tmpText = parser.nextText().trim();

            if ((type == TYPE_DEFAULT) || (type == TYPE_DELETE)
                    || (type == TYPE_CUSTOM)) {
                curVar = new cVariant(tmpText, type);
            } else if (!tmpText.equals("")) {
                int id = 0;
                tmpText = tmpText.trim();
                try {
                    if (type == TYPE_INT) {
                        // Not use reflect as will cause NoSuchFieldException
                        // with user sw version
                        // fld = R.integer.class.getDeclaredField(tmpText);
                        // id = fld.getInt(tmpText);
                        id = context.getResources().getIdentifier(tmpText,
                                "integer", RES_PACKAGE_NAME);
                        int value = context.getResources().getInteger(id);
                        Log.i(TAG, "value = " + value);
                        curVar = new cVariant(value, type);
                    } else if (type == TYPE_BOOL) {
                        id = context.getResources().getIdentifier(tmpText,
                                "bool", RES_PACKAGE_NAME);
                        boolean value = context.getResources().getBoolean(id);
                        Log.i(TAG, "value = " + value);
                        curVar = new cVariant(value ? 1 : 0, type);
                    } else if (type == TYPE_STRING) {
                        id = context.getResources().getIdentifier(tmpText,
                                "string", RES_PACKAGE_NAME);
                        String value = context.getResources().getString(id);
                        Log.i(TAG, "value = " + value);
                        curVar = new cVariant(value, type);
                    }
                    Log.i(TAG,
                            "type = " + name + ", id = "
                                    + Integer.toHexString(id));
                } catch (IllegalArgumentException e) {
                    Log.i(TAG, e.toString());
                } catch (NotFoundException e) {
                    Log.i(TAG, e.toString());
                }
            }
            if ((!curBaseName.equals("")) && (curVar != null)) {
                curSetData.put(curBaseName, curVar);
                curVar = null;
            }
        }
    }

    /**
     * find out the current tag is base tag. only support int, bool, string,
     * default, delete, custom.
     */
    private int getBaseType(String name) {
        int ret = TYPE_UNBASE;
        if ((name.equalsIgnoreCase("int"))
                || (name.equalsIgnoreCase("integer"))) {
            ret = TYPE_INT;
        } else if (name.equalsIgnoreCase("bool")) {
            ret = TYPE_BOOL;
        } else if (name.equalsIgnoreCase("string")) {
            ret = TYPE_STRING;
        } else if (name.equalsIgnoreCase("default")) {
            ret = TYPE_DEFAULT;
        } else if (name.equalsIgnoreCase("delete")) {
            ret = TYPE_DELETE;
        } else if (name.equalsIgnoreCase("custom")) {
            ret = TYPE_CUSTOM;
        }
        return ret;
    }

    /**
     * Module data, it restore the all data parsed from xml file.
     */
    private class setData {
        private HashMap<String, cVariant> map = new HashMap<String, cVariant>();

        public cVariant get(String key) {
            cVariant v = null;
            if (map.containsKey(key)) {
                v = map.get(key);
            }
            return v;
        }

        void put(String key, cVariant o) {
            if (key != null && key.length() != 0) {
                map.put(key, o);
            }
        }

        public boolean containsKey(String key) {
            return map.containsKey(key);
        }

        public Iterator<Entry<String, cVariant>> getAll() {
            return map.entrySet().iterator();
        }
    }

    /**
     * all data parsed from xml file will packaged to Variant.
     */
    private class cVariant {
        Object obj = null;
        int type = TYPE_UNBASE;

        public cVariant(Object value, int type) {
            if (value != null) {
                this.obj = value;
                this.type = type;
            }
        }

        public int getType() {
            return type;
        }

        public Object getObj() {
            return obj;
        }
    }
}
