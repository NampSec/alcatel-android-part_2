/* Copyright (C) 2016 Tcl Corporation Limited */
/* ----------|----------------------|----------------------|----------------- */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* -------------------------------------------------------------------------- */
/* 9/29/2015 |     rurong.zhang     |        CR 674472     | Going directly   */
/*           |                      |                      | into a specific  */
/*           |                      |                      | function of      */
/*           |                      |                      | an app           */
/*           |                      |                      |                  */
/******************************************************************************/

package com.android.settings.deviceinfo.boomkey.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class BoomAlternativeShortcutslistView extends ListView {

    public BoomAlternativeShortcutslistView(Context context) {
        super(context);
    }

    public BoomAlternativeShortcutslistView(Context paramContext,
            AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }

}
