/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.deviceinfo;



import android.content.Context;
import android.support.v7.preference.CheckBoxPreference;
import android.util.AttributeSet;
import com.android.settings.R;

public class MyRadioButtonPreference extends CheckBoxPreference{

    public interface OnClickListener {
        public abstract void onRadioButtonClicked(MyRadioButtonPreference emiter);
    }

    private OnClickListener mListener = null;
    public MyRadioButtonPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, R.style.MyRadioButtonPreference);
        setWidgetLayoutResource(R.layout.preference_widget_radiobutton);
        // TODO Auto-generated constructor stub
    }
    public MyRadioButtonPreference(Context context, AttributeSet attrs) {
        super(context, attrs, 0, R.style.MyRadioButtonPreference);
        setWidgetLayoutResource(R.layout.preference_widget_radiobutton);

    }

    //[FEATURE]-Mod-BEGIN by TSNJ,beibei.yang,10/14/2015,Task-499635
    public void setOnClickListener(OnClickListener listener) {
        mListener = listener;
    }
    //[FEATURE]-Mod-END by TSNJ,beibei.yang,
    @Override
    public void onClick() {
        if (mListener != null) {
            mListener.onRadioButtonClicked(this);
        }
    }
}
