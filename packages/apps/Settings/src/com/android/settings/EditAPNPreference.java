/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.util.Log;
/**
 * TODO: Add a soft dialpad for PIN entry.
 */
class EditAPNPreference extends CustomEditTextPreference {

    private String inputType;
    public EditAPNPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.EditAPNPreference);

        inputType = array.getString(R.styleable.EditAPNPreference_inputtype);
        array.recycle();
    }

    public EditAPNPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.EditAPNPreference);

        inputType = array.getString(R.styleable.EditAPNPreference_inputtype);
        array.recycle();
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        final EditText editText = (EditText) view.findViewById(android.R.id.edit);

        if (editText != null) {
            if("text".equals(inputType)){
                editText.setInputType(InputType.TYPE_CLASS_TEXT );
            } else if("textEmailAddress".equals(inputType)){
                editText.setInputType(InputType.TYPE_CLASS_TEXT |InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            } else if("textUri".equals(inputType)){
                editText.setInputType(InputType.TYPE_CLASS_TEXT |InputType.TYPE_TEXT_VARIATION_URI);
            } else if("number".equals(inputType)){
                editText.setInputType(InputType.TYPE_CLASS_NUMBER );
            } else if("textPassword".equals(inputType)){
                editText.setInputType(InputType.TYPE_CLASS_TEXT |InputType.TYPE_TEXT_VARIATION_PASSWORD);
            } else if("textNoSuggestions".equals(inputType)){
                editText.setInputType(InputType.TYPE_CLASS_TEXT |InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
            } else {
                editText.setInputType(InputType.TYPE_CLASS_TEXT );
            }
        }
    }

    public boolean isDialogOpen() {
        Dialog dialog = getDialog();
        return dialog != null && dialog.isShowing();
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
    }

}
