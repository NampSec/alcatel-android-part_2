/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Index;
import com.android.settings.search.Indexable;
import android.provider.SearchIndexableResource;
import com.android.settings.search.SearchIndexableRaw;
import android.content.res.Resources;

import java.util.List;
import java.util.ArrayList;
public class SettingsRegulatory extends RestrictedSettingsFragment implements Indexable{
    static final String TAG = "SettingsRegulatory";
    public SettingsRegulatory(String restrictionKey) {
        super(restrictionKey);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
    }

    @Override
    protected int getMetricsCategory() {
        // TODO Auto-generated method stub
        return 0;
    }
    //[BUGFIX]-Add-BEGIN by TSNJ.heng.zhang1,2016-10-27,Defect-3134469
    public static final BaseSearchIndexProvider SEARCH_INDEX_DATA_PROVIDER =
        new BaseSearchIndexProvider() {
            @Override
            public List<SearchIndexableRaw> getRawDataToIndex(Context context, boolean enabled) {
                final List<SearchIndexableRaw> result = new ArrayList<SearchIndexableRaw>();
                SearchIndexableRaw data = new SearchIndexableRaw(context);

                data.title = context.getString(R.string.regulatory_safety);
                data.screenTitle = context.getString(R.string.regulatory_safety);
                result.add(data);
                return result;
            }
        };
    //[BUGFIX]-Add-END by TSNJ.heng.zhang1,2016-10-27,Defect-3134469
}
