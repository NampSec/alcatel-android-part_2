/*
 * Copyright (c) 2013 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 *
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 */

/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.wifi;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiEnterpriseConfig;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.android.settings.R;

import android.text.TextUtils;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import com.android.settingslib.wifi.AccessPoint;

/**
 * This class is the main controller for CMCC. Now it's responsible for the
 * following things: 1) Show the PresetNetworkInfo Activity to represent the
 * preset networks.
 */
public class SSIDPreloadService extends Service {

    private static final String TAG = "SSIDPreloadService";
    private static final boolean DEBUG = true;

    private static final int EVENT_CHECK_AND_SAVE_PRESET_NETWORK = 0;
    private static final int EVENT_REMOVE_PRELOADED_SSID = 1;
    private static final String SHARE_PREFERENCE_FILE_NAME = "wifi_ssid_preload";
    private static final String SHARE_PREFERENCE_PRELOAD_FLAG_KEY = "wifi_ssid_preload_flag";
    private static final String SHARE_PREFERENCE_SSV_INDEX_KEY = "wifi_ssid_ssv_index";

    public static final String PSK = "PSK";
    public static final String WEP = "WEP";
    public static final String EAP = "EAP";
    public static final String OPEN = "OPEN";
    private TelephonyManager mTelephonyManager;

    private Handler mHandler = new SSIDPreloadEventHandler();
    private boolean mIsSSVEnabled;
    private WifiManager mWifiManager;

    private WifiManager.ActionListener mSaveListener = new WifiManager.ActionListener() {
        public void onSuccess() {
            if (DEBUG) Log.e(TAG, "Save network successfully");
            setSSIDPreloadStatus(true);
        }

        public void onFailure(int reason) {
            setSSIDPreloadStatus(false);
            Log.e(TAG, "Failed to save network, reason:" + reason);
        }
    };

    private WifiManager.ActionListener mForgetListener = new WifiManager.ActionListener() {
        @Override
        public void onSuccess() {
            setSSIDPreloadStatus(false);
            if (DEBUG) Log.e(TAG, "Forget network successfully");
        }
        @Override
        public void onFailure(int reason) {
            Log.e(TAG, "Failed to forget network, reason:" + reason);
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        mIsSSVEnabled = getResources().getBoolean(R.bool.def_Settings_wifi_preloadSsid_ssvEnable);
        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (DEBUG) Log.d(TAG,"SSIDPreloadService onStartCommand mIsSSVEnabled = " + mIsSSVEnabled);
        if (mIsSSVEnabled) {
            mHandler.sendEmptyMessage(EVENT_REMOVE_PRELOADED_SSID);
        } else {
            mHandler.sendEmptyMessage(EVENT_CHECK_AND_SAVE_PRESET_NETWORK);
        }
        return super.onStartCommand(intent, flags, startId);
    }


    private class SSIDPreloadEventHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case EVENT_CHECK_AND_SAVE_PRESET_NETWORK:
                    try {
                        if (DEBUG) Log.d(TAG,"checkAndSavePresetNetwork");
                        preloadSSIDNetwork(getApplicationContext());
                        stopSelf();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case EVENT_REMOVE_PRELOADED_SSID:
                    if (DEBUG) Log.d(TAG,"removePreloadedSSID");
                    removePreloadedSSID(getApplicationContext());
                    sendEmptyMessage(EVENT_CHECK_AND_SAVE_PRESET_NETWORK);
                    break;
                default:
                    break;
            }
        }
    }


    private void preloadSSIDNetwork(Context context) {

        if (getResources().getBoolean(R.bool.def_Settings_wifi_preloadSsid_enable) == false) {
            if (DEBUG) Log.d(TAG, "Preload wifi ssid not enabled");
            return;
        }

        int maxPreloadNumber = getResources().getInteger(R.integer.def_Settings_wifi_ssid_number);

        //Add for SSV mechanism
        //Now we support preload only one SSID per MCCMNC
        int preloadIndex = 0;
        if (mIsSSVEnabled) {
            int index = getPreloadSSVIndex();
            if (index >= 0 && index < maxPreloadNumber) {
                preloadIndex = index;              //Locate the SSID that need to preload
                maxPreloadNumber = index + 1;      //Only preload one SSID
                if (DEBUG) Log.d(TAG, "Preload wifi ssid SSV: preloadIndex = " + preloadIndex + " maxPreloadNumber = " + maxPreloadNumber);
            } else {
                if (DEBUG) Log.d(TAG, "Preload wifi ssid SSV: index error or SSID number error !");
                return;
            }
        }

        for (; preloadIndex < maxPreloadNumber; preloadIndex++ ) {
            WifiConfiguration config = new WifiConfiguration();

            config.SSID = AccessPoint.convertToQuotedString(getStringFromArray(getResources().getString(R.string.def_Settings_wifi_ssidNameList), preloadIndex));

            if (DEBUG) Log.i(TAG, "Preload SSID : " + config.SSID);
            if (AccessPoint.convertToQuotedString(config.SSID).trim().length() == 0) continue;

            int priority = getIntegerFromArray(getResources().getString(R.string.def_Settings_wifi_priorityList), preloadIndex);
            if (priority > 0) {
                config.priority = priority;
                if (DEBUG) Log.i(TAG,"Preload priority : " + config.priority);
            }

            String password = getStringFromArray(getResources().getString(R.string.def_Settings_wifi_passwordList),preloadIndex);
            String security = getStringFromArray(getResources().getString(R.string.def_Settings_wifi_securityModeList),preloadIndex);

            if (!TextUtils.isEmpty(security) && (security.equals(PSK))) {
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                if (!TextUtils.isEmpty(password)) {
                    if (password.matches("[0-9A-Fa-f]{64}")) {
                        config.preSharedKey = password;
                    } else {
                        config.preSharedKey = '"' + password + '"';
                    }
                }
                if (DEBUG) Log.i(TAG, "Preload PSK SSID, config.preSharedKey : " + config.preSharedKey);
            } else if (!TextUtils.isEmpty(security) && (security.equals(WEP))) {
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                if (!TextUtils.isEmpty(password)) {
                    int length = password.length();
                    if ((length == 10 || length == 26 || length == 58) && password.matches("[0-9A-Fa-f]*")) {
                       config.wepKeys[0] = password;
                    } else {
                        config.wepKeys[0] = '"' + password + '"';
                    }
                }
                if (DEBUG) Log.i(TAG, "Preload WEP SSID, config.wepKeys[0] : " + config.wepKeys[0]);
            } else if (!TextUtils.isEmpty(security) && (security.equals(EAP))) {
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.IEEE8021X);
                config.enterpriseConfig = new WifiEnterpriseConfig();
                if (password.length() != 0) {
                    config.enterpriseConfig.setPassword(password);
                }
                String mIdentity = getStringFromArray(getResources().getString(R.string.def_Settings_wifi_eap_identityList),preloadIndex);
                int mEapMethod = getIntegerFromArray(getResources().getString(R.string.def_Settings_wifi_eap_methodList),preloadIndex);
                int mPhase2 = getIntegerFromArray(getResources().getString(R.string.def_Settings_wifi_eap_phrase2_authenticationList),preloadIndex);
                if (mPhase2 < 0) { //Invalid value
                    mPhase2 = 0;
                }
                String mCaCert = getStringFromArray(getResources().getString(R.string.def_Settings_wifi_eap_ca_certificationList),preloadIndex);
                String mUserCert = getStringFromArray(getResources().getString(R.string.def_Settings_wifi_eap_user_certificateList),preloadIndex);
                String mAnonymous = getStringFromArray(getResources().getString(R.string.def_Settings_wifi_eap_anonymous_identityList),preloadIndex);

                int selectedSimCardNumber = getIntegerFromArray(getResources().getString(R.string.def_Settings_wifi_selectedSimCardNumberList),preloadIndex);

                config.enterpriseConfig.setEapMethod(mEapMethod);
                config.enterpriseConfig.setPhase2Method(mPhase2);
                config.enterpriseConfig.setIdentity(mIdentity);
                config.enterpriseConfig.setCaCertificateAlias(mCaCert);
                config.enterpriseConfig.setClientCertificateAlias(mUserCert);
                config.enterpriseConfig.setAnonymousIdentity(mAnonymous);

                if ("SIM".equals(mEapMethod) || "AKA".equals(mEapMethod)) {
                    if (mTelephonyManager.isMultiSimEnabled()
                            && selectedSimCardNumber > 0 && selectedSimCardNumber <= 2) {
                        //config.SIMNum = selectedSimCardNumber;
                    }
                }
                //[BUGFIX]-ADD-BEGIN by TCTSH.yuguan.chen,2015/01/19,Defect1276162
                if(mEapMethod == WifiEnterpriseConfig.Eap.SIM) {
                    if(mTelephonyManager.isMultiSimEnabled()) {
                        boolean isSim1Ready = mTelephonyManager.getSimState(0) == TelephonyManager.SIM_STATE_READY;
                        boolean isSim2Ready = mTelephonyManager.getSimState(1) == TelephonyManager.SIM_STATE_READY;
                        boolean isMccmncConfigured = getResources().getString(R.string.def_Settings_wifi_mccmncList) != "";
                        int[] subId1 = SubscriptionManager.getSubId(0);
                        int[] subId2 = SubscriptionManager.getSubId(1);
                        String icc1 = null;
                        String icc2 = null;
                        if(subId1 != null && subId1.length != 0) {
                            icc1 = mTelephonyManager.getSimOperatorNumeric(subId1[0]);//[BUGFIX]jianhong.yang,2016/09/28,task 3005505
                        }
                        if(subId2 != null && subId2.length != 0) {
                            icc2 = mTelephonyManager.getSimOperatorNumeric(subId2[0]);//[BUGFIX]jianhong.yang,2016/09/28,task 3005505
                        }
                        String mccmncs = getStringFromArray(getResources().getString(R.string.def_Settings_wifi_mccmncList),preloadIndex);
                        if(isSim1Ready) {
                            if(!isMccmncConfigured) {
                                config.SIMNum = 1;
                            } else {
                                if(icc1 != null && icc1 != "" && mccmncs.contains(icc1)) {
                                    config.SIMNum = 1;
                                } else if(isSim2Ready && icc2 != null  && icc2 != "" && mccmncs.contains(icc2)){
                                    config.SIMNum = 2;
                                }
                            }
                        } else if(isSim2Ready) {
                            config.SIMNum = 2;
                        }
                        if (DEBUG) Log.i(TAG, "EAP SIM SSID: " + config.SSID + " SIMNum: " + config.SIMNum + " mccmncs: " + mccmncs
                                + " SIM Slot1: " + isSim1Ready + ", " + icc1 + " SIM Slot2: " + isSim2Ready + ", " + icc2);
                    }
                }
                //[BUGFIX]-ADD-END by TCTSH.yuguan.chen,2015/01/19,Defect1276162
                if (DEBUG) Log.i(TAG, "Preload EAP SSID, password : " + password + " mIdentity : " + mIdentity + " mEapMethod : " + mEapMethod
                                   + " mPhase2 : " + mPhase2 + " mCaCert : " + mCaCert + " mUserCert" + mUserCert + " mAnonymous " + mAnonymous + " SIM card : " + selectedSimCardNumber);
            } else {
                if (DEBUG) Log.i(TAG, "Preload Opened SSID");
                config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            }

            if (getIntegerFromArray(getResources().getString(R.string.def_Settings_wifi_broadcast_statusList),preloadIndex) == 0) {
                config.hiddenSSID = true;
            } else {
                config.hiddenSSID = false;
            }
            if (DEBUG) Log.i(TAG, "Preload SSID is hidden :" + config.hiddenSSID);
            mWifiManager.save(config, mSaveListener);
        }
        mWifiManager.startScan();
    }

    //User have changed the SIM card, we should forget the preloaded SSID
    private void removePreloadedSSID(Context context) {
        List<WifiConfiguration> configs = mWifiManager.getConfiguredNetworks();
        String ssidArray = getResources().getString(R.string.def_Settings_wifi_ssidNameList);
        for (WifiConfiguration config : configs) {
            String configSSID = AccessPoint.removeDoubleQuotes(config.SSID);
            if (ssidArray.contains(configSSID)) {
                int index = getStringIndexFromArray(ssidArray,configSSID);
                if (index != -1 ) {
                    String security = getStringFromArray(getResources().getString(R.string.def_Settings_wifi_securityModeList),index);
                    int keyManagement = config.getAuthType();
                    if ((keyManagement == KeyMgmt.WPA_PSK || keyManagement == KeyMgmt.WPA2_PSK)
                            && !TextUtils.isEmpty(security) && (security.equals(PSK))) {
                        mWifiManager.forget(config.networkId, mForgetListener);
                    } else if ((keyManagement == KeyMgmt.WPA_EAP || keyManagement == KeyMgmt.IEEE8021X)
                            && !TextUtils.isEmpty(security) && (security.equals(EAP))) {
                        mWifiManager.forget(config.networkId, mForgetListener);
                    } else if (keyManagement == KeyMgmt.NONE && (!TextUtils.isEmpty(security)
                            && (security.equals(WEP)) || !TextUtils.isEmpty(security) && (security.equals(OPEN)))) {
                        mWifiManager.forget(config.networkId, mForgetListener);
                    }
                }
            }
        }
        mWifiManager.saveConfiguration();
    }

    private static int getStringIndexFromArray(String arrayString, String string) {
        if (string == null || arrayString == null || string.length() == 0 || arrayString.length() == 0) {
            return -1;
        }
        String[] array = arrayString.split(";");
        for (int i = 0 ; i < array.length ; i++) {
            if (array[i].equals(string)) {
                return i;
            }
        }
        return -1;
    }

    private static String getStringFromArray(String arrayString , int num) {
        if (arrayString == null) {
            return "";
        }
        String[] array = arrayString.split(";");
        if (num >= array.length) {
            return "";
        } else {
            return array[num].trim();
        }
    }

    private int getIntegerFromArray(String arrayString , int num) {
        if (arrayString == null) {
            return -1;
        }
        String[] array = arrayString.split(";");
        if (num >= array.length) {
            return -1;
        } else {
            try {
                //[BUGFIX]MOD-BEGIN TCTNB.jianhong.yang@tcl.com 2015/12/28,defect 1198850
                String parseNum = array[num].trim();
                if (parseNum.startsWith("0x") || parseNum.startsWith("0X")) {
                    return Integer.parseInt(parseNum.substring(2), 16);
                }
                return Integer.parseInt(parseNum);
                //[BUGFIX]MOD-END TCTNB.jianhong.yang
            } catch (NumberFormatException e) {
                if (DEBUG) Log.i(TAG, "Preload SSID NumberFormatException");
                return -1;
            }
        }
    }

    private void setSSIDPreloadStatus(boolean isPreloaded) {
        SharedPreferences sharedPreferences = getSharedPreferences(
                SHARE_PREFERENCE_FILE_NAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SHARE_PREFERENCE_PRELOAD_FLAG_KEY, isPreloaded);
        editor.commit();
    }

    private boolean isSSIDPreloaded() {
        SharedPreferences sharedPreferences = getSharedPreferences(
                SHARE_PREFERENCE_FILE_NAME,
                Activity.MODE_PRIVATE);
        return sharedPreferences.getBoolean(SHARE_PREFERENCE_PRELOAD_FLAG_KEY, false);
    }

    private int getPreloadSSVIndex() {
        SharedPreferences sharedPreferences = getSharedPreferences(
                SHARE_PREFERENCE_FILE_NAME,
                Activity.MODE_PRIVATE);
        return sharedPreferences.getInt(SHARE_PREFERENCE_SSV_INDEX_KEY, -1);
    }

    static boolean isCarrierAp(AccessPoint accessPoint, Context context) {
        if (accessPoint == null) {
            return false;
        }
        if (context.getResources().getBoolean(R.bool.def_Settings_wifi_preloadSsid_allowDelete)) {
            return false;
        }

        String ssidArray = context.getResources().getString(R.string.def_Settings_wifi_ssidNameList);
        int index = getStringIndexFromArray(ssidArray,accessPoint.getSsidStr());
        if (index >= 0) {
            String securityList = context.getResources().getString(R.string.def_Settings_wifi_securityModeList);
            String security = getStringFromArray(securityList,index);
            int keyManagement = accessPoint.getConfig().getAuthType();
            if ((keyManagement == KeyMgmt.WPA_PSK || keyManagement == KeyMgmt.WPA2_PSK)
                    && !TextUtils.isEmpty(security) && (security.equals(PSK))) {
                return true;
            } else if ((keyManagement == KeyMgmt.WPA_EAP || keyManagement == KeyMgmt.IEEE8021X)
                    && !TextUtils.isEmpty(security) && (security.equals(EAP))) {
                return true;
            } else if (keyManagement == KeyMgmt.NONE && (!TextUtils.isEmpty(security)
                    && (security.equals(WEP)) || !TextUtils.isEmpty(security) && (security.equals(OPEN)))) {
                return true;
            }
            return false;
        }
        return false;
    }
}
