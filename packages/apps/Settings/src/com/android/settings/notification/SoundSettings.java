/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.notification;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;  //[SOLUTION]-ADD-By TCTNB.wen.zhuang, 08/16/2016, SOLUTION-2541835
import android.os.UserHandle;
import android.os.UserManager;
import android.os.Vibrator;
import android.support.v7.preference.PreferenceCategory; // MODIFIED by xiang.miao-nb, 2016-10-18,BUG-3136359
import android.preference.SeekBarVolumizer;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.SearchIndexableResource;
import android.provider.Settings;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;
import android.support.v7.preference.TwoStatePreference;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.DefaultRingtonePreference;
import com.android.settings.R;
import com.android.settings.RingtonePreference;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.Utils;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.search.Indexable;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.RestrictedPreference;

//[SOLUTION]-Add-BEGIN by TCTNB.wen.zhuang, 08/18/2016, SOLUTION-2541835
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
//[SOLUTION]-Add-END by TCTNB.wen.zhuang

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemProperties;
import android.preference.CheckBoxPreference;
import android.text.TextUtils;

import static com.android.settingslib.RestrictedLockUtils.EnforcedAdmin;

//[FEATURE]-Add-BEGIN by TCTNB.Bo.Yu,2016/10/27, task 3234021
import com.arkamys.audio.ArkamysAudioAPI;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.content.Intent;
import android.os.RemoteException;
import android.support.v7.preference.PreferenceGroup;
import android.support.v14.preference.SwitchPreference;
//[FEATURE]-Add-END by TCTNB.Bo.Yu,2016/10/27, task 3234021

public class SoundSettings extends SettingsPreferenceFragment implements Indexable {
    private static final String TAG = "SoundSettings";

    //[BUGFIX]-Mod-BEGIN by TCTNB.yubin.ying,09/06/2016,Solution-2521300,
    private static final String KEY_SILENT_MODE = "silent_mode";
    private TwoStatePreference mSilentMode;
    //[BUGFIX]-Mod-END by TCTNB.yubin.ying
    private static final String KEY_MEDIA_VOLUME = "media_volume";
    private static final String KEY_ALARM_VOLUME = "alarm_volume";
    private static final String KEY_RING_VOLUME = "ring_volume";
    private static final String KEY_NOTIFICATION_VOLUME = "notification_volume";
    private static final String KEY_PHONE_RINGTONE = "ringtone";
    private static final String KEY_NOTIFICATION_RINGTONE = "notification_ringtone";
    private static final String KEY_ALARM_RINGTONE = "alarm_ringtone";
    private static final String KEY_VIBRATE_WHEN_RINGING = "vibrate_when_ringing";
    private static final String KEY_WIFI_DISPLAY = "wifi_display";
    private static final String KEY_ZEN_MODE = "zen_mode";
    private static final String KEY_CELL_BROADCAST_SETTINGS = "cell_broadcast_settings";

    private static final String SELECTED_PREFERENCE_KEY = "selected_preference";
    private static final int REQUEST_CODE = 200;

    private static final String KEY_POWER_RINGTONE_ENABLED = "power_ringtone_silence";  //[SOLUTION]-ADD-By TCTNB.wen.zhuang, 08/18/2016, SOLUTION-2541835

    //[FEATURE]-Add by TCTNB.Bo.Yu,2016/10/27, task 3234021
    private static final String KEY_ARKAMYS = "arkamys_key";
    private static final String KEY_ARKAMYS_EFFECT_SWITCH = "arkamys_switch";

    private static final String[] RESTRICTED_KEYS = {
        KEY_MEDIA_VOLUME,
        KEY_ALARM_VOLUME,
        KEY_RING_VOLUME,
        KEY_NOTIFICATION_VOLUME,
        KEY_ZEN_MODE,
    };

    private static final int SAMPLE_CUTOFF = 2000;  // manually cap sample playback at 2 seconds

    private final VolumePreferenceCallback mVolumeCallback = new VolumePreferenceCallback();
    private final H mHandler = new H();
    private final SettingsObserver mSettingsObserver = new SettingsObserver();
    private final Receiver mReceiver = new Receiver();
    private final ArrayList<VolumeSeekBarPreference> mVolumePrefs = new ArrayList<>();

    private Context mContext;
    private boolean mVoiceCapable;
    private Vibrator mVibrator;
    private AudioManager mAudioManager;
    private VolumeSeekBarPreference mRingOrNotificationPreference;
    /* MODIFY-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
    private DefaultRingtonePreference mPhoneRingtonePreference;
    /* MODIFY-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
    private Preference mNotificationRingtonePreference;
    private Preference mAlarmRingtonePreference;
    private TwoStatePreference mVibrateWhenRinging;
    private ComponentName mSuppressor;
    private int mRingerMode = -1;

    private PackageManager mPm;
    private UserManager mUserManager;
    private RingtonePreference mRequestPreference;

    /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
    private static final String KEY_PHONE_RINGTONE1 = "ringtone1";
    private static final String KEY_PHONE_RINGTONE2 = "ringtone2";
    private static final String KEY_VIBRATE_WHEN_RINGING1 = "vibrate_when_ringing1";
    private static final String KEY_VIBRATE_WHEN_RINGING2 = "vibrate_when_ringing2";
    private DefaultRingtonePreference mSim1PhoneRingtonePreference;
    private DefaultRingtonePreference mSim2PhoneRingtonePreference;
    private TwoStatePreference mVibrateWhenRinging1;
    private TwoStatePreference mVibrateWhenRinging2;
    private RingtoneManager mRingtoneManager;
    /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
    //ADD-BEGIN by Dingyi  2016/08/18 SOLUTION 2521714
    private static final String KEY_HEADSET_MODE = "headset_mode";
    private TwoStatePreference mHeadsetMode;
    //ADD-END by Dingyi  2016/08/18 SOLUTION 2521714
    /* MODIFIED-BEGIN by xiang.miao-nb, 2016-10-18,BUG-3136359*/
    private static final String KEY_SOUNDS_CATEGORY = "sounds_category";
    private PreferenceCategory mSoundsCategory;
    /* MODIFIED-END by xiang.miao-nb,BUG-3136359*/

    private TwoStatePreference mPowerRingtoneSilence;  //[SOLUTION]-ADD-By TCTNB.wen.zhuang, 08/18/2016, SOLUTION-2541835

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2759309
//[ALWE][6070]Create notification sound menu
    //defect 1563553
    private boolean mSeperateShowRingNotion = false;
    private VolumeSeekBarPreference mRingPreference;
    private VolumeSeekBarPreference mNotificationPreference;

	//task 2521342
    private boolean mUpdateVolumeIcon = false;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.SOUND;
    }

    //ADD-BEGIN by Dingyi  2016/08/18 SOLUTION 2521714
    private final BroadcastReceiver mRingerModeChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AudioManager.RINGER_MODE_CHANGED_ACTION)) {
                UpdateState();
            }
        }
    };

    private void UpdateState() {
        if (getActivity() == null) return;
        int mSoundMode = SystemProperties.getInt("ro.headset.sound.mode", 0);
        String mPersistSysHeadsetMode = SystemProperties.get("persist.sys.headset_on");
        if (0 != mSoundMode) {
            if (TextUtils.isEmpty(mPersistSysHeadsetMode)) {
                if (1 == mSoundMode) {
                    mHeadsetMode.setChecked(true);
                    SystemProperties.set("persist.sys.headset_on", "true");
                } else if (2 == mSoundMode) {
                    mHeadsetMode.setChecked(false);
                    SystemProperties.set("persist.sys.headset_on", "false");
                }
            } else {
               mHeadsetMode.setChecked("true".equals(mPersistSysHeadsetMode));
           }
        }
    }
    //ADD-END by Dingyi  2016/08/18 SOLUTION 2521714

    //[FEATURE]-Add-BEGIN by TCTNB.Bo.Yu,2016/10/27, task 3234021
    private SwitchPreference mArkamysSwitchPreference;
    private static ArkamysAudioAPI arkamysApi;
    private AdditionServiceConnection connection;
    class AdditionServiceConnection implements ServiceConnection {
    @Override
    public void onServiceConnected(ComponentName name, IBinder service){
        Log.i(TAG, "ArkamysAudio connected");
        arkamysApi = ArkamysAudioAPI.Stub.asInterface((IBinder)service);
    }
    @Override
    public void onServiceDisconnected(ComponentName name){
        Log.i(TAG, "ArkamysAudio disconnected");
        arkamysApi = null;
    }
    }
    //[FEATURE]-Add-END by TCTNB.Bo.Yu,2016/10/27, task 3234021



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mPm = getPackageManager();
        mUserManager = UserManager.get(getContext());
        mVoiceCapable = Utils.isVoiceCapable(mContext);
        mRingtoneManager = new RingtoneManager(mContext);/* ADD by Dingyi for dualsim 2016/08/03 FR 2655692*/

        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        mVibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if (mVibrator != null && !mVibrator.hasVibrator()) {
            mVibrator = null;
        }

        addPreferencesFromResource(R.xml.sound_settings);

        initVolumePreference(KEY_MEDIA_VOLUME, AudioManager.STREAM_MUSIC,
                com.android.internal.R.drawable.ic_audio_media_mute);
        initVolumePreference(KEY_ALARM_VOLUME, AudioManager.STREAM_ALARM,
                com.android.internal.R.drawable.ic_audio_alarm_mute);

        //[BUGFIX]-Mod-BEGIN by TCTNB.yubin.ying,09/06/2016,Solution-2521300,
        initSilentMode();
        //[BUGFIX]-Mod-END by TCTNB.yubin.ying
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2759309
//[ALWE][6070]Create notification sound menu:defect 1563553
        mSeperateShowRingNotion = mContext.getResources().getBoolean(com.android.internal.R.bool.def_feature_seperateShowRingNoti_on);

        mUpdateVolumeIcon = mContext.getResources().getBoolean(com.android.internal.R.bool.def_feature_update_volume_icon);
        if (!mSeperateShowRingNotion) {
            if (mVoiceCapable) {
                if (mUpdateVolumeIcon) {
                    mRingOrNotificationPreference = initVolumePreference(KEY_RING_VOLUME, AudioManager.STREAM_RING,
                                     R.drawable.ic_ring_volume_off);
                } else {
                    mRingOrNotificationPreference = initVolumePreference(KEY_RING_VOLUME, AudioManager.STREAM_RING,
                                    com.android.internal.R.drawable.ic_audio_ring_notif_mute);
                }
                removePreference(KEY_NOTIFICATION_VOLUME);
            } else {
                mRingOrNotificationPreference = initVolumePreference(KEY_NOTIFICATION_VOLUME, AudioManager.STREAM_NOTIFICATION,
                                com.android.internal.R.drawable.ic_audio_ring_notif_mute);
                removePreference(KEY_RING_VOLUME);
            }
        } else {
            if (mUpdateVolumeIcon) {
                mRingPreference = initVolumePreference(KEY_RING_VOLUME, AudioManager.STREAM_RING,
                                R.drawable.ic_ring_volume_off);
                mNotificationPreference = initVolumePreference(KEY_NOTIFICATION_VOLUME, AudioManager.STREAM_NOTIFICATION,
                                com.android.internal.R.drawable.ic_audio_ring_notif_mute);
            } else {
                mRingPreference = initVolumePreference(KEY_RING_VOLUME, AudioManager.STREAM_RING,
                                com.android.internal.R.drawable.ic_audio_ring_notif_mute);
                mNotificationPreference = initVolumePreference(KEY_NOTIFICATION_VOLUME, AudioManager.STREAM_NOTIFICATION,
                                com.android.internal.R.drawable.ic_audio_notification_mute);
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        // Enable link to CMAS app settings depending on the value in config.xml.
        boolean isCellBroadcastAppLinkEnabled = this.getResources().getBoolean(
                com.android.internal.R.bool.config_cellBroadcastAppLinks);
        try {
            if (isCellBroadcastAppLinkEnabled) {
                if (mPm.getApplicationEnabledSetting("com.android.cellbroadcastreceiver")
                        == PackageManager.COMPONENT_ENABLED_STATE_DISABLED) {
                    isCellBroadcastAppLinkEnabled = false;  // CMAS app disabled
                }
            }
        } catch (IllegalArgumentException ignored) {
            isCellBroadcastAppLinkEnabled = false;  // CMAS app not installed
        }
        if (!mUserManager.isAdminUser() || !isCellBroadcastAppLinkEnabled ||
                RestrictedLockUtils.hasBaseUserRestriction(mContext,
                        UserManager.DISALLOW_CONFIG_CELL_BROADCASTS, UserHandle.myUserId())) {
            removePreference(KEY_CELL_BROADCAST_SETTINGS);
        }
        mSoundsCategory = (PreferenceCategory) findPreference(KEY_SOUNDS_CATEGORY); // MODIFIED by xiang.miao-nb, 2016-10-18,BUG-3136359

        //[FEATURE]-Add-BEGIN by TCTNB.Bo.Yu,2016/10/27, task 3234021
        final PreferenceGroup arkamys = (PreferenceGroup)findPreference(KEY_ARKAMYS);
        initArkamys(arkamys);
        //[FEATURE]-Add-END by TCTNB.Bo.Yu,2016/10/27, task 3234021
        initRingtones();
        initVibrateWhenRinging();
        //ADD-BEGIN by Dingyi  2016/08/18 SOLUTION 2521714
        initHeadSetMode();
        //ADD-END by Dingyi  2016/08/18 SOLUTION 2521714
        initPowerRingtoneSilence();  //[SOLUTION]-ADD-By TCTNB.wen.zhuang, 08/18/2016, SOLUTION-2541835
        updateRingerMode();
        updateEffectsSuppressor();

        if (savedInstanceState != null) {
            String selectedPreference = savedInstanceState.getString(SELECTED_PREFERENCE_KEY, null);
            if (!TextUtils.isEmpty(selectedPreference)) {
                mRequestPreference = (RingtonePreference) findPreference(selectedPreference);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        lookupRingtoneNames();
        mSettingsObserver.register(true);
        mReceiver.register(true);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2759309
//[ALWE][6070]Create notification sound menu:defect 1563553
        //updateRingOrNotificationPreference();
        if (mSeperateShowRingNotion) {
            if (mUpdateVolumeIcon) {
               ringAndNotificationPreferenceUpdate();
            } else {
               updateRingAndNotificationPreference();
            }
        } else {
            if (mUpdateVolumeIcon) {
                ringOrNotificationPreferenceUpdate();
            } else {
                updateRingOrNotificationPreference();
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        updateEffectsSuppressor();
        for (VolumeSeekBarPreference volumePref : mVolumePrefs) {
            volumePref.onActivityResume();
        }

        final EnforcedAdmin admin = RestrictedLockUtils.checkIfRestrictionEnforced(mContext,
                UserManager.DISALLOW_ADJUST_VOLUME, UserHandle.myUserId());
        final boolean hasBaseRestriction = RestrictedLockUtils.hasBaseUserRestriction(mContext,
                UserManager.DISALLOW_ADJUST_VOLUME, UserHandle.myUserId());
        for (String key : RESTRICTED_KEYS) {
            Preference pref = findPreference(key);
            if (pref != null) {
                pref.setEnabled(!hasBaseRestriction);
            }
            if (pref instanceof RestrictedPreference && !hasBaseRestriction) {
                ((RestrictedPreference) pref).setDisabledByAdmin(admin);
            }
        }
        RestrictedPreference broadcastSettingsPref = (RestrictedPreference) findPreference(
                KEY_CELL_BROADCAST_SETTINGS);
        if (broadcastSettingsPref != null) {
            broadcastSettingsPref.checkRestrictionAndSetDisabled(
                    UserManager.DISALLOW_CONFIG_CELL_BROADCASTS);
        }
        //ADD-BEGIN by Dingyi  2016/08/18 SOLUTION 2521714
        UpdateState();
        IntentFilter filter = new IntentFilter(Intent.ACTION_DOCK_EVENT);
        filter.addAction(AudioManager.RINGER_MODE_CHANGED_ACTION);
        getActivity().registerReceiver(mRingerModeChangedReceiver, filter);
        //ADD-END by Dingyi  2016/08/18 SOLUTION 2521714
    }

    @Override
    public void onPause() {
        super.onPause();
        for (VolumeSeekBarPreference volumePref : mVolumePrefs) {
            volumePref.onActivityPause();
        }
        mVolumeCallback.stopSample();
        mSettingsObserver.register(false);
        mReceiver.register(false);
        //ADD-BEGIN by Dingyi  2016/08/18 SOLUTION 2521714
        getActivity().unregisterReceiver(mRingerModeChangedReceiver);
        //ADD-END by Dingyi  2016/08/18 SOLUTION 2521714
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        if (preference instanceof RingtonePreference) {
            mRequestPreference = (RingtonePreference) preference;
            mRequestPreference.onPrepareRingtonePickerIntent(mRequestPreference.getIntent());
            startActivityForResult(preference.getIntent(), REQUEST_CODE);
            return true;
        } else if (preference == findPreference(KEY_CELL_BROADCAST_SETTINGS)) {
            final Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setComponent(new ComponentName(
                 "com.android.cellbroadcastreceiver",
                 "com.android.cellbroadcastreceiver.CellBroadcastSettings"));

            if (mContext.getPackageManager()
                        .queryIntentActivities(intent, 0).isEmpty())  {
                Log.d(TAG, "Activity com.android.cellbroadcastreceiver" +
                                ".CellBroadcastSettings does not exist");
                return false;
            }
            startActivity(intent);
            return true;
        }

        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mRequestPreference != null) {
            mRequestPreference.onActivityResult(requestCode, resultCode, data);
            mRequestPreference = null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mRequestPreference != null) {
            outState.putString(SELECTED_PREFERENCE_KEY, mRequestPreference.getKey());
        }
    }

    // === Volumes ===

    private VolumeSeekBarPreference initVolumePreference(String key, int stream, int muteIcon) {
        final VolumeSeekBarPreference volumePref = (VolumeSeekBarPreference) findPreference(key);
        volumePref.setCallback(mVolumeCallback);
        volumePref.setStream(stream);
        mVolumePrefs.add(volumePref);
        volumePref.setMuteIcon(muteIcon);
        return volumePref;
    }

    private void updateRingOrNotificationPreference() {
        mRingOrNotificationPreference.showIcon(mSuppressor != null
                ? com.android.internal.R.drawable.ic_audio_ring_notif_mute
                : mRingerMode == AudioManager.RINGER_MODE_VIBRATE || wasRingerModeVibrate()
                ? com.android.internal.R.drawable.ic_audio_ring_notif_vibrate
                : com.android.internal.R.drawable.ic_audio_ring_notif);
    }

    private boolean wasRingerModeVibrate() {
        return mVibrator != null && mRingerMode == AudioManager.RINGER_MODE_SILENT
                && mAudioManager.getLastAudibleStreamVolume(AudioManager.STREAM_RING) == 0;
    }

    private void updateRingerMode() {
        final int ringerMode = mAudioManager.getRingerModeInternal();
        if (mRingerMode == ringerMode) return;
        mRingerMode = ringerMode;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2759309
//[ALWE][6070]Create notification sound menu
        //updateRingOrNotificationPreference();
        if (mSeperateShowRingNotion) {
            if (mUpdateVolumeIcon) {
                 ringAndNotificationPreferenceUpdate();
            } else {
                 updateRingAndNotificationPreference();
            }
        } else {
            if (mUpdateVolumeIcon) {
                 ringOrNotificationPreferenceUpdate();
            } else {
                 updateRingOrNotificationPreference();
            }
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        //[BUGFIX]-Mod-BEGIN by TCTNB.yubin.ying,09/06/2016,Solution-2521300,
        if(mSilentMode != null) {
            mSilentMode.setChecked(mRingerMode == AudioManager.RINGER_MODE_SILENT);
        }
        //[BUGFIX]-Mod-END by TCTNB.yubin.ying
        /* MODIFIED-BEGIN by minglang.ji, 2016-11-07,BUG-3358678*/
        if (mSeperateShowRingNotion) {
            if (mNotificationPreference != null) {
                if (mRingerMode == AudioManager.RINGER_MODE_SILENT || mRingerMode == AudioManager.RINGER_MODE_VIBRATE) {
                    mNotificationPreference.setEnabled(false);
                } else {
                    mNotificationPreference.setEnabled(true);
                }
            }
        }
        /* MODIFIED-END by minglang.ji,BUG-3358678*/
    }

    private void updateEffectsSuppressor() {
        final ComponentName suppressor = NotificationManager.from(mContext).getEffectsSuppressor();
        if (Objects.equals(suppressor, mSuppressor)) return;
        mSuppressor = suppressor;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2759309
//[ALWE][6070]Create notification sound menu
        //if (mRingOrNotificationPreference != null) {
        //    final String text = suppressor != null ?
        //            mContext.getString(com.android.internal.R.string.muted_by,
        //                    getSuppressorCaption(suppressor)) : null;
        //    mRingOrNotificationPreference.setSuppressionText(text);
        //}
        //updateRingOrNotificationPreference();
        //[FEATURE] mod by haifeng.zhai for defect 1563553 2016-03-03 begin
        if (!mSeperateShowRingNotion) {
            if (mRingOrNotificationPreference != null) {
                final String text = suppressor != null ?
                        mContext.getString(com.android.internal.R.string.muted_by,
                                getSuppressorCaption(suppressor)) : null;
                mRingOrNotificationPreference.setSuppressionText(text);
            }
            if (mUpdateVolumeIcon) {
                ringOrNotificationPreferenceUpdate();
            } else {
                updateRingOrNotificationPreference();
            }
        } else {
            if (mNotificationPreference != null) {
                final String text = suppressor != null ?
                        mContext.getString(com.android.internal.R.string.muted_by,
                                getSuppressorCaption(suppressor)) : null;
                mNotificationPreference.setSuppressionText(text);
            }
            if (mRingPreference != null) {
                final String text = suppressor != null ?
                        mContext.getString(com.android.internal.R.string.muted_by,
                                getSuppressorCaption(suppressor)) : null;
                mRingPreference.setSuppressionText(text);
            }
            if (mUpdateVolumeIcon) {
                ringAndNotificationPreferenceUpdate();
            } else {
                updateRingAndNotificationPreference();
            }
        }
        //[FEATURE] mod by haifeng.zhai for defect 1563553 2016-03-03 end
    }

    private String getSuppressorCaption(ComponentName suppressor) {
        final PackageManager pm = mContext.getPackageManager();
        try {
            final ServiceInfo info = pm.getServiceInfo(suppressor, 0);
            if (info != null) {
                final CharSequence seq = info.loadLabel(pm);
                if (seq != null) {
                    final String str = seq.toString().trim();
                    if (str.length() > 0) {
                        return str;
                    }
                }
            }
        } catch (Throwable e) {
            Log.w(TAG, "Error loading suppressor caption", e);
        }
        return suppressor.getPackageName();
    }

    private final class VolumePreferenceCallback implements VolumeSeekBarPreference.Callback {
        private SeekBarVolumizer mCurrent;

        @Override
        public void onSampleStarting(SeekBarVolumizer sbv) {
            if (mCurrent != null && mCurrent != sbv) {
                mCurrent.stopSample();
            }
            mCurrent = sbv;
            if (mCurrent != null) {
                mHandler.removeMessages(H.STOP_SAMPLE);
                mHandler.sendEmptyMessageDelayed(H.STOP_SAMPLE, SAMPLE_CUTOFF);
            }
        }

        @Override
        public void onStreamValueChanged(int stream, int progress) {
            // noop
        }

        public void stopSample() {
            if (mCurrent != null) {
                mCurrent.stopSample();
            }
        }
    };

    //[FEATURE]-Add-BEGIN by TCTNB.Bo.Yu,2016/10/27, task 3234021
    // === Arkamys ===
    private void initArkamys(PreferenceGroup parent) {
        connection = new AdditionServiceConnection();
        Intent i = new Intent();
        i.setClassName("com.arkamys.audio","com.arkamys.audio.ArkamysAudioService");
        boolean ret = getActivity().bindService(i,connection,Context.BIND_AUTO_CREATE);
        mArkamysSwitchPreference = (SwitchPreference)parent.findPreference(KEY_ARKAMYS_EFFECT_SWITCH);
        mArkamysSwitchPreference.setChecked(Settings.System.getInt(getContentResolver(),
                "arkamys_audio_effect_enable", 1) != 0);
        mArkamysSwitchPreference.setOnPreferenceChangeListener(
        new OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
        final boolean val = (Boolean)newValue;
        Log.i(TAG, "Arkamys Effect activated " + val);
        mArkamysSwitchPreference.setTitle(val ? mContext.getString(R.string.audio_arkamys_activated_summary):mContext.getString(R.string.audio_arkamys_deactivated_summary));
        try {
            arkamysApi.setEffectEnable(val);
            Settings.System.putInt(getContentResolver(),
                        "arkamys_audio_effect_enable",
                        val ? 1 : 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
        }
        });
    }
    //[FEATURE]-Add-END by TCTNB.Bo.Yu,2016/10/27, task 3234021

    // === Phone & notification ringtone ===

    private void initRingtones() {
        /* MODIFY-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        mPhoneRingtonePreference = (DefaultRingtonePreference)getPreferenceScreen().findPreference(KEY_PHONE_RINGTONE);
        mSim1PhoneRingtonePreference = (DefaultRingtonePreference)getPreferenceScreen().findPreference(KEY_PHONE_RINGTONE1);
        mSim2PhoneRingtonePreference = (DefaultRingtonePreference)getPreferenceScreen().findPreference(KEY_PHONE_RINGTONE2);
        /* MODIFY-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
        if (mPhoneRingtonePreference != null && !mVoiceCapable) {
            mSoundsCategory.removePreference(mPhoneRingtonePreference); // MODIFIED by xiang.miao-nb, 2016-10-18,BUG-3136359
            mPhoneRingtonePreference = null;
        }
        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        if (TelephonyManager.getDefault().isMultiSimEnabled()) {
            int numPhones = TelephonyManager.getDefault().getPhoneCount();
            for (int i = 0; i < numPhones; i++) {
                int subId = i;
                if (subId == 0) {
                    mSim1PhoneRingtonePreference.setSubId(subId);
                } else if (subId > 0) {
                    mSim2PhoneRingtonePreference.setSubId(subId);
                }
            }

            //From the code, it is slotid, not subid.
            List<SubscriptionInfo> mSubscriptionInfos = SubscriptionManager.from(mContext).getActiveSubscriptionInfoList();
            if (numPhones > 1 && mSubscriptionInfos != null && mSubscriptionInfos.size() == 1) {
                mPhoneRingtonePreference.setSubId(mSubscriptionInfos.get(0).getSimSlotIndex());
            }
        }

        if (isTwoSimReady()) {
            /* MODIFIED-BEGIN by xiang.miao-nb, 2016-10-18,BUG-3136359*/
            mSoundsCategory.removePreference(mPhoneRingtonePreference);
            mPhoneRingtonePreference = null;
        } else {
            mSoundsCategory.removePreference(mSim1PhoneRingtonePreference);
            mSoundsCategory.removePreference(mSim2PhoneRingtonePreference);
            /* MODIFIED-END by xiang.miao-nb,BUG-3136359*/
            mSim1PhoneRingtonePreference = null;
            mSim2PhoneRingtonePreference = null;
        }
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
        mNotificationRingtonePreference =
                getPreferenceScreen().findPreference(KEY_NOTIFICATION_RINGTONE);
        mAlarmRingtonePreference = getPreferenceScreen().findPreference(KEY_ALARM_RINGTONE);
    }

    /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
    private boolean isTwoSimReady() {
        int mPhoneCount = TelephonyManager.getDefault().getPhoneCount();
        boolean hasIccCard = false;
        int count = 0;
        for (int i = 0; i < mPhoneCount; i++) {
            hasIccCard = TelephonyManager.getDefault().hasIccCard(i);
            if (hasIccCard) {
                count ++;
            }
        }
        if(count != 2) {
            return false;
        }
        return true;
    }
    /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

    //ADD-BEGIN by Dingyi  2016/08/18 SOLUTION 2521714
    private void initHeadSetMode() {
        mHeadsetMode = (TwoStatePreference) getPreferenceScreen().findPreference(KEY_HEADSET_MODE);
        int mSoundMode = SystemProperties.getInt("ro.headset.sound.mode", 0);
        if (0 == mSoundMode) {
            getPreferenceScreen().removePreference(mHeadsetMode);
        }
        mHeadsetMode.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final boolean val = (boolean) newValue;
                SystemProperties.set("persist.sys.headset_on", val ? "true" : "false");
                return true;
            }
        });
    }
    //ADD-END by Dingyi  2016/08/18 SOLUTION 2521714

    private void lookupRingtoneNames() {
        AsyncTask.execute(mLookupRingtoneNames);
    }

    private final Runnable mLookupRingtoneNames = new Runnable() {
        @Override
        public void run() {
            if (mPhoneRingtonePreference != null) {
                /* MODIFY-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
//                final CharSequence summary = updateRingtoneName(
//                        mContext, RingtoneManager.TYPE_RINGTONE);
//                if (summary != null) {
//                    mHandler.obtainMessage(H.UPDATE_PHONE_RINGTONE, summary).sendToTarget();
//                }
                int slotId = -1;
                boolean isDualSim = TelephonyManager.getDefault().isMultiSimEnabled() && TelephonyManager.getDefault().getPhoneCount() > 1;
                List<SubscriptionInfo> mSubscriptionInfos = SubscriptionManager.from(mContext).getActiveSubscriptionInfoList();
                if (isDualSim && mSubscriptionInfos != null && mSubscriptionInfos.size() == 1) {
                    slotId = mSubscriptionInfos.get(0).getSimSlotIndex();
                    mPhoneRingtonePreference.setSubId(slotId);
                } else if (mPhoneRingtonePreference.getSubId() > 0) {
                    mPhoneRingtonePreference.setSubId(0);
                }

                CharSequence ringtoneTitle=updateRingtoneName(mContext, RingtoneManager.TYPE_RINGTONE, slotId);
                if(ringtoneTitle.equals(mContext.getString(com.android.internal.R.string.ringtone_unknown))){
                    Uri defaultRingtoneUri=RingtoneManager.getDefaultRingtoneUri(mContext);
                    if (defaultRingtoneUri == null) {
                        ringtoneTitle = "";
                    } else {
                        ringtoneTitle = mRingtoneManager.getRingtoneTitle(defaultRingtoneUri);
                    }
                    RingtoneManager.setActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_RINGTONE, defaultRingtoneUri);
                }
                if (ringtoneTitle != null) {
                    mHandler.obtainMessage(H.UPDATE_PHONE_RINGTONE, ringtoneTitle).sendToTarget();
                }
                /* MODIFY-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
            }
            /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
            if (mSim1PhoneRingtonePreference != null) {
                final CharSequence summary = updateRingtoneName(
                        mContext, RingtoneManager.TYPE_RINGTONE, mSim1PhoneRingtonePreference.getSubId());
                if (summary != null) {
                    mHandler.obtainMessage(H.UPDATE_SIM1_RINGTONE, mSim1PhoneRingtonePreference.getSubId(), 0, summary).sendToTarget();
                }
            }
            if (mSim2PhoneRingtonePreference != null) {
                final CharSequence summary = updateRingtoneName(
                        mContext, RingtoneManager.TYPE_RINGTONE, mSim2PhoneRingtonePreference.getSubId());
                if (summary != null) {
                    mHandler.obtainMessage(H.UPDATE_SIM2_RINGTONE, mSim2PhoneRingtonePreference.getSubId(), 0, summary).sendToTarget();
                }
            }
            /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
            if (mNotificationRingtonePreference != null) {
                final CharSequence summary = updateRingtoneName(
                        mContext, RingtoneManager.TYPE_NOTIFICATION, -1);/* MODIFY by Dingyi for dualsim 2016/08/03 FR 2655692*/
                if (summary != null) {
                    mHandler.obtainMessage(H.UPDATE_NOTIFICATION_RINGTONE, summary).sendToTarget();
                }
            }
            if (mAlarmRingtonePreference != null) {
                final CharSequence summary =
                        updateRingtoneName(mContext, RingtoneManager.TYPE_ALARM, -1);/* MODIFY by Dingyi for dualsim 2016/08/03 FR 2655692*/
                if (summary != null) {
                    mHandler.obtainMessage(H.UPDATE_ALARM_RINGTONE, summary).sendToTarget();
                }
            }
        }
    };

    private static CharSequence updateRingtoneName(Context context, int type, int subId) {/* MODIFY by Dingyi for dualsim 2016/08/03 FR 2655692*/
        if (context == null) {
            Log.e(TAG, "Unable to update ringtone name, no context provided");
            return null;
        }
        /* MODIFY-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        Uri ringtoneUri = null;
        if (type == RingtoneManager.TYPE_RINGTONE && subId != -1) {
            ringtoneUri = RingtoneManager.getActualRingtoneUriBySubId(context, subId);
        } else {
            ringtoneUri = RingtoneManager.getActualDefaultRingtoneUri(context, type);
        }
        /* MODIFY-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
        CharSequence summary = context.getString(com.android.internal.R.string.ringtone_unknown);
        // Is it a silent ringtone?
        if (ringtoneUri == null) {
            summary = context.getString(com.android.internal.R.string.ringtone_silent);
        } else {
            Cursor cursor = null;
            try {
                if (MediaStore.AUTHORITY.equals(ringtoneUri.getAuthority())) {
                    // Fetch the ringtone title from the media provider
                    cursor = context.getContentResolver().query(ringtoneUri,
                            new String[] { MediaStore.Audio.Media.TITLE }, null, null, null);
                } else if (ContentResolver.SCHEME_CONTENT.equals(ringtoneUri.getScheme())) {
                    cursor = context.getContentResolver().query(ringtoneUri,
                            new String[] { OpenableColumns.DISPLAY_NAME }, null, null, null);
                }
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        summary = cursor.getString(0);
                    }
                }
            } catch (SQLiteException sqle) {
                // Unknown title for the ringtone
            } catch (IllegalArgumentException iae) {
                // Some other error retrieving the column from the provider
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return summary;
    }

    // === Vibrate when ringing ===

    private void initVibrateWhenRinging() {
        mVibrateWhenRinging =
                (TwoStatePreference) getPreferenceScreen().findPreference(KEY_VIBRATE_WHEN_RINGING);
        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        mVibrateWhenRinging1 = (TwoStatePreference) getPreferenceScreen().findPreference(KEY_VIBRATE_WHEN_RINGING1);
        mVibrateWhenRinging2 = (TwoStatePreference) getPreferenceScreen().findPreference(KEY_VIBRATE_WHEN_RINGING2);
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
        if (mVibrateWhenRinging == null) {
            Log.i(TAG, "Preference not found: " + KEY_VIBRATE_WHEN_RINGING);
            return;
        }
        if (!mVoiceCapable) {
            mSoundsCategory.removePreference(mVibrateWhenRinging); // MODIFIED by xiang.miao-nb, 2016-10-18,BUG-3136359
            mVibrateWhenRinging = null;
            return;
        }
        mVibrateWhenRinging.setPersistent(false);
        updateVibrateWhenRinging();
        mVibrateWhenRinging.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final boolean val = (Boolean) newValue;
                return Settings.System.putInt(getContentResolver(),
                        Settings.System.VIBRATE_WHEN_RINGING,
                        val ? 1 : 0);
            }
        });
        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        mVibrateWhenRinging1.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final boolean val = (Boolean) newValue;
                return Settings.System.putInt(getContentResolver(),
                        Settings.System.VIBRATE_WHEN_RINGING,
                        val ? 1 : 0);
            }
        });
        mVibrateWhenRinging2.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final boolean val = (Boolean) newValue;
                return Settings.System.putInt(getContentResolver(),
                        Settings.System.VIBRATE_WHEN_RINGING2,
                        val ? 1 : 0);
            }
        });
        if (isTwoSimReady()) {
            /* MODIFIED-BEGIN by xiang.miao-nb, 2016-10-18,BUG-3136359*/
            mSoundsCategory.removePreference(mVibrateWhenRinging);
            mVibrateWhenRinging = null;
        }else {
            mSoundsCategory.removePreference(mVibrateWhenRinging1);
            mVibrateWhenRinging1 = null;
            mSoundsCategory.removePreference(mVibrateWhenRinging2);
            /* MODIFIED-END by xiang.miao-nb,BUG-3136359*/
            mVibrateWhenRinging2 = null;
        }
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
    }

    private void updateVibrateWhenRinging() {
        if (mVibrateWhenRinging == null) return;
        mVibrateWhenRinging.setChecked(Settings.System.getInt(getContentResolver(),
                Settings.System.VIBRATE_WHEN_RINGING, 0) != 0);
        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        if (mVibrateWhenRinging1 == null) return;
        mVibrateWhenRinging1.setChecked(Settings.System.getInt(getContentResolver(),
                Settings.System.VIBRATE_WHEN_RINGING, 0) != 0);
        if (mVibrateWhenRinging2 == null) return;
        mVibrateWhenRinging2.setChecked(Settings.System.getInt(getContentResolver(),
                Settings.System.VIBRATE_WHEN_RINGING2, 0) != 0);
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
    }

    //[SOLUTION]-Add-BEGIN by TCTNB.wen.zhuang, 08/18/2016, SOLUTION-2541835
    // === PowerRingtoneSilence ===
    private void initPowerRingtoneSilence() {
        Log.i("zhuang","initPowerRingtoneSilence...");
        mPowerRingtoneSilence = (TwoStatePreference) getPreferenceScreen().findPreference(KEY_POWER_RINGTONE_ENABLED);
        if (mPowerRingtoneSilence == null) {
            Log.i(TAG, "Preference not found: " + KEY_POWER_RINGTONE_ENABLED);
            return;
        }
        mPowerRingtoneSilence.setChecked(Settings.System.getInt(getContentResolver(),
                Settings.System.TCT_POWER_RINGTONE_ENABLE, 1) != 1);
        mPowerRingtoneSilence.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final boolean val = (Boolean) newValue;
                setBootValue("boot_ringtone_enable", val ? "0" : "1");
                return Settings.System.putInt(getContentResolver(),
                        Settings.System.TCT_POWER_RINGTONE_ENABLE,
                        val ? 0 : 1);
            }
        });
    }

    private boolean setBootValue(String name , String value){
        File boot_value= new File("/cache/boot_value/");
        if(!boot_value.exists() && !boot_value.isDirectory()){
            boot_value.mkdir();
        }
        try {
            FileWriter fileWriter = new FileWriter("/cache/boot_value/"+name);
            BufferedWriter buffuerWriter = new BufferedWriter(fileWriter);
            buffuerWriter.write(value);
            buffuerWriter.flush();
            buffuerWriter.close();
            fileWriter.close();
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    //[SOLUTION]-Add-END by TCTNB.wen.zhuang

    // === Callbacks ===

    private final class SettingsObserver extends ContentObserver {
        private final Uri VIBRATE_WHEN_RINGING_URI =
                Settings.System.getUriFor(Settings.System.VIBRATE_WHEN_RINGING);

        public SettingsObserver() {
            super(mHandler);
        }

        public void register(boolean register) {
            final ContentResolver cr = getContentResolver();
            if (register) {
                cr.registerContentObserver(VIBRATE_WHEN_RINGING_URI, false, this);
            } else {
                cr.unregisterContentObserver(this);
            }
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);
            if (VIBRATE_WHEN_RINGING_URI.equals(uri)) {
                updateVibrateWhenRinging();
            }
        }
    }

    private final class H extends Handler {
        private static final int UPDATE_PHONE_RINGTONE = 1;
        private static final int UPDATE_NOTIFICATION_RINGTONE = 2;
        private static final int STOP_SAMPLE = 3;
        private static final int UPDATE_EFFECTS_SUPPRESSOR = 4;
        private static final int UPDATE_RINGER_MODE = 5;
        private static final int UPDATE_ALARM_RINGTONE = 6;
        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        private static final int UPDATE_SIM1_RINGTONE = 7;
        private static final int UPDATE_SIM2_RINGTONE = 8;
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
        private H() {
            super(Looper.getMainLooper());
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATE_PHONE_RINGTONE:
                    mPhoneRingtonePreference.setSummary((CharSequence) msg.obj);
                    break;
                case UPDATE_NOTIFICATION_RINGTONE:
                    mNotificationRingtonePreference.setSummary((CharSequence) msg.obj);
                    break;
                case STOP_SAMPLE:
                    mVolumeCallback.stopSample();
                    break;
                case UPDATE_EFFECTS_SUPPRESSOR:
                    updateEffectsSuppressor();
                    break;
                case UPDATE_RINGER_MODE:
                    updateRingerMode();
                    break;
                case UPDATE_ALARM_RINGTONE:
                    mAlarmRingtonePreference.setSummary((CharSequence) msg.obj);
                    break;
                /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
                case UPDATE_SIM1_RINGTONE:
                    mSim1PhoneRingtonePreference.setSummary((CharSequence) msg.obj);
                    break;
                case UPDATE_SIM2_RINGTONE:
                    mSim2PhoneRingtonePreference.setSummary((CharSequence) msg.obj);
                    break;
                /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
            }
        }
    }

    private class Receiver extends BroadcastReceiver {
        private boolean mRegistered;

        public void register(boolean register) {
            if (mRegistered == register) return;
            if (register) {
                final IntentFilter filter = new IntentFilter();
                filter.addAction(NotificationManager.ACTION_EFFECTS_SUPPRESSOR_CHANGED);
                filter.addAction(AudioManager.INTERNAL_RINGER_MODE_CHANGED_ACTION);
                mContext.registerReceiver(this, filter);
            } else {
                mContext.unregisterReceiver(this);
            }
            mRegistered = register;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (NotificationManager.ACTION_EFFECTS_SUPPRESSOR_CHANGED.equals(action)) {
                mHandler.sendEmptyMessage(H.UPDATE_EFFECTS_SUPPRESSOR);
            } else if (AudioManager.INTERNAL_RINGER_MODE_CHANGED_ACTION.equals(action)) {
                mHandler.sendEmptyMessage(H.UPDATE_RINGER_MODE);
            }
        }
    }

    // === Summary ===

    private static class SummaryProvider extends BroadcastReceiver
            implements SummaryLoader.SummaryProvider {

        private final Context mContext;
        private final AudioManager mAudioManager;
        private final SummaryLoader mSummaryLoader;
        private final int maxVolume;

        public SummaryProvider(Context context, SummaryLoader summaryLoader) {
            mContext = context;
            mSummaryLoader = summaryLoader;
            mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
            maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
        }

        @Override
        public void setListening(boolean listening) {
            if (listening) {
                IntentFilter filter = new IntentFilter();
                filter.addAction(AudioManager.VOLUME_CHANGED_ACTION);
                filter.addAction(AudioManager.STREAM_DEVICES_CHANGED_ACTION);
                filter.addAction(AudioManager.RINGER_MODE_CHANGED_ACTION);
                filter.addAction(AudioManager.INTERNAL_RINGER_MODE_CHANGED_ACTION);
                filter.addAction(AudioManager.STREAM_MUTE_CHANGED_ACTION);
                filter.addAction(NotificationManager.ACTION_EFFECTS_SUPPRESSOR_CHANGED);
                mContext.registerReceiver(this, filter);
            } else {
                mContext.unregisterReceiver(this);
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String percent =  NumberFormat.getPercentInstance().format(
                    (double) mAudioManager.getStreamVolume(AudioManager.STREAM_RING) / maxVolume);
            mSummaryLoader.setSummary(this,
                    mContext.getString(R.string.sound_settings_summary, percent));
        }
    }

    public static final SummaryLoader.SummaryProviderFactory SUMMARY_PROVIDER_FACTORY
            = new SummaryLoader.SummaryProviderFactory() {
        @Override
        public SummaryLoader.SummaryProvider createSummaryProvider(Activity activity,
                SummaryLoader summaryLoader) {
            return new SummaryProvider(activity, summaryLoader);
        }
    };

    // === Indexing ===

    public static final BaseSearchIndexProvider SEARCH_INDEX_DATA_PROVIDER =
            new BaseSearchIndexProvider() {

        public List<SearchIndexableResource> getXmlResourcesToIndex(
                Context context, boolean enabled) {
            final SearchIndexableResource sir = new SearchIndexableResource(context);
            sir.xmlResId = R.xml.sound_settings;
            return Arrays.asList(sir);
        }

        public List<String> getNonIndexableKeys(Context context) {
            final ArrayList<String> rt = new ArrayList<String>();
            if (Utils.isVoiceCapable(context)) {
                rt.add(KEY_NOTIFICATION_VOLUME);
            } else {
                rt.add(KEY_RING_VOLUME);
                rt.add(KEY_PHONE_RINGTONE);
                rt.add(KEY_WIFI_DISPLAY);
                rt.add(KEY_VIBRATE_WHEN_RINGING);
            }

            final PackageManager pm = context.getPackageManager();
            final UserManager um = (UserManager) context.getSystemService(Context.USER_SERVICE);

            // Enable link to CMAS app settings depending on the value in config.xml.
            boolean isCellBroadcastAppLinkEnabled = context.getResources().getBoolean(
                    com.android.internal.R.bool.config_cellBroadcastAppLinks);
            try {
                if (isCellBroadcastAppLinkEnabled) {
                    if (pm.getApplicationEnabledSetting("com.android.cellbroadcastreceiver")
                            == PackageManager.COMPONENT_ENABLED_STATE_DISABLED) {
                        isCellBroadcastAppLinkEnabled = false;  // CMAS app disabled
                    }
                }
            } catch (IllegalArgumentException ignored) {
                isCellBroadcastAppLinkEnabled = false;  // CMAS app not installed
            }
            if (!um.isAdminUser() || !isCellBroadcastAppLinkEnabled) {
                rt.add(KEY_CELL_BROADCAST_SETTINGS);
            }

            return rt;
        }
    };

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2759309
//[ALWE][6070]Create notification sound menu
    private void updateRingAndNotificationPreference(){
        mRingPreference.showIcon(mSuppressor != null
                ? com.android.internal.R.drawable.ic_audio_ring_notif_mute
                : mRingerMode == AudioManager.RINGER_MODE_VIBRATE
                ? com.android.internal.R.drawable.ic_audio_ring_notif_vibrate
                : mRingerMode == AudioManager.RINGER_MODE_SILENT
                ? com.android.internal.R.drawable.ic_audio_ring_notif_mute
                : com.android.internal.R.drawable.ic_audio_ring_notif);
        mNotificationPreference.showIcon(mSuppressor != null
                ? com.android.internal.R.drawable.ic_audio_notification_mute
                : mRingerMode == AudioManager.RINGER_MODE_VIBRATE
                ? com.android.internal.R.drawable.ic_audio_ring_notif_vibrate
                : mRingerMode == AudioManager.RINGER_MODE_SILENT
                ? com.android.internal.R.drawable.ic_audio_notification_mute
                : com.android.internal.R.drawable.ic_audio_notification);
    }

    private void ringOrNotificationPreferenceUpdate() {
          mRingOrNotificationPreference.showIcon(mSuppressor != null
                  ? R.drawable.ic_ring_volume_off
                  : mRingerMode == AudioManager.RINGER_MODE_VIBRATE
                  ? com.android.internal.R.drawable.ic_audio_ring_notif_vibrate
                  : mRingerMode == AudioManager.RINGER_MODE_SILENT
                  ? R.drawable.ic_ring_volume_off
                  : R.drawable.ic_ring_volume_on);
   }

    private void ringAndNotificationPreferenceUpdate(){
        mRingPreference.showIcon(mSuppressor != null
                ? R.drawable.ic_ring_volume_off
                : mRingerMode == AudioManager.RINGER_MODE_VIBRATE
                ? com.android.internal.R.drawable.ic_audio_ring_notif_vibrate
                : mRingerMode == AudioManager.RINGER_MODE_SILENT
                ? R.drawable.ic_ring_volume_off
                : R.drawable.ic_ring_volume_on);
        mNotificationPreference.showIcon(mSuppressor != null
                ? com.android.internal.R.drawable.ic_audio_ring_notif_mute
                : mRingerMode == AudioManager.RINGER_MODE_VIBRATE
                ? com.android.internal.R.drawable.ic_audio_ring_notif_vibrate
                : mRingerMode == AudioManager.RINGER_MODE_SILENT
                ? com.android.internal.R.drawable.ic_audio_ring_notif_mute
                : com.android.internal.R.drawable.ic_audio_ring_notif);
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    //[BUGFIX]-Mod-BEGIN by TCTNB.yubin.ying,09/06/2016,Solution-2521300,
    private void initSilentMode() {
        mSilentMode = (TwoStatePreference) getPreferenceScreen().findPreference(KEY_SILENT_MODE);
        if (mSilentMode == null) {
            Log.i(TAG, "Preference not found: " + KEY_SILENT_MODE);
            return;
        }
        mSilentMode.setChecked(mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT);
        mSilentMode.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final boolean val = (Boolean) newValue;
                mAudioManager.setRingerMode(val ? AudioManager.RINGER_MODE_SILENT : AudioManager.RINGER_MODE_NORMAL);
                return true;
            }
        });
    }
    //[BUGFIX]-Mod-END by TCTNB.yubin.ying
}
