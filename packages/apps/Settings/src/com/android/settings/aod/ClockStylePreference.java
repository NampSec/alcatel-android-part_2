/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.settings.aod;

import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.util.AttributeSet;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceViewHolder;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.view.MotionEvent;
import android.view.View;
import com.android.settings.R;
import com.android.settings.aod.view.ClockStyleAdapter;
import static android.provider.Settings.System.AOD_COLCK_STYLE;

public class ClockStylePreference extends Preference implements
        OnItemClickListener, View.OnTouchListener {

    private Context mContext;

    private GridView mClockStyleGridView;
    private ClockStyleAdapter mClockStyleAdapter;
    private int[] mClockStyle = { R.drawable.always_on_choose_1,
            R.drawable.always_on_choose_2, R.drawable.always_on_choose_3,
            R.drawable.always_on_choose_4, R.drawable.always_on_choose_5,
            R.drawable.always_on_choose_6, R.drawable.always_on_choose_7 };

    public ClockStylePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setLayoutResource(R.layout.clock_style_prefence);
    }

    @Override
    public void onBindViewHolder(PreferenceViewHolder view) {
        super.onBindViewHolder(view);
        // TODO Auto-generated method stub
        initGridView(view);
    }

    private void initGridView(PreferenceViewHolder view) {
        // TODO Auto-generated method stub
        mClockStyleGridView = (GridView) view
                .findViewById(R.id.clock_style_grid_view);
        mClockStyleAdapter = new ClockStyleAdapter(mContext, mClockStyle);
        mClockStyleGridView.setAdapter(mClockStyleAdapter);
        mClockStyleAdapter.setFocusPosition(Settings.System.getInt(
                mContext.getContentResolver(), AOD_COLCK_STYLE, 0));
        mClockStyleGridView.setOnItemClickListener(this);
        mClockStyleGridView.setOnTouchListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        Settings.System.putInt(mContext.getContentResolver(), AOD_COLCK_STYLE,
                position);
        mClockStyleAdapter.setFocusPosition(position);
    }

    @Override
    public boolean onTouch(View arg0, MotionEvent arg1) {
        // TODO Auto-generated method stub
        mClockStyleGridView.getParent()
                .requestDisallowInterceptTouchEvent(true);
        return false;
    }
}
