/*                                                               Date:06/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2014 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  TCTNB.93391                                                     */
/*  Email  :  xinchao.zhang@tcl.com                                           */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/31/2016|     TCTNB.93391      |       ALM2655959      |  Diag Protect    */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;
import android.os.SystemProperties;
import android.hardware.usb.UsbManager;

public class DiagProtectorReceiver extends BroadcastReceiver{

    // private UsbManager mUsbManager;
    private static final String TAG = "DiagProtectorReceiver";
    private static final String ACTION_USB_PROTECT_CHANGED = "sys.usbprotect.changed";
    private static final String PROPERTY_DIAGPROTECT = "persist.sys.usb.protect";

    @Override
    public void onReceive(Context content, Intent intent) {

        String action = intent.getAction();

        if (action.equals(ACTION_USB_PROTECT_CHANGED)) {

            Log.i(TAG, "recieve broadcaset sys.usbprotect.changed");

            String strDiagProtect = intent.getExtra("usbprotect").toString();
            SystemProperties.set(PROPERTY_DIAGPROTECT, strDiagProtect);
            Log.i(TAG, "set usbprotect= " + strDiagProtect);

            String functions = SystemProperties.get("persist.sys.usb.config", "");

            if(strDiagProtect.equals("1")) { // from off -> on
                Log.i(TAG, "Off->On, get Functions=" + functions);
                if (functions.contains(UsbManager.USB_FUNCTION_MTP)) {
                    functions = UsbManager.USB_FUNCTION_MTP;
                } else if (functions.contains(UsbManager.USB_FUNCTION_PTP)) {
                     functions = UsbManager.USB_FUNCTION_PTP;
                } else if (functions.contains(UsbManager.USB_FUNCTION_MIDI)) {
                     functions = UsbManager.USB_FUNCTION_MIDI;
                } else if (functions.contains(UsbManager.USB_FUNCTION_CHARGING)) {
                     functions = UsbManager.USB_FUNCTION_CHARGING;
                } else {
                     functions = UsbManager.USB_FUNCTION_MTP;
                }
            } else { // from on->off
                Log.i(TAG, "On->Off, get Functions=" + functions);
                if (functions.contains(UsbManager.USB_FUNCTION_MTP)) {
                    functions = "mtp,diag";
                } else if (functions.contains(UsbManager.USB_FUNCTION_PTP)) {
                     functions = "ptp,diag";
                } else if (functions.contains(UsbManager.USB_FUNCTION_MIDI)) {
                     functions = "midi,diag";
                } else if (functions.contains(UsbManager.USB_FUNCTION_CHARGING)) {
                     functions = "charging,diag";
                } else {
                     functions = "mtp,diag";
                }
            }
            Log.i(TAG, "Set Functions=" + functions);

            UsbManager mUsbManager = (UsbManager)content.getSystemService(content.USB_SERVICE);
            mUsbManager.setCurrentFunction(functions);
            //[BUGFIX]-Mod-BEGIN by TCTNB.yubin.ying,10/26/2016,3215384,
            if(functions.contains(UsbManager.USB_FUNCTION_CHARGING)) {
                mUsbManager.setUsbDataUnlocked(false);
            } else {
                mUsbManager.setUsbDataUnlocked(true);
            }
            //[BUGFIX]-Mod-END by TCTNB.yubin.ying
        }
    }
}
