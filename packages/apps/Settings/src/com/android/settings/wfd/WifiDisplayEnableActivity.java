/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/11/2016|jianhong.yang@tcl.com |      Task2694490     |   Porting code   */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings.wfd;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewParent;
import android.view.ViewGroup;
import android.content.Intent;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.hardware.display.DisplayManager;

public class WifiDisplayEnableActivity extends Activity{
    /** Called when the activity is first created. */
    private DisplayManager mDisplayManager;
    private WifiManager mWifiManager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDisplayManager = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        Intent intent = getIntent();
        String action = intent.getAction();
        if (Intent.ACTION_SEND.equals(action) || Intent.ACTION_SEND_MULTIPLE.equals(action)) {//modify pr1000377 cuihua.yang 2015-12-07
            if (! mWifiManager.isWifiEnabled()) {
                if(mWifiManager.isWifiApEnabled()) {
                    Intent in = new Intent();
                    in.setClassName("com.android.settings", "com.android.settings.wfd.WifiDisplayEnableNoticeActivity");
                    in.putExtra("WifiDisplaySettings", 2);
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(in);
                    finish();
                } else {
                    Intent in = new Intent();
                    in.setClassName("com.android.settings", "com.android.settings.wfd.WifiDisplayEnableNoticeActivity");
                    in.putExtra("WifiDisplaySettings", 0);
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(in);
                    finish();
                }
            } else if (mWifiManager.isWifiEnabled() && mDisplayManager.getWifiDisplayStatus().getFeatureState() == 2) {
                Intent in = new Intent();
                in.setClassName("com.android.settings", "com.android.settings.wfd.WifiDisplayEnableNoticeActivity");
                in.putExtra("WifiDisplaySettings", 1);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(in);
                finish();
            } else {
                startTetheringInterface(true);
            }
        } else {
            startTetheringInterface(false);
        }

    }

    private void startTetheringInterface (boolean isFromShare) {
        Intent intent = new Intent("android.settings.CAST_SETTINGS");
        intent.putExtra("dismissWhenConnected", isFromShare);
        startActivity(intent);
        WifiDisplayEnableActivity.this.finish();
    }
}

