/******************************************************************************/
/*                                                               Date:04/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2013 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Ruili.Liu                                                       */
/*  Email  :  ruili.liu@tcl.com                                               */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : when share photo via wifidisplay,pop up this alertdialog       */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 08/11/2016|jianhong.yang@tcl.com |      Task2694490     |   Porting code   */
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.settings.wfd;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.display.WifiDisplayStatus;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.settings.R;
import com.android.settings.Settings.WifiDisplaySettingsActivity;

import android.app.AlertDialog;

import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController;

public class WifiDisplayEnableNoticeActivity extends AlertActivity implements DialogInterface.OnClickListener {

    public static final String TAG = "WifiDisplayEnableNoticeActivity";
    private static final int WIFI_DISPLAY_DISABLE_REASON_WIFI_OFF = 0;
    private static final int WIFI_DISPLAY_DISABLE_REASON_OFF = 1;
    private static final int WIFI_DISPLAY_DISABLE_REASON_HOTSPOT_ON = 2;

    private Intent in;
    private int mFlag;

    @Override
    protected void onCreate(Bundle saveInstaBundle) {
        super.onCreate(saveInstaBundle);

        in = getIntent();
        mFlag = in.getIntExtra("WifiDisplaySettings", WIFI_DISPLAY_DISABLE_REASON_OFF);
        final AlertController.AlertParams p = mAlertParams;
        if (mFlag == WIFI_DISPLAY_DISABLE_REASON_WIFI_OFF){
            p.mTitle = getString(R.string.wifi_display_alertdialog_title_openwifi);
        } else if (mFlag == WIFI_DISPLAY_DISABLE_REASON_HOTSPOT_ON) {
            p.mTitle = getString(R.string.wifi_display_alertdialog_title_openwifidisplay);
        }
        p.mView = createView();
        p.mPositiveButtonText = getString(R.string.wifi_display_alertdialog_option_turn_on);
        p.mPositiveButtonListener = this;
        p.mNegativeButtonText = getString(R.string.wifi_display_alertdialog_option_cancel);
        p.mNegativeButtonListener = this;
        setupAlert();
    }

    private View createView() {
        View view = getLayoutInflater().inflate(R.layout.confirm_dialog, null);
        TextView contentView = (TextView)view.findViewById(R.id.content);

        contentView.setTextColor(Color.BLACK);
        if (mFlag == WIFI_DISPLAY_DISABLE_REASON_WIFI_OFF) {
            contentView.setText(getString(R.string.wifi_display_alertdialog_text_turnonwifi));
        } else if (mFlag == WIFI_DISPLAY_DISABLE_REASON_OFF) {
            contentView.setText(getString(R.string.wifi_display_alertdialog_text_openwifidisplay));

        } else if(mFlag == WIFI_DISPLAY_DISABLE_REASON_HOTSPOT_ON)  {
            contentView.setText(getString(R.string.wifi_display_alertdialog_text_turnoffhotspot));
        }

        return view;
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
          case DialogInterface.BUTTON_POSITIVE:

              boolean isAirplaneMode = Settings.Global.getInt(this.getContentResolver(),
                      Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
              if(isAirplaneMode) {
                  Toast.makeText(this, R.string.wifi_in_airplane_mode, Toast.LENGTH_SHORT).show();
                  return;
              }
            WifiManager mWifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
            if (mFlag == WIFI_DISPLAY_DISABLE_REASON_HOTSPOT_ON || mFlag == WIFI_DISPLAY_DISABLE_REASON_WIFI_OFF) {
                if(mFlag == WIFI_DISPLAY_DISABLE_REASON_HOTSPOT_ON &&(mWifiManager.getWifiApState()==WifiManager.WIFI_AP_STATE_ENABLED))
                    mWifiManager.setWifiApEnabled(null, false);
                while(mWifiManager.getWifiApState()!=WifiManager.WIFI_AP_STATE_DISABLED){
                    try {
                         Thread.sleep(100);
                    } catch (InterruptedException e) {
                         // TODO Auto-generated catch block
                         e.printStackTrace();
                    }
                }
                mWifiManager.setWifiEnabled(true);
                Toast.makeText(this, getString(R.string.wifi_starting),Toast.LENGTH_SHORT).show();
                while(mWifiManager.getWifiState()!=WifiManager.WIFI_STATE_ENABLED){
                    try {
                         Thread.sleep(100);
                    } catch (InterruptedException e) {
                         // TODO Auto-generated catch block
                         e.printStackTrace();
                    }
                }
                Settings.Global.putInt(getContentResolver(), Settings.Global.WIFI_SAVED_STATE, 2);
            }
            Settings.Global.putInt(getContentResolver(), Settings.Global.WIFI_DISPLAY_ON, 1);
            Intent intent = new Intent("android.settings.CAST_SETTINGS");
            intent.putExtra("dismissWhenConnected", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            break;

        case DialogInterface.BUTTON_NEGATIVE:
            finish();
            break;
        }
    }
}
