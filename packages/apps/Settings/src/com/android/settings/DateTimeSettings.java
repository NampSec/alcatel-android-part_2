/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceChangeListener;
import android.text.format.DateFormat;
import android.widget.DatePicker;
import android.widget.TimePicker;
import com.android.internal.logging.MetricsProto.MetricsEvent;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settingslib.RestrictedLockUtils;
import com.android.settingslib.RestrictedSwitchPreference;
import com.android.settingslib.datetime.ZoneGetter;

import java.util.Calendar;
import java.util.Date;

import static com.android.settingslib.RestrictedLockUtils.EnforcedAdmin;
import android.content.res.Resources;
import android.text.BidiFormatter;
import android.text.TextDirectionHeuristics;
import android.text.TextUtils;
import android.view.View;
import java.text.SimpleDateFormat;
import android.os.SystemProperties;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
import android.content.res.Resources;
import android.support.v7.preference.ListPreference;
import android.text.TextUtils;
import android.util.Log;

import java.text.SimpleDateFormat;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

//[BUGFIX]-Add-BEGIN by TCTNB.Yang.Hu,2015/11/17, Defect-937969 TCT DRM solution
import android.drm.DrmManagerClient;
import android.text.format.Time;
import com.tct.drm.TctSystemClock;
//[BUGFIX]-Add-END by TCTNB.Yang.Hu,2015/11/17

public class DateTimeSettings extends SettingsPreferenceFragment
        implements OnTimeSetListener, OnDateSetListener, OnPreferenceChangeListener {

    private static final String HOURS_12 = "12";
    private static final String HOURS_24 = "24";

    // Used for showing the current date format, which looks like "12/31/2010", "2010/12/13", etc.
    // The date value is dummy (independent of actual date).
    private Calendar mDummyDate;

    private static final String KEY_AUTO_TIME = "auto_time";
    private static final String KEY_AUTO_TIME_ZONE = "auto_zone";

    private static final int DIALOG_DATEPICKER = 0;
    private static final int DIALOG_TIMEPICKER = 1;

    // have we been launched from the setup wizard?
    protected static final String EXTRA_IS_FIRST_RUN = "firstRun";

    // Minimum time is Nov 5, 2007, 0:00.
    private static final long MIN_DATE = 1194220800000L;

    private RestrictedSwitchPreference mAutoTimePref;
    private Preference mTimePref;
    private Preference mTime24Pref;
    private SwitchPreference mAutoTimeZonePref;
    private Preference mTimeZone;
    private Preference mDatePref;

    //ADD-BEGIN by Dingyi  2016/08/25 TASK 2791962
    private static Context mContext = null;
    private static boolean isRuTZ = false;
    private static Resources resources = null;
    private static final HashMap<String, String> mCityZoneMap = new HashMap<String, String>() {
    {   put("Europe/Kaliningrad", "1");
        put("Europe/Moscow", "2");
        put("Europe/Volgograd", "2");
        put("Europe/Samara", "3");
        put("Asia/Yekaterinburg", "4");
        put("Asia/Omsk", "5");
        put("Asia/Novosibirsk", "6");
        put("Asia/Krasnoyarsk", "6");
        put("Asia/Irkutsk", "7");
        put("Asia/Yakutsk", "8");
        put("Asia/Vladivostok", "9");
        put("Asia/Sakhalin", "9");
        put("Asia/Magadan", "10");
        put("Asia/Kamchatka", "11");
        put("Asia/Anadyr", "11");
    }
    };
    //ADD-END by Dingyi  2016/08/25 TASK 2791962

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
    private static final String TAG = "DateTimeSettings";
    private ListPreference mDateFormat;

    private static final String KEY_DATE_FORMAT = "date_format";
    private static boolean showChooseDateFormat = false;
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    @Override
    protected int getMetricsCategory() {
        return MetricsEvent.DATE_TIME;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        addPreferencesFromResource(R.xml.date_time_prefs);
        //ADD-BEGIN by Dingyi  2016/08/25 TASK 2791962
        mContext = getActivity();
        isRuTZ=mContext.getResources().getBoolean(R.bool.settings_ru_timenzone_show);
        //ADD-END by Dingyi  2016/08/25 TASK 2791962

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
        //Defect1061782
        resources = getResources();
        showChooseDateFormat = resources.getBoolean(com.android.internal.R.bool.def_tctfw_systemUI_Dateformat_same_with_setting);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        initUI();
    }

    private void initUI() {
        boolean autoTimeEnabled = getAutoState(Settings.Global.AUTO_TIME);
        boolean autoTimeZoneEnabled = getAutoState(Settings.Global.AUTO_TIME_ZONE);

        mAutoTimePref = (RestrictedSwitchPreference) findPreference(KEY_AUTO_TIME);
        mAutoTimePref.setOnPreferenceChangeListener(this);
        EnforcedAdmin admin = RestrictedLockUtils.checkIfAutoTimeRequired(getActivity());
        mAutoTimePref.setDisabledByAdmin(admin);

        Intent intent = getActivity().getIntent();
        boolean isFirstRun = intent.getBooleanExtra(EXTRA_IS_FIRST_RUN, false);

        mDummyDate = Calendar.getInstance();

        // If device admin requires auto time device policy manager will set
        // Settings.Global.AUTO_TIME to true. Note that this app listens to that change.
        mAutoTimePref.setChecked(autoTimeEnabled);
        mAutoTimeZonePref = (SwitchPreference) findPreference(KEY_AUTO_TIME_ZONE);
        mAutoTimeZonePref.setOnPreferenceChangeListener(this);
        // Override auto-timezone if it's a wifi-only device or if we're still in setup wizard.
        // TODO: Remove the wifiOnly test when auto-timezone is implemented based on wifi-location.
        if (Utils.isWifiOnly(getActivity()) || isFirstRun) {
            getPreferenceScreen().removePreference(mAutoTimeZonePref);
            autoTimeZoneEnabled = false;
        }
        mAutoTimeZonePref.setChecked(autoTimeZoneEnabled);

        mTimePref = findPreference("time");
        mTime24Pref = findPreference("24 hour");
        mTimeZone = findPreference("timezone");
        mDatePref = findPreference("date");

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
        mDateFormat = (ListPreference) findPreference(KEY_DATE_FORMAT);//Defect1061782
        if (isFirstRun) {
            getPreferenceScreen().removePreference(mTime24Pref);
            getPreferenceScreen().removePreference(mDateFormat);//Defect1061782
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

        mTimePref.setEnabled(!autoTimeEnabled);
        mDatePref.setEnabled(!autoTimeEnabled);
        mTimeZone.setEnabled(!autoTimeZoneEnabled);

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
        //Defect1061782
        Log.d(TAG, "def_tctfw_systemUI_Dateformat_same_with_setting = " + showChooseDateFormat);
        if (!showChooseDateFormat) {
            getPreferenceScreen().removePreference(mDateFormat);
            return;
        }
        String [] dateFormats = getResources().getStringArray(R.array.date_format_values);
        String [] formattedDates = new String[dateFormats.length];
        String currentFormat = getDateFormat();
        Log.i(TAG, "initUI currentFormat=" + currentFormat);
        // Initialize if DATE_FORMAT is not set in the system settings
        // This can happen after a factory reset (or data wipe)
        if (currentFormat == null) {
            currentFormat = "";
        }

        currentFormat = currentFormat.replace("-", ".").replace("/", ".");;
        mDummyDate.set(mDummyDate.get(Calendar.YEAR), mDummyDate.DECEMBER, 31, 13, 0, 0);
        java.text.DateFormat shortDateFormat = DateFormat.getDateFormat(getActivity());
        String formatted;
        for (int i = 0; i < formattedDates.length; i++) {
            if (i == 0) {
                formatted = shortDateFormat.format(mDummyDate.getTime());
                formatted = formatted.replace("/", ".");
                formattedDates[i] = getResources().getString(R.string.normal_date_format, formatted);
                Log.i(TAG, "initUI formatted"  + i + "=" + formattedDates[i]);
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormats[i]);
                formatted = sdf.format(mDummyDate.getTime());
                Log.i(TAG, "initUI formatted" + i + "=" + formatted);
                formattedDates[i] = formatted;
            }
        }
        mDateFormat.setEntries(formattedDates);
        mDateFormat.setEntryValues(dateFormats);

        if (TextUtils.isEmpty(currentFormat)) {
            currentFormat = dateFormats[0];
        }

        mDateFormat.setValue(currentFormat);
        mDateFormat.setOnPreferenceChangeListener(this);
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    @Override
    public void onResume() {
        super.onResume();

        ((SwitchPreference)mTime24Pref).setChecked(is24Hour());

        // Register for time ticks and other reasons for time change
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        getActivity().registerReceiver(mIntentReceiver, filter, null, null);

        updateTimeAndDateDisplay(getActivity());
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mIntentReceiver);
    }

    public void updateTimeAndDateDisplay(Context context) {
        final Calendar now = Calendar.getInstance();
        mDummyDate.setTimeZone(now.getTimeZone());
        // We use December 31st because it's unambiguous when demonstrating the date format.
        // We use 13:00 so we can demonstrate the 12/24 hour options.
        mDummyDate.set(now.get(Calendar.YEAR), 11, 31, 13, 0, 0);
        Date dummyDate = mDummyDate.getTime();
        if (getResources().getBoolean(R.bool.config_regional_date_format)) {
            String dateFormat = Settings.System.getString(context.getContentResolver(),
                    Settings.System.DATE_FORMAT);
            mDatePref.setSummary(DateFormat.format(dateFormat,now.getTime()));
        } else {
            mDatePref.setSummary(DateFormat.getLongDateFormat(context).format(now.getTime()));
        }
        mTimePref.setSummary(DateFormat.getTimeFormat(getActivity()).format(now.getTime()));
        //MODIFY-BEGIN by Dingyi  2016/08/25 TASK 2791962
//        mTimeZone.setSummary(ZoneGetter.getTimeZoneOffsetAndName(now.getTimeZone(), now.getTime()));
        mTimeZone.setSummary(getTimeZoneOffsetAndName(now.getTimeZone(), now.getTime()));
        //MODIFY-END by Dingyi  2016/08/25 TASK 2791962
        mTime24Pref.setSummary(DateFormat.getTimeFormat(getActivity()).format(dummyDate));

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
        if (showChooseDateFormat) {
            java.text.DateFormat dtFormat;
            String sel_dtfmt = getDateFormat();
            if ("".equals(sel_dtfmt) || sel_dtfmt == null) {
                dtFormat = DateFormat.getDateFormat(getActivity());
                Log.i(TAG , "sel_dtfmt is null, shortDateFormat=" + dtFormat);
            } else {
                dtFormat = new SimpleDateFormat(sel_dtfmt);
                Log.i(TAG , "shortDateFormat=" + sel_dtfmt);
            }
            String date = dtFormat.format(now.getTime());
            String dateFormat = dtFormat.format(dummyDate);
            date = date.replace("/", ".").replace("-", ".");
            dateFormat = dateFormat.replace("/", ".").replace("-", ".");
            mDatePref.setSummary(date);
            mDateFormat.setSummary(dateFormat);
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
    }

    //ADD-BEGIN by Dingyi  2016/08/25 TASK 2791962
    public static String getTimeZoneOffsetAndName(TimeZone tz, Date now) {
        Locale locale = Locale.getDefault();
        String gmtString = getGmtOffsetString(locale, tz, now);
        String zoneNameString = getZoneLongName(locale, tz, now);
        if (zoneNameString == null) {
            return gmtString;
        }

        if (SystemProperties.getBoolean("ro.ssv.enabled", false)) {
            String operator = SystemProperties.get("ro.ssv.operator.choose","TEF");
            if (("AMV".equals(operator)) || ("TEF".equals(operator))) {
                String timeZoneForAMX = getTimeZoneforLatamSSV();
                zoneNameString = TextUtils.isEmpty(timeZoneForAMX) ?  zoneNameString: timeZoneForAMX;
            }
        }

        // We don't use punctuation here to avoid having to worry about localizing that too!
        return gmtString + " " + zoneNameString;
    }

    private static String getGmtOffsetString(Locale locale, TimeZone tz, Date now) {
        // Use SimpleDateFormat to format the GMT+00:00 string.
        SimpleDateFormat gmtFormatter = new SimpleDateFormat("ZZZZ");
        gmtFormatter.setTimeZone(tz);
        String gmtString = gmtFormatter.format(now);

        // Ensure that the "GMT+" stays with the "00:00" even if the digits are RTL.
        BidiFormatter bidiFormatter = BidiFormatter.getInstance();
        boolean isRtl = TextUtils.getLayoutDirectionFromLocale(locale) == View.LAYOUT_DIRECTION_RTL;
        gmtString = bidiFormatter.unicodeWrap(gmtString,
                isRtl ? TextDirectionHeuristics.RTL : TextDirectionHeuristics.LTR);
        return gmtString;
    }

    private static String getZoneLongName(Locale locale, TimeZone tz, Date now) {
        if (isRuTZ) {
            String id = tz.getID();
            if (mCityZoneMap.containsKey(id)) {
                String[] russianTimezoneNames = mContext.getResources().getStringArray(R.array.russian_timezonename);
                return russianTimezoneNames[Integer.parseInt(mCityZoneMap.get(id)) -1 ];
             }
        }
        boolean daylight = tz.inDaylightTime(now);
        // This returns a name if it can, or will fall back to GMT+0X:00 format.
        return tz.getDisplayName(daylight, TimeZone.LONG, locale);
    }
    private  static String getTimeZoneforLatamSSV(){
        String getTimeZonePropAMX = SystemProperties.get("persist.sys.timezone");
        String mccmnc = SystemProperties.get("persist.sys.lang.mccmnc", "");
        String operator = SystemProperties.get("ro.ssv.operator.choose","TEF");
        String timeZoneDisplayNameLatamSsv[] = resources.getStringArray(R.array.timezone_lable_for_latam_ssv);

        if ("America/Argentina/Buenos_Aires".equals(getTimeZonePropAMX)) {
            return timeZoneDisplayNameLatamSsv[0];
        } else if ("America/Santiago".equals(getTimeZonePropAMX)) {
            return timeZoneDisplayNameLatamSsv[9];
        } else if ("America/Bogota".equals(getTimeZonePropAMX)){
            if ("TEF".equals(operator) && (mccmnc != null) && (mccmnc.startsWith("740"))) {
                return timeZoneDisplayNameLatamSsv[10];
            } else {
                return timeZoneDisplayNameLatamSsv[1];
            }
        } else if ("America/Costa_Rica".equals(getTimeZonePropAMX)) {
            return timeZoneDisplayNameLatamSsv[2];
        } else if ("America/Santo_Domingo".equals(getTimeZonePropAMX)) {
            return timeZoneDisplayNameLatamSsv[3];
        } else if ("America/Lima".equals(getTimeZonePropAMX)) {
            return timeZoneDisplayNameLatamSsv[8];
        } else if ("America/Montevideo".equals(getTimeZonePropAMX)) {
            return timeZoneDisplayNameLatamSsv[4];
        } else if ("America/Panama".equals(getTimeZonePropAMX)) {
            return timeZoneDisplayNameLatamSsv[6];
        } else if ("America/Mexico_City".equals(getTimeZonePropAMX)) {
            return timeZoneDisplayNameLatamSsv[7];
        } else if ("America/Puerto_Rico".equals(getTimeZonePropAMX)) {
            return timeZoneDisplayNameLatamSsv[5];
        }

        return "";
     }
    //ADD-END by Dingyi  2016/08/25 TASK 2791962
    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        final Activity activity = getActivity();
        if (activity != null) {
            setDate(activity, year, month, day);
            updateTimeAndDateDisplay(activity);
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        final Activity activity = getActivity();
        if (activity != null) {
            setTime(activity, hourOfDay, minute);
            updateTimeAndDateDisplay(activity);
        }

        // We don't need to call timeUpdated() here because the TIME_CHANGED
        // broadcast is sent by the AlarmManager as a side effect of setting the
        // SystemClock time.
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference.getKey().equals(KEY_AUTO_TIME)) {
            boolean autoEnabled = (Boolean) newValue;
            Settings.Global.putInt(getContentResolver(), Settings.Global.AUTO_TIME,
                    autoEnabled ? 1 : 0);
            mTimePref.setEnabled(!autoEnabled);
            mDatePref.setEnabled(!autoEnabled);
        } else if (preference.getKey().equals(KEY_AUTO_TIME_ZONE)) {
            boolean autoZoneEnabled = (Boolean) newValue;
            Settings.Global.putInt(
                    getContentResolver(), Settings.Global.AUTO_TIME_ZONE, autoZoneEnabled ? 1 : 0);
            mTimeZone.setEnabled(!autoZoneEnabled);
//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
        } else if (preference.getKey().equals(KEY_DATE_FORMAT)) {
            String format = (String) newValue;
            Log.i(TAG, "onSharedPreferenceChanged format = " + format);
            Settings.System.putString(getContentResolver(),
                    Settings.System.DATE_FORMAT, format);
            updateTimeAndDateDisplay(getActivity());
        }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)
        return true;
    }

    @Override
    public Dialog onCreateDialog(int id) {
        final Calendar calendar = Calendar.getInstance();
        switch (id) {
        case DIALOG_DATEPICKER:
            DatePickerDialog d = new DatePickerDialog(
                    getActivity(),
                    this,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
            configureDatePicker(d.getDatePicker());
            return d;
        case DIALOG_TIMEPICKER:
            return new TimePickerDialog(
                    getActivity(),
                    this,
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE),
                    DateFormat.is24HourFormat(getActivity()));
        default:
            throw new IllegalArgumentException();
        }
    }

    static void configureDatePicker(DatePicker datePicker) {
        // The system clock can't represent dates outside this range.
        Calendar t = Calendar.getInstance();
        t.clear();
        t.set(1970, Calendar.JANUARY, 1);
        datePicker.setMinDate(t.getTimeInMillis());
        t.clear();
        t.set(2037, Calendar.DECEMBER, 31);
        datePicker.setMaxDate(t.getTimeInMillis());
    }

    /*
    @Override
    public void onPrepareDialog(int id, Dialog d) {
        switch (id) {
        case DIALOG_DATEPICKER: {
            DatePickerDialog datePicker = (DatePickerDialog)d;
            final Calendar calendar = Calendar.getInstance();
            datePicker.updateDate(
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
            break;
        }
        case DIALOG_TIMEPICKER: {
            TimePickerDialog timePicker = (TimePickerDialog)d;
            final Calendar calendar = Calendar.getInstance();
            timePicker.updateTime(
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE));
            break;
        }
        default:
            break;
        }
    }
    */
    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        if (preference == mDatePref) {
            showDialog(DIALOG_DATEPICKER);
        } else if (preference == mTimePref) {
            // The 24-hour mode may have changed, so recreate the dialog
            removeDialog(DIALOG_TIMEPICKER);
            showDialog(DIALOG_TIMEPICKER);
        } else if (preference == mTime24Pref) {
            final boolean is24Hour = ((SwitchPreference)mTime24Pref).isChecked();
            set24Hour(is24Hour);
            updateTimeAndDateDisplay(getActivity());
            timeUpdated(is24Hour);
        }
        return super.onPreferenceTreeClick(preference);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
            Intent data) {
        updateTimeAndDateDisplay(getActivity());
    }

    private void timeUpdated(boolean is24Hour) {
        Intent timeChanged = new Intent(Intent.ACTION_TIME_CHANGED);
        timeChanged.putExtra(Intent.EXTRA_TIME_PREF_24_HOUR_FORMAT, is24Hour);
        getActivity().sendBroadcast(timeChanged);
    }

    /*  Get & Set values from the system settings  */

    private boolean is24Hour() {
        return DateFormat.is24HourFormat(getActivity());
    }

    private void set24Hour(boolean is24Hour) {
        Settings.System.putString(getContentResolver(),
                Settings.System.TIME_12_24,
                is24Hour? HOURS_24 : HOURS_12);
    }

//[SOLUTION]-Add-BEGIN by TCTNB.(JiangLong Pan), 08/26/2016, SOLUTION-2520370
//[Pre-CTS][launcher][notification]date format should be changed
    //Defect1061782
    private String getDateFormat() {
        return Settings.System.getString(getContentResolver(),
                Settings.System.DATE_FORMAT);
    }
//[SOLUTION]-Add-END by TCTNB.(JiangLong Pan)

    private boolean getAutoState(String name) {
        try {
            return Settings.Global.getInt(getContentResolver(), name) > 0;
        } catch (SettingNotFoundException snfe) {
            return false;
        }
    }

    /* package */ static void setDate(Context context, int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        //[BUGFIX]-Add-BEGIN by TCTNB.Yang.Hu,2015/11/17, Defect-937969 TCT DRM solution
        Time time = new Time();
        time.setToNow();
        long currenttime = time.toMillis(false);
        //[BUGFIX]-Add-END by TCTNB.Yang.Hu,2015/11/17

        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);
        long when = Math.max(c.getTimeInMillis(), MIN_DATE);

        if (when / 1000 < Integer.MAX_VALUE) {
            ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).setTime(when);
            //[BUGFIX]-Add-BEGIN by TCTNB.Yang.Hu,2015/11/17, Defect-937969 TCT DRM solution
            TctSystemClock mTctClock = new TctSystemClock();
            mTctClock.setCurrentTimeMillis(when, when - currenttime);
            //[BUGFIX]-Add-END by TCTNB.Yang.Hu,2015/11/17
        }
    }

    /* package */ static void setTime(Context context, int hourOfDay, int minute) {
        Calendar c = Calendar.getInstance();

        //[BUGFIX]-Add-BEGIN by TCTNB.Yang.Hu,2015/11/17, Defect-937969 TCT DRM solution
        Time time = new Time();
        time.setToNow();
        long currenttime = time.toMillis(false);
        //[BUGFIX]-Add-END by TCTNB.Yang.Hu,2015/11/17

        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long when = Math.max(c.getTimeInMillis(), MIN_DATE);

        if (when / 1000 < Integer.MAX_VALUE) {
            ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).setTime(when);
            //[BUGFIX]-Add-BEGIN by TCTNB.Yang.Hu,2015/11/17, Defect-937969 TCT DRM solution
            TctSystemClock mTctClock = new TctSystemClock();
            mTctClock.setCurrentTimeMillis(when, when - currenttime);
            //[BUGFIX]-Add-END by TCTNB.Yang.Hu,2015/11/17
        }
    }

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Activity activity = getActivity();
            if (activity != null) {
                //[BUGFIX]-Add-BEGIN by TCTNB.Yang.Hu,2015/11/17, Defect-937969 TCT DRM solution
                try {
                    if (Settings.Global.getInt(mContext.getContentResolver(), Settings.Global.AUTO_TIME) > 0) {
                        //this may be auto update time from NTP write 1,0
                        DrmManagerClient mClient = new DrmManagerClient(mContext);
                        if (mClient != null){
                            mClient.tct_writeSecureTimeToFile(1, 0);
                            mClient.release();
                        }
                    }
                } catch (SettingNotFoundException e) {
                    e.printStackTrace();
                }
                //[BUGFIX]-Add-END by TCTNB.Yang.Hu,2015/11/17
                updateTimeAndDateDisplay(activity);
            }
        }
    };

    private static class SummaryProvider implements SummaryLoader.SummaryProvider {

        private final Context mContext;
        private final SummaryLoader mSummaryLoader;

        public SummaryProvider(Context context, SummaryLoader summaryLoader) {
            mContext = context;
            mSummaryLoader = summaryLoader;
        }

        @Override
        public void setListening(boolean listening) {
            if (listening) {
                final Calendar now = Calendar.getInstance();
                mSummaryLoader.setSummary(this, ZoneGetter.getTimeZoneOffsetAndName(
                        now.getTimeZone(), now.getTime()));
            }
        }
    }

    public static final SummaryLoader.SummaryProviderFactory SUMMARY_PROVIDER_FACTORY
            = new SummaryLoader.SummaryProviderFactory() {
        @Override
        public SummaryLoader.SummaryProvider createSummaryProvider(Activity activity,
                                                                   SummaryLoader summaryLoader) {
            return new SummaryProvider(activity, summaryLoader);
        }
    };
}
